<?php
return [
    //global rules
    '<controller>/<action>' => '<controller>/<action>',
    'debug/<controller>/<action>' => 'debug/<controller>/<action>',
    //index rules
    '' => 'site/index',
    '/manga/<id:\d+>/<name>' => 'manga/index',
    '/<controller>' => '<controller>/index',
    //site rules
    '<action:[\w-]+>' => 'site/<action>',
    //group rules
    'group/<action:[\w-]+>' => 'group/<action>',
    //manga rules
    '/manga/list/<page:\d+>' => 'manga/list',
    //chapter rules
    '/read/<id:\d+>/<chapter:\d+>/<name>' => 'chapter/read',
];