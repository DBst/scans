<?php

namespace app\models;

use Yii;
use yii\base\Model;

class Functions extends Model
{
    public static function mangaNameUrl($name)
    {
        return strtolower(preg_replace("/^[a-zA-Z0-9_ ]$/", '', str_replace(' ', '_', $name)));
    }

    public static function getMangaType($type)
    {
        if ($type == 1) {
            return 'Manga';
        } elseif ($type == 2) {
            return 'Manhwa';
        } elseif ($type == 3) {
            return 'Webtoons';
        } elseif ($type == 4) {
            return 'Manhua';
        }
        return '';
    }

    public static function unixToDate($unix)
    {
        return date('Y-m-d H:i', $unix);
    }

    public static function getClientIP()
    {
        $ipaddress = false;
        if (isset($_SERVER['HTTP_CLIENT_IP'])) {
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } elseif (isset($_SERVER['HTTP_X_FORWARDED'])) {
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        } elseif (isset($_SERVER['HTTP_X_CLUSTER_CLIENT_IP'])) {
            $ipaddress = $_SERVER['HTTP_X_CLUSTER_CLIENT_IP'];
        } elseif (isset($_SERVER['HTTP_FORWARDED_FOR'])) {
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        } elseif (isset($_SERVER['HTTP_FORWARDED'])) {
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        } elseif (isset($_SERVER['REMOTE_ADDR'])) {
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        }

        return $ipaddress;
    }

    public static function uploadImage($image, $method = 0, $folder = '', $name = false, $removeFile = true)
    {
        try {
            if ($method == 0) {
                // imgur upload
                $oldImage = $image;
                $image = explode(',', $image)[1];
            
                $timeout = 30;
                $curl = curl_init();
                curl_setopt($curl, CURLOPT_URL, 'https://api.imgur.com/3/image.json');
                curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($curl, CURLOPT_TIMEOUT, $timeout);
                curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Client-ID ' . Yii::$app->init->config->imgur_client_id));
                curl_setopt($curl, CURLOPT_POST, 1);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($curl, CURLOPT_POSTFIELDS, ['image' => $image]);
                $out = curl_exec($curl);
                if ($out == false) {
                    throw new \Exception(curl_error($curl));
                }
                curl_close($curl);
                $json = json_decode($out);
                if (!empty($json) && $json->status == 200) {
                    return [$json->data->link, 0, $json->data->deletehash];
                }
                self::uploadImage($oldImage, 1);
            } elseif ($method == 1) {
                // imgbb upload
                $oldImage = $image;
                $image = explode(',', $image)[1];
            
                $timeout = 30;
                $curl = curl_init();
                curl_setopt($curl, CURLOPT_URL, 'https://api.imgbb.com/1/upload');
                curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($curl, CURLOPT_TIMEOUT, $timeout);
                curl_setopt($curl, CURLOPT_POST, 1);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($curl, CURLOPT_POSTFIELDS, ['image' => $image, 'key' => Yii::$app->init->config->imgbb_api_key]);
                $out = curl_exec($curl);
                if ($out == false) {
                    throw new \Exception(curl_error($curl));
                }
                curl_close($curl);
                $json = json_decode($out);
                if (!empty($json) && $json->status == 200) {
                    return [$json->data->display_url, 1, $json->data->delete_url];
                }
                self::uploadImage($oldImage, 2);
            } elseif ($method == 2) {
                // freeimage upload
                $oldImage = $image;
                $image = explode(',', $image)[1];
            
                $timeout = 30;
                $curl = curl_init();
                curl_setopt($curl, CURLOPT_URL, 'https://freeimage.host/api/1/upload');
                curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($curl, CURLOPT_TIMEOUT, $timeout);
                curl_setopt($curl, CURLOPT_POST, 1);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($curl, CURLOPT_POSTFIELDS, ['image' => $image, 'key' => Yii::$app->init->config->freeimage_api_key]);
                $out = curl_exec($curl);
                if ($out == false) {
                    throw new \Exception(curl_error($curl));
                }
                curl_close($curl);
                $json = json_decode($out);
                if (!empty($json) && $json->success->code == 200) {
                    return [$json->image->display_url, 2, ''];
                }
                self::uploadImage($oldImage, 10);
            } elseif ($method == 10) {
                // local upload
                $data = explode(',', $image);
                if (!empty($data[1])) {
                    $data[1] = str_replace(' ', '+', $data[1]);
                    $data = base64_decode($data[1]);
                    $im = imagecreatefromstring($data);
                    $dir = __DIR__ . '/../web/images/upload/';
                    if(!$folder) {
                        $dir .= 'chapters/';
                    } else {
                        $dir .= $folder;
                    }
                    $dir .= '/';
                    if(!$name) {
                        $name = rand(1000000000, 9999999999) . '_' . time() . '.webp';
                    } else {
                        $name .= '.webp';
                    }
                    if (file_exists($dir . $name) && $removeFile) {
                        unlink($dir . $name);
                    }
                    imagewebp($im, $dir . $name, 90);
                    if (file_exists($dir . $name)) {
                        return [$name, 10];
                    }
                }
            }
            return false;
        } catch (\Exception $a) {
            Yii::error($a);
            return false;
        }
    }
    
    public static function getBase64Size($image)
    {
        try {
            $size = (int) (strlen(rtrim($image, '=')) * 3 / 4);
            return $size / 1024 / 1024;
        } catch (\Exception $a) {
            Yii::error($a);
            return false;
        }
    }

    public static function imageExists($image)
    {
        if(@getimagesize(Yii::$app->init->config->protocol . Yii::$app->init->config->domain . $image)) {
            return true;
        }
        return false;
    }


    /**
     * Return error page
     *
     * @return mixed
     */
    public static function errorPage($error = 503, $dirFix = '../')
    {
        $data = [
            'error' => 404,
            'name' => 'Page not found',
            'message' => Yii::t('app', 'Strona niedostępna'),
        ];
        header($_SERVER["SERVER_PROTOCOL"]." 404 Page not found", true, 404);
        if ($error == 503) {
            $data = [
                'error' => $error,
                'name' => $error . ' ' . Yii::t('app', 'Strona niedostępna'),
                'message' => Yii::t('app', 'Strona niedostępna'),
            ];
            header($_SERVER["SERVER_PROTOCOL"]." " . $error . " Service Unavailable", true, $error);
        } elseif ($error == 401) {
            $data = [
                'error' => $error,
                'name' => $error . ' Unauthorized',
                'message' => Yii::t('app', 'Zaloguj się jeszcze raz'),
            ];
            header($_SERVER["SERVER_PROTOCOL"]." " . $error . " Service Unavailable", true, $error);
        } elseif ($error == 403) {
            $data = [
                'error' => $error,
                'name' => $error . ' Access Denied',
                'message' => Yii::t('app', 'Nie masz uprawnień do przeglądania tej strony'),
            ];
            header($_SERVER["SERVER_PROTOCOL"]." " . $error . " Service Unavailable", true, $error);
        }
        return Yii::$app->controller->render($dirFix. 'site/error', $data);
    }

}
