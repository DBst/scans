<?php

namespace app\controllers;

use common\models\Follows;
use common\models\Languages;
use common\models\Manga;
use common\models\MangaReviews;
use common\models\ReadList;
use Yii;

class MangaController extends \yii\web\Controller
{

    public function beforeAction($action)
    {
        Languages::setLanguage();
        Yii::$app->user->returnUrl = $_SERVER['REQUEST_URI'];
        return $action;
    }

    /**
     * Displays a page with information about some manga.
     */
    public function actionIndex(): mixed
    {
        return Manga::getMangaPage();
    }

    /**
     * It displays a page with manga and allows you to filter them
     */
    public function actionList(): mixed
    {
        return Manga::getMangaListPage();
    }

    /**
     * Follow manga
     */
    public function actionFollow(): array
    {
        return Follows::add();
    }

    /**
     * Get form that allow you to review manga
     */
    public function actionGetReviewForm(): array
    {
        return MangaReviews::getForm();
    }

    /**
     * Set review for manga
     */
    public function actionSetReview(): array
    {
        return MangaReviews::setReview();
    }

    /**
     * Sync user read list with db
     */
    public function actionSyncReadList(): array
    {
        return Manga::syncReadList();
    }

}
