<?php

namespace app\controllers;

use common\models\Languages;
use common\models\Manga;
use common\models\MangaChapters;
use app\models\Functions;
use Yii;

class ChapterController extends \yii\web\Controller
{

    public function beforeAction($action)
    {
        Languages::setLanguage();
        Yii::$app->user->returnUrl = $_SERVER['REQUEST_URI'];
        return $action;
    }

    /**
     * Displays page with some chapter.
     */
    public function actionRead(): mixed
    {
        return MangaChapters::getReadPage();
    }

    /**
     * Displays page with upload chapter feature.
     */
    public function actionUpload(): mixed
    {
        try {
            if (!Yii::$app->user->isGuest) {
                $maxFileSize = (int) ini_get('upload_max_filesize');
                $maxFileSize = $maxFileSize > 20 ? 20 : $maxFileSize;

                $mangaList = Manga::find()->cache(60*60*24)->all();
                return $this->render('upload', ['mangaList' => $mangaList, 'maxFileSize' => $maxFileSize]);
            }
            return Functions::errorPage(401);
        } catch (Exception $a) {
            Yii::error($a);
            return Functions::errorPage();
        }
    }

    /**
     * Save chapter in database
     */
    public function actionSave(): array
    {
        return MangaChapters::saveChapter();
    }

    /**
     * Displays page with list of uploaded by you chapters.
     */
    public function actionMyChapters(): mixed
    {
        if(!Yii::$app->user->isGuest) {
            $offset = !empty(Yii::$app->request->get('offset')) ? (int) Yii::$app->request->get('offset') : 0;
            $chapters = MangaChapters::find()->where(['added_by' => Yii::$app->user->id])->orderBy(['added_at' => SORT_DESC])->offset($offset)->limit(11)->all();
            return $this->render('my-chapters', ['chapters' => $chapters]);
        }
        return Functions::errorPage(401);
    }

}
