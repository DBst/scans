<?php

namespace app\controllers;

use common\models\GroupInvite;
use common\models\Groups;
use common\models\Languages;
use app\models\Functions;
use yii\web\Controller;
use Yii;

class GroupController extends Controller
{

    public function beforeAction($action)
    {
        Languages::setLanguage();
        Yii::$app->user->returnUrl = $_SERVER['REQUEST_URI'];
        return $action;
    }

    public function actionIndex()
    {
        return Groups::managePage('index');
    }

    public function actionSettings()
    {
        return Groups::managePage('settings');
    }

    public function actionChapters()
    {
        return Groups::managePage('chapters');
    }

    public function actionMembers()
    {
        return Groups::managePage('members');
    }

    public function actionCreate()
    {
        try {
            if (empty(Yii::$app->user->identity->group_id)) {
                $groups = new Groups();
                return $this->render('create', ['groups' => $groups]);
            }
            return Functions::errorPage(503);
        } catch (Exception $a) {
            Yii::error($a);
            return Functions::errorPage(503);
        }
    }

    public function actionSettingsSave(): array
    {
        return Groups::saveSettings();
    }

    public function actionCancelInvitation(): array
    {
        return GroupInvite::cancel();
    }

    public function actionRemove(): array
    {
        return Groups::removeUser();
    }

    public function actionInvite(): array
    {
        return Groups::invite();
    }

}
