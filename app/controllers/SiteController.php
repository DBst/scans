<?php

namespace app\controllers;

use Codeception\Platform\Group;
use common\models\Comments;
use common\models\CommentsRating;
use common\models\Follows;
use common\models\GroupInvite;
use common\models\Groups;
use common\models\Languages;
use common\models\LoginForm;
use common\models\MainCarousel;
use common\models\Manga;
use common\models\MangaChapters;
use common\models\MangaImages;
use common\models\MangaReviews;
use common\models\Notifications;
use common\models\Users;
use Exception;
use app\models\Functions;
use app\models\PasswordResetRequestForm;
use app\models\ResendVerificationEmailForm;
use app\models\ResetPasswordForm;
use app\models\SignupForm;
use app\models\VerifyEmailForm;
use Yii;
use yii\base\InvalidArgumentException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\Response;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions(): array
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function beforeAction($action)
    {
        Languages::setLanguage();
        Yii::$app->user->returnUrl = $_SERVER['REQUEST_URI'];
        return $action;
    }

    /**
     * Displays homepage.
     */
    public function actionIndex(): mixed
    {
        try {
            $data = MangaChapters::getDataForIndexPage();

            return $this->render('index', [
                'most_popular' => $data['manga'][0][2],
                'main_carousel' => $data['carousel'],
                'last_added' => $data['manga'][1][2],
                'last_updated' => $data['manga'][2][2],
            ]);
        } catch (Exception $a) {
            Yii::error($a);
            return Functions::errorPage();
        }
    }

    /**
     * Verifies the correctness of the entered link
     */
    public function actionImgurCheckImages(): array
    {
        return MangaImages::imgurCheckExists();
    }

    /**
     *
     */
    public function actionGroupCreateSave(): array
    {
        return Groups::create();
    }

    /**
     * Logs out the current user.
     */
    public function actionLogout(): Response
    {
        Yii::$app->user->logout();

        Yii::$app->session->setFlash('success', Yii::t('app', 'Pomyślnie wylogowano.'));
        return $this->redirect(Yii::$app->request->getReferrer());
    }

    /**
     * Return notifications to user
     */
    public function actionGetNotifications(): array
    {
        return Notifications::get();
    }

    public function actionSetLanguage(): array
    {
        return Languages::change();
    }

    /**
     * Signs user up.
     */
    public function actionSignup(): array
    {
        return Users::signupForm();
    }

    /**
     * Logs in a user.
     */
    public function actionLoginForm(): array
    {
        return Users::loginForm();
    }

    /**
     * Requests password reset.
     */
    public function actionRequestPasswordReset(): array
    {
        return Users::requestPasswordReset();
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @throws BadRequestHttpException
     */
    public function actionResetPassword(): array
    {
        return Users::resetPassword();
    }

    /**
     * Verify email address
     *
     * @param string $token
     * @throws BadRequestHttpException
     */
    public function actionVerifyEmail(string $token): Response
    {
        return Users::verifyEmail($token);
    }

    /**
     * Resend verification email
     */
    public function actionResendVerificationEmail(): Response|string
    {
        return Users::resendVerificationEmail();
    }

}
