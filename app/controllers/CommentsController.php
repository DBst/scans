<?php

namespace app\controllers;

use common\models\Comments;
use common\models\Languages;
use Yii;

class CommentsController extends \yii\web\Controller
{

    public function beforeAction($action)
    {
        Languages::setLanguage();
        Yii::$app->user->returnUrl = $_SERVER['REQUEST_URI'];
        return $action;
    }

    /**
     * Feature that allow adding comments to manga.
     */
    public function actionAdd(): array
    {
        return Comments::add();
    }

    /**
     * Feature that allow rate comments.
     */
    public function actionRate(): array
    {
        return Comments::rate();
    }

}
