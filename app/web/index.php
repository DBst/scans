<?php

defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'dev');
$PHP_VERSION_NUMBER =  implode(explode('.', phpversion()));
for($i = strlen($PHP_VERSION_NUMBER); $i < 5; $i++) {
    $PHP_VERSION_NUMBER .= '0';
}
$PHP_VERSION_NUMBER = (int) $PHP_VERSION_NUMBER;

if($PHP_VERSION_NUMBER < 80000) {
    echo 'Hyakki require PHP 8.0 or higher!';
    exit;
}

require __DIR__ . '/../../vendor/autoload.php';
require __DIR__ . '/../../vendor/yiisoft/yii2/Yii.php';
require __DIR__ . '/../../common/config/bootstrap.php';
require __DIR__ . '/../config/bootstrap.php';

$config = yii\helpers\ArrayHelper::merge(
    require __DIR__ . '/../../common/config/main.php',
    require __DIR__ . '/../../common/config/main-local.php',
    require __DIR__ . '/../config/main.php',
    require __DIR__ . '/../config/main-local.php'
);

(new yii\web\Application($config))->run();
