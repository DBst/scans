const App = () => {

    // variables
    let chapterImages = [];
    let file;
    let cardOver = false;
    let silentLoaderInterval;

    // page initialization
    function init() {
        $(document).find('.last-updated').niceScroll({
            horizrailenabled: false
        });
        initCarousels();
        window.dispatchEvent(new Event('resize'));
        setTimeout(function () {
            loader(true);
            setTimeout(function () {
                $('.header-navbar-container').css('height', $('.header-navbar-container').height());
                $(document).off('scroll', window).on('scroll', window, function () {
                    if (window.scrollY >= $('.header-navbar-container').height() && !$('.header-navbar').hasClass('sticky-header')) {
                        $('.header-navbar').addClass("sticky-header", 500).fadeOut(0).fadeIn(250);
                        $('#scroll-to-top').removeClass('d-none');
                    } else if (window.scrollY <= $('.header-navbar-container').height() && $('.header-navbar').hasClass('sticky-header')) {
                        $('.header-navbar').removeClass("sticky-header", 100);
                        $('#scroll-to-top').addClass('d-none');
                    }
                });
            }, 150);
        }, 50);
        initUploadChapter();
        $('#select-manga').select2({
            'placeholder': $('#select-manga').attr('placeholder'),
        });
        let updateNavItem = $('nav .navbar-collapse .nav-item a.nav-link[href="' + location.pathname + '"]');
        $('nav .navbar-collapse li.active').removeClass('active');
        if (updateNavItem.length) {
            $(updateNavItem).closest('.nav-item').addClass('active');
        }
        $(function () {
            $('.tip').tooltip()
        });
        //image loading animation
        $('.chapter-image img').on('load', (e) => {
            $(e.currentTarget).parent().removeClass('chapter-image-loading');
        }).each((i, item) => {
            if(item.complete) {
                $(item).trigger('load');
            }
        });
        initReadList();
        document.querySelectorAll('.form-outline').forEach((formOutline) => {
            new mdb.Input(formOutline).init();
        });

        let siteTitle = $('input[name="site-title"]');
        if(!$('head > title').text().includes(siteTitle.val())) {
            $('head > title').text($('head > title').text() + ` - ${siteTitle.val()}`);
        }
    }

    function loader(load = false, message = 'Ładowanie...', silent = true) {
        if(silent) {
            silentLoader(!load);
            return;
        }

        if (load) {
            Notiflix.Loading.Remove();
        } else {
            Notiflix.Loading.Dots(message);
        }
    }

    const silentLoader = (run = true) => {

        let maxWidth = $(window).width();
        let currentWidth = $('#silent-loader').width();

        if(run) {

            $('#silent-loader').css('display', `block`);
            if(silentLoaderInterval !== null) {
                silentLoader(false);
            }

            let c = 0;

            silentLoaderInterval = setInterval((i) => {

                c++;
                let percentage = c / (c + 2) * 100;

                $('#silent-loader').css('width', `${ percentage }%`);

            }, 50);

        } else {

            $('#silent-loader').css('width', `100%`);
            setTimeout(() => {
                $('#silent-loader').css('display', `none`);
                $('#silent-loader').css('width', `0%`);
            }, 250);

            clearInterval(silentLoaderInterval);
            silentLoaderInterval = null;

        }

    }

    function ajaxRequest(url, data) {
        return $.ajax({
            url: url,
            type: 'POST',
            dataType: 'json',
            data: data,
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            }
        }).fail(function () {
            loader(true);
            Notiflix.Notify.Failure(translate.defaultProblem);
        });
    }

    $(document).on('pjax:success', function () {
        init();
    });
    $(document).on('pjax:popstate', () => {
        setTimeout(() => {
            init();
        },150);
    })
    $(document).ready(function () {
        $('.notifications-list ul').niceScroll();
        init();
    });
    Notiflix.Loading.Init({
        svgColor: '#6032c6',
        fontFamily: 'Quicksand',
        useGoogleFont: true,
        cssAnimationDuration: 200,
    });
    Notiflix.Notify.Init({
        opacity: 0.95,
        borderRadius: '50px',
        clickToClose: true,
        fontSize: '15px',
        cssAnimationStyle: 'from-right',
        distance: '65px',
    });
    loader();

    function initCarousels() {
        if ($(".index-carousel").length) {
            let swiper = new Swiper(".index-carousel", {
                speed: 600,
                parallax: true,
                autoplay: {
                    delay: 5000,
                    disableOnInteraction: false,
                },
                pagination: {
                    el: ".swiper-pagination",
                    dynamicBullets: true,
                }, navigation: {
                    nextEl: ".swiper-button-next",
                    prevEl: ".swiper-button-prev",
                }
            });
            let swiper2 = new Swiper(".last-added", {
                slidesPerView: 1,
                pagination: {
                    el: ".swiper-pagination",
                    clickable: true,
                },
                breakpoints: {
                    "@0.00": {
                        slidesPerView: 2,
                    },
                    "@0.75": {
                        slidesPerView: 3,
                    },
                    "@1.00": {
                        slidesPerView: 5,
                    },
                    "@1.50": {
                        slidesPerView: 6,
                    },
                },
            });
        }
    }

    //dropdown slide down after hover
    $('.dropdown').hover(function () {
        $(this).find('.dropdown-menu').first().stop(true, true).delay(50).slideDown(150);
    }, function () {
        let classList = $(this).attr('class').split(/\s+/);
        if(classList !== 1) {
            $(this).find('.dropdown-menu').first().stop(true, true).delay(50).slideUp(150);
        }
    });

    //show manga description after hover her card
    $(document).on('mouseover', '.manga-card', function () {
        let mouseover_ = this;
        let id = $(this).attr('id');
        cardOver = setTimeout(function () {
            if ($('#' + id + ':hover').length) {
                $(mouseover_).find('.additional-info').removeClass('d-none');
            }
        }, 1000);
    });
    $(document).on('mouseout', '.manga-card', function () {
        $(this).find('.additional-info').addClass('d-none');
        clearTimeout(cardOver);
    });

    //change img src to no-image if cant load properly image
    $(document).on('error', 'img', function () {
        if($(this).attr('src') !== '') {
            $(this).unbind("error").attr("src", '/images/site/no-image.jpg');
        }
    });
    $(window).bind('load', function () {
        $('img').each(function () {
            if ((typeof this.naturalWidth != "undefined" && this.naturalWidth == 0) || this.readyState == 'uninitialized') {
                if($(this).attr('src') !== '') {
                    $(this).attr('src', '/images/site/no-image.jpg');
                }
            }
        });
    });

    //show manga description after click on btn
    $(document).on('click', '.manga-page .show-description', function () {
        if ($('.manga-description').css('display') == 'none') {
            $('.manga-description').fadeIn(150)
        } else {
            $('.manga-description').fadeOut(150)
        }
    });

    //pjax async loading
    $(document).on('click', 'a', (e) => {
        try {
            let $this = $(e.currentTarget);
            let isPjax = typeof $this.attr('data-pjax') !== 'undefined';

            if ($('#content').length && isPjax) {
                e.preventDefault();
                if ($this.attr('href') !== window.location.pathname) {
                    loader();
                    $.pjax({
                        container: '#content',
                        url: $this.attr('href'),
                        scrollTo: 0,
                    }).then(() => {
                        $('html, body').animate({
                            scrollTop: 0
                        }, 50);
                    });
                }
            }
        } catch (error) {
            window.location.href = $this.attr('href');
        }
    });

    //manga upload section
    function initUploadChapter() {
        let dropzone = $('.dropzone');
        if (dropzone.length && dropzone[0].dropzone) {
            dropzone[0].dropzone.destroy();
        }
        dropzone.dropzone({
            autoProcessQueue: false,
            parallelUploads: 2,
            thumbnailHeight: 120,
            thumbnailWidth: 120,
            maxFilesize: 20,
            filesizeBase: 1000,
            addRemoveLinks: true,
            acceptedFiles: 'image/*',
            dictRemoveFile: 'Usuń',
            // previewTemplate: `
            //     <div class="dz-preview dz-file-preview">
            //         <div class="dz-details">
            //         <div class="dz-filename"><span data-dz-name></span></div>
            //         <div class="dz-size" data-dz-size></div>
            //         <img data-dz-thumbnail />
            //     </div>
            //     <div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div>
            //     <div class="dz-success-mark"><span>✔</span></div>
            //     <div class="dz-error-mark"><span>✘</span></div>
            //     <div class="dz-error-message"><span data-dz-errormessage></span></div>
            //     </div>
            // `,
            // previewTemplate: `
            //     <div class="dz-preview dz-image-preview">
            //         <div class="dz-image">
            //         <img data-dz-thumbnail="" alt="7c1d50609e8db43a9434c11ccc9c36d1.jpg" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHgAAAB4CAYAAAA5ZDbSAAAXCElEQVR4Xu1dCXRV5Z3/vzUbkAQQkH2rg+LCqrLTVkARVDrjlKpjcQOdcYoWe0Y745lj6cJYHedMMWxaqj0ixwVUVBQkzDiKkA7HAVuwgIJFEBJCIPvb5/f77ruP+15eyEt49+W+5H6cnITk3vu++/2+/758jgiGZPkIhUJysvyUDB8+XILBoHqbiDMiTnx3ikPq6+vF4XDEvWVYInLkyBEZMWKE+n1YXW3OiIQCcu/ChbLy2RLMRoTzLa+okIGDhkoE84o4wuKIhCUcCIjP3ygelzdtE3FkO8Dcn3v37pUJEyeJ0+ORxsZGbXHCQcnPz5fKilPidDjF5XbFLdqJUxXS9+KLxenSf28DnLZdlY4HRcIRCQXDcvTYURlx6WUSAkVqwII2AXpujkdqa2qbUC4pvLy8XAYOHZaOaaT0DJuCU1qm+IsI4vGvj8vgoUMVy3N63AI0NfYMEP0+jZJdMQrV7q+rq5Pi4mIJO+Mpug1TSPkWG+CUl0on0rCcPXtWevXqIw6nU8LEFRQdAfXm5+XJ6arT4klgydwQAci4bt26qYcElTTMzLABbuU6U87mFxSI15srQSgnEWDlcbgkBABra6vF7XY3odwwwC8qKhK/328D3Mr1ztjl4RA0TVJpQRcJhkPiAFt2g9UGfT5xu5xSAc1Up1B9UtwMXq9Xinr0lJra2ibAZ2LykaBf7rr3XlmzcpWtRZ93wcMiudCMFRU6HQrgSCAkXmjPXx/9SsnWRJnrA/jdu3eXBn9AqWGJf7cBzsQKpPgZiiVDgXJ6PeoOAhsKBKV02zaZNHEClKxzD+ImIJi9e/eWM2fOiLi1e9pjUAbfdc89NgU3t/jUkik/Gxr9SlsOUe6SVQPwLe+9JxOuvVYcrnilqRbsePz48XLgwAFlNjm9Oe2BrfpMG+Bmlj4SikA5jsiAgQOV50cgZzkohz1QpNb9/kWZN2+eOKFJ66O6ulq6dOkC23ikfHnkcMx0ajd0CTCcLgvuukueW7XalsFGIKhUDf/WJXLkq6+Ux8kBuasoAqCX/Ga5LFx4bxPcaApdf/31sv2/PhQXZXR7Ihv9bBvgBBCo+dLUGT1mnBw+fFgaA5ppE4a8zYFG/MwzzwDc+0C559hyTU2N5OTkyIwZM2Tnzp3Kt0zzyQrDBjgBBVLopEmTpOwPu8Wd45VACG4JkGKOyy133323PP3009CkfWDFBbE7qXzddttt8sYbbyiWHQK6NsCQUFYKNpC9+nwBueXm7ykqbAj7FLACVu2BxnzP3Qtk+fLlcduhoaFBsey//4d/lFc3vI77fVYg2vg5QAbfuWCBPL96TeeWwXUN9fKjBxfLS+vWKyr0h6A1A+AwtOg5c+bIK69ovyf71gcpd9myZfKvT/xMKV8ubATLjc4OMKmOptCDDy2WdQA3iCgRh4cyFtQ5feo0gPuKdC3sEocdwV2xYoU8+uijYOPwclkO2eiEbIB98sQTT8ivn/l3OCc8MYARD5Qrr7hC/rBrFzZAWLy556iT1Lpq1SpZsmSJYtFhCNxYyNBqQHdmgEm5a9askUceeUQa8DNlrhfKFOXxkMEDZc+ePZKHCJFxkOI/2bVTZsy6QYFr+QGA77jzTln73POdSwZTO16/fr088MADEgSgQVAhZa476mI8cOBzBSCDBcaxe/dumf6d70hdg2ZOkZotPTobwASNlPv2e5vljttvl8ao5utAYNeJoH1XRIvKK8rh3Ig3ZGkfMxuDeVTMqYqKaktjy8nRVXn7HXfIC2t/1zkomAB/9tlncu2UyZIDrZdhPA4CzIB9+cmTWAhHnMzl389Wn5WhSLWpra1BjpVH/MGQ5cHtVACTAmnm7N+/XyZMmCCwYBVLVrYuRghpNir9xuBb5u9p6wbx+8Li7tKla1eVJZlNo9NQsD8YQK6UX7r36K4cF42gQIKrvoBYfV2NAjcRYJpDxbinAU6QEH52GezgbAC6wwOsa7qnzlRJr549Dc4IZC0DXKba+JEPbHRgKNYWldX0MZPInZ705QtncmNEIiG5ae5c2fDa6x1LBuvA0uTRUm0KJAcZGYFo8MABrdmFlJsT3xyXwsJCPSlSActE9QawdGZpkIL5/0gGMyHTuwHCyhO3saMBTLKjm7EBspVsGdI1bt1cCNxXVlaq+K1xUMaShXcpLFKUa3kzqIXd4ACbunH2bNn4+oaORcEE14fUGYIbBFVGouEdptLw6+D+fdK3b99YcnqMcqFUkaKZu6woNxPODDU/cI4EBS8dlMxHzpw5Uza98WbHAFinOLLYnhf1UH5iYuuIaBRM6ty3708yqF+/OIVK16AvuugiqQfIftyXsQFwC8BJmBif7uGCPf/t6dNl8zvvdgyAKXOpMOXk5YPF8p826KHKhcK0efNmueaaa+IYtgIXf+/ff4Bi2ypKaCrlglrxGXSD1tfWSVlZmUycOBGcRoc3fS7QDgcwwWKqaj2S5IxB93xkQ3744YcycuRIRbnGFFayxwFDhqoMyFgBWbpJKe55qOjj/1ERseX99+VaJO4VFfeAnqB7z9LHPToMwD5ox16YMgXItKDmq5dkEkjK0i3vviNTpkyJyVyycgJNUK9A1OjEqUoFQSZkbl5ejgSgH7yz6W3FPjmXboXFBrGQXoCnTp0qWza/l90smtrykGHD5NSpU1Fa0WQu2fW6detk3tw5KkqkBw+4qKT2yZMnQybvkwbkXGVqeDwuefqpp2TBnT9UGZrcgIVF3cUXc3/aAMewIEgcZHENyJXSh4teDLDAny1dinDgEmXzctD1mJubKydQtM08qo8++kijXFOT5DTAukGRCmIjPVvyrNw2/wcxMUGOYybAU7CJt76/JTspmJQ4evRo+fzPByDDzg0PAGNAnsF8UojugiQLVklyoJ6NGzdmiGg1gAvhy15030I1J3IWvfLfBjgJDHq7hGnTpgljtMyqYClnHqiTrPj2+d+XkpISlc7KwXIS3a5dCqr+1a+fyhC4/BhkhCCufMtNN8na538bm5M+ATMBph3MLNHSrR9kFwWTNS9atEgF7ZVSFQWY5tDsG2+Ul1F5QJtXp1xeQ+pl2utTkH9nYJ5kani8bpl3yy2yeuVKdHcINalCtAE2IEGWzLSZxx77qaxe85zKWybl0gwKIFo0Y+YMeevNt2LsT3dg1NQ3yG/XrlXpOWYPrQWLVu3ixteY0WPkfdQxFcAfnmyYDfBkUPC2bKFgAvb444/LcnSNaUDaDLMruJIuUOe4MWPk448/Vmuoyzfd7Fn74u9lISg+E4MAU2dzYW5XjbpCtm8rVaZQV8hgG+DzIEANmCbP4sWLYVKA5WIZIwCcdUQD+vdVrYkS47l0XFBGT/vudZnAVn0GS01DqAkeNLA/TLA/NglFEmxuPN3ZYiYFu8FCrr76avnv0u3Wl8Gvvvqq3INaVwLtgtISoLMCplCvnhfJMXS8YXVfYpU9MyNZM3S6uiZjAOd581SU6uhfDquIlhclMMZx7Ngx6dOnjw2wviiUuTt2fiJ/fev3FYhKvoEHuqEiUkum/zix2RizN75E8diVV43SGqWYOHQbOkJnCTZcz57d5SRzuxIaoHEK9JwNREmq/h78nZkUDHeOXDN2nHzy0Q5l63PDWa4R2i4kn9887xaprDobcyWyL0YRZNqJEyeUfEtkzUeOfIUMyEtV0MBcJ4bmJNFzqumZqqrS3J6Jczp9+jQCGv2VB41cyKjhm+XosDTAtF0PHjwoY6A8eaAlN6InBqkiDPnrgTlUjUzHxLxlLt7x48dl2PBLVFSIi68DYBoR0+0J6qVZ5oPLNJFwOSe9jRLlLq/LGAU7QjJ+1BjZtWOn9SiYLRAuv/xyRaEM/DncXgUwF+g02gEy7TWRDZIF9urVCy8DHTYKLlm0K33u3Sb7hIoeKZgBBAVugkigCGG/DoKrt1nq1AATUDYb69mrN/ncuQgQfM1coKqqqiY2ZfWZGuRd5avUVrZeyITcdUDeutCLMohWRWcwp0QziJTLd8nNL1Cyj4ohRy4cH2eqzqg2TEoGw5Y3j0UHZfzosdaiYALYr38/mEKImxr4Hf3LBF4vGTFmQtZWo00g0nMUCUE+ZyLsF4FXilR5urJCbbhEbsL8rp7I5AyAe4SQtuuMptx2WoCpTfKLpg47tHJhOPSFO1t5SmVBGBcyliTXpSuyMlA4hiZlZstclrhEmAoE6jwJJa+H2ljnODdTbwg4waciFcG8jCM/1yun0InWG22vZC4Fh+TKy0bK/+3+tP1lMKmOrkSWZrKaQE9hoSJVCZnrxYKR7RkVKwLco0cPJNhpSg7LOM0GmFhSqfry0EEZPHhwE5lL5ZB6AJ0sKiiS0CerUwP80MNL5Fm03eOgFgydWf7lp48q9yTlnXH44S3q3buPVKNeyJHQ2TXdGrO+aVS5CxD+8+f7ZdCgQXHchKYPN183pNw2wHY/1w86fjZhPCRQU6coXOV/mRgPRnBURo64TPZ+uscaFJwM4F8sfUJ+8shPVPWfPsJQcIYgj+oEHAoMODS3mOkEmuDSzi0t3SZjx45RjzbqAeRATMWtqKyCvHU1m1NtA5xAwcv/8z/QumihcuFzEdlJbsKEiXLo0BcaBWSgIxXbKLkgT999+x25bsZ3Y32zCLJe2NYPqbiUvwE0UTu/cyUogYaA4gSmUzDs4Eu/9Vfyxz2fWZeCEwGm3O3btx8q/RrO9WxOJ6kmeRZS9+RNtEmaCb82YXFGO9/xUspcViyyLFU5YiBKzpv4GgHAjTbAMRlsBJgLStvSi8wNrLJpsOoyN4e1wADw+edWy+0oGDem21LhI0eZMnW6fLp3T+xQjhYnhap7H/QHffam+qKxmS4ZNlw+/9P+7KDgTAHMz6HMZaedJ598Uu6/f6HS0o0AMwhyE9JwPvyfj5WYSLmOyQY4XotuDwoOIRpFjfjHDz0sv/jlz+NYMqmWcpf9PV566SX8TTPPUh4ZBDgCNjEY4uyLA4dsCjYCRJk7+4YbUJX3umbnGhwZBPjfQNXLlv0KCh9yuxIcGS0CnUGA4ZKXgQT4IACmeDtvuNCPIE68U6bFd4leoBfvGa9v0sqQFyUzkzJBwUY7l5OcfcMs2bBhQ5OQHzX4l5BV8sg/Papks56XnepCqOsyCDBLV5hccOSLL5NmdDDjUx8hvE8earpSHar2i+WpIIT1615WIszyANO/zEKwD7a+H3fAhr5DX3vtNbkHZyDUg4VztMnnnUGAmTZ7MQ7haglghmDdEEl0waY6oHWoKN/M666Tt9/a1OTIAktSMO3dxkZ4pHgWkmHQvt2xY4fMRTsEavD1jRfQeDSDALMAnBT8l8NHWqTgVIE9d53SQmUqKidKP9jW5HZLAUwhS3bjwpx9aExqDGhQO966davceuutSrmiFw2uqtavR/QONz6E4UKW0HCYaSbxPfr06Q2AD8cAPol+X4MHDdbOLkzikUnmpNHcxozews5HdwR6afQORVMnI7H+gw+sDjD6PDuR+gMlJNQA6ow2/KZ/mWlBo0aNUh40yhmtuvcCbPBIQJi+o+dJmwkwo1kMfBzFEQO6nqgyXlCop9QBqtkpDoojsnzVQ1t1I9C0z6mTJuJwkq3ZCTCpl1RAZeqcDYydn+KiJLuMdxs5hJkAExR2Lfjm66MKDl2X0HUHbbOmPiZNmixlu/83Ov8OAHBzr35hAMc/1UyAuUEJ8IljXyeFsrXvMW7c1cprpyUMZhHAupnENBwmzhmOZEh9e7fxSrMBZry8HO2iWkeryV9m7Phx8ikCF06DDpIVLNoGOLXdmfUA0w4uK9uVeuAgtXU571VhKC1z5t4sZ2MVj6nboi19PFnp0CFDYt1m3eguwOGgiwuD8W0OLxIBOXTNXnd4FCCRkcUFuv6R9QDzEA6+TICmUIaGB8ess2CNYUZtpA9gAqbOW8R7qVQjnVFHkyfyPVr9tO6R079rXqkIjsutiGtak7UAZwjLpB+jtNt2mkBzclm3e331dUpj1rV+G+B2Asqsj/XbAJu1tNZ4rg2wNXAwbRY2wKYtrTUebANsDRxMm4UNsGlLa40H2wBbAwfTZmEDbNrSWuPBNsDWwMG0WdgAm7a01niwDbA1cDBtFu0GsIf5TCaXjJq2agkP1nObzPi8C+kwxHm1G8Bm1yaZsdjZ+Mx2A5jxSh4S2REGy14Y1WFXg3QPnofMcKFex6znmOm9xZgh6kKlJLNKVLsJlViHBMToOY51tdVxCe0ZiSZxMqwP9mewDX+6F974PDd6gO3HsQJ+tntI8/CgApM1VuxGwBgvg/f6oST8TuCN/2dvFHbH169bUbI81m+EU8sIwGleg3Z/nLk5WUHVw6T8mxPZk5PV7oikeQI2wIYWDmleW0s8zgbYBrjNGzGMOqh0sWim8YxLZ9qsnnzGgzbUuX6tqHxr84pk5Mawaius97bONAUfQ+nKgAH9tTeNnumY6muzmrALDu9kSc8FJ74bswup1rsT+mSlOimrXVeQnyvlKADT62kzCTCL51gXNXAIis+wMNCZW7U8QXQSZOFZWiob0pk+2qq3MPliB/Kg2d8jI/2iE1g07VvVEHzwoGgmZ3xzuda9ehtKVx7+8RIpWbFCkX+T7OB01F607g3SdLWWFKunxhJgP+qL2xdgUjBLZqOvyIpY/NhSRnYcBOz6h3u+PX0aDtl8r8laNakP5hU8cGMlzhfiCDvaXoObJmTS8hi9a5beRxO9CxUF6yInkyw6RsGDhmr1wUi6p8+ZPUk0kZw6y3aQXUdC6kyMTZs2Nem2mxRgNgFnA21tx2ctycZtjJkzZynXn94J14oAs+iutLS0Sbf68+9w7Ay4Nun1Gj9+fGoUnBaSsdhDeHQsqdQPylUs0YIUHEZjNh981h6e5JWmkZSC0/RsSz3G+gCHJYzzHhXAbWyjlGzBbYCjZl/7y2Ad4EYAHH/G04VQig2wDfCF7B/r3Js9LNqm4DbtGusDDJPUVrLahK26yQa47WvXrncyvYUHXD322D8nbSWsF02/jEOs1QEdaCBmVTOJ3XUX3b8IzWcSzSTNt8VYwPz582X69Okpr3nWK1l8aX5161oMt2MSD1DUTxPAwVncDDysy6oAszc2o1uNbAIXG9qBCRw8mq+srEyuuuqqzgMw35TA5SAB0OjD1U8Ab24lnCG/ZYINEUlwB8N1mTjoyqSna+/evXLpiEtsgDW3ffPDYSGAcW5MwkTjATa2l7IBji4VdzsjRcnOD+YljJMxHqyfdZhJRwdZLY8OHDR0iBbdaiZTV7VvhM6gUnoZacLXoUOHZOCAfjYFT5sySV588UXJ8WotihLHaQRThg8f3i7hQoqUEKJAVWdwFjMm1pTXaIrD0qU/l5KSEgmjobgNcIIMHo5g+j7kOSceEN3c1s8kBadCfgT+wcU/kjVr1oCKtcOv2SCuAokC3YsLU3mEuibrtWi+RDIlywZY2wOdBuDEFr6JJKBMLYQUfaASbbSUV5EyEeGEVJzmiiS5yvKK80bXjfoCKfg+2MQvvPACZhJt/I2ER/bLzs1JvVyo0wDMYD8zOHg0brLBDMveffqa0sowAhu8W3GxVIK9nq+K0Qa4GaJJhUWTIoqKi6QOTgTayEmyzVInyTRf6WRTUihdfh/TYLXB+f7dgh/KenjgIvBsqY0BCuYmNZzo1+JMOg0F2wC3uBese0FHpeC//cF8eWPjRiU2dE8W/ek2BWMvJmrR2UjBNsC6vErii+4IAH/v1r+RTW+9pdJoScGMF/vhBXPD8ZHqsGVwqitl4nXNKVk2wB2cgmfPnSNbt2yJo+AAUn9tGdxBZPCNAHhLAsBaH4/U2UnHYdG5iAcbRFO2y2BCOGvWLNm+fbuEIYNDBNUflLAPx9KnXtki/w9wBO3jiqi+ggAAAABJRU5ErkJggg=="></div> <div class="dz-details"> <div class="dz-size"><span data-dz-size=""><strong>16.7</strong> KB</span></div> <div class="dz-filename"><span data-dz-name="">7c1d50609e8db43a9434c11ccc9c36d1.jpg</span></div> </div> <div class="dz-progress"> <span class="dz-upload" data-dz-uploadprogress=""></span> </div> <div class="dz-error-message"><span data-dz-errormessage=""></span></div> <div class="dz-success-mark"> <svg width="54px" height="54px" viewBox="0 0 54 54" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"> <title>Check</title> <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"> <path d="M23.5,31.8431458 L17.5852419,25.9283877 C16.0248253,24.3679711 13.4910294,24.366835 11.9289322,25.9289322 C10.3700136,27.4878508 10.3665912,30.0234455 11.9283877,31.5852419 L20.4147581,40.0716123 C20.5133999,40.1702541 20.6159315,40.2626649 20.7218615,40.3488435 C22.2835669,41.8725651 24.794234,41.8626202 26.3461564,40.3106978 L43.3106978,23.3461564 C44.8771021,21.7797521 44.8758057,19.2483887 43.3137085,17.6862915 C41.7547899,16.1273729 39.2176035,16.1255422 37.6538436,17.6893022 L23.5,31.8431458 Z M27,53 C41.3594035,53 53,41.3594035 53,27 C53,12.6405965 41.3594035,1 27,1 C12.6405965,1 1,12.6405965 1,27 C1,41.3594035 12.6405965,53 27,53 Z" stroke-opacity="0.198794158" stroke="#747474" fill-opacity="0.816519475" fill="#FFFFFF"></path> </g> </svg> </div> <div class="dz-error-mark"> <svg width="54px" height="54px" viewBox="0 0 54 54" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"> <title>Error</title> <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"> <g stroke="#747474" stroke-opacity="0.198794158" fill="#FFFFFF" fill-opacity="0.816519475"> <path d="M32.6568542,29 L38.3106978,23.3461564 C39.8771021,21.7797521 39.8758057,19.2483887 38.3137085,17.6862915 C36.7547899,16.1273729 34.2176035,16.1255422 32.6538436,17.6893022 L27,23.3431458 L21.3461564,17.6893022 C19.7823965,16.1255422 17.2452101,16.1273729 15.6862915,17.6862915 C14.1241943,19.2483887 14.1228979,21.7797521 15.6893022,23.3461564 L21.3431458,29 L15.6893022,34.6538436 C14.1228979,36.2202479 14.1241943,38.7516113 15.6862915,40.3137085 C17.2452101,41.8726271 19.7823965,41.8744578 21.3461564,40.3106978 L27,34.6568542 L32.6538436,40.3106978 C34.2176035,41.8744578 36.7547899,41.8726271 38.3137085,40.3137085 C39.8758057,38.7516113 39.8771021,36.2202479 38.3106978,34.6538436 L32.6568542,29 Z M27,53 C41.3594035,53 53,41.3594035 53,27 C53,12.6405965 41.3594035,1 27,1 C12.6405965,1 1,12.6405965 1,27 C1,41.3594035 12.6405965,53 27,53 Z"></path> </g> </g> </svg> </div> <a class="dz-remove" href="javascript:undefined;" data-dz-remove="">Usuń</a></div>
            // `,
            previewTemplate: `
                <div class="dz-preview dz-image-preview">
                    <div class="dz-image">
                        <img data-dz-thumbnail="" alt="7c1d50609e8db43a9434c11ccc9c36d1.jpg" src="" />
                    </div>
                    <div class="dz-details"> 
                        <div class="dz-size">
                            <span data-dz-size=""><strong>0</strong> KB</span>
                        </div>
                        <div class="dz-preview-btn" style="">
                            Podgląd
                        </div>
                        <div class="dz-filename"><span data-dz-name=""></span></div>
                     </div>
                </div>
            `,
            init: function () {
                this.on("addedfile", function (file) {
                    let reader = new FileReader();
                    reader.onload = function (event) {
                        chapterImages.push(event.target.result);
                    };
                    reader.readAsDataURL(file);
                    let previewBtn = $(file.previewElement).find('.dz-preview-btn');
                    $(previewBtn).off('click').on('click', (e) => {
                        Swal.fire({
                            imageUrl: file.dataURL,
                            imageAlt: 'A problem occured'
                        });
                    });
                });
                this.on("removedfile", function (file) {
                    let reader = new FileReader();
                    reader.onload = function (event) {
                        let index = chapterImages.indexOf(event.target.result);
                        if (index !== -1) {
                            chapterImages.splice(index, 1);
                        }
                    };
                    reader.readAsDataURL(file);
                });
            }
        });
    }
    $(document).on('click', '#manga-save', function () {
        loader(false, 'Trwa przesyłanie...');
        let data = {
            'manga_id': $('#select-manga').val(),
            'chapter_number': $('#manga-chapter-number').val(),
            'language': $('#select-manga-language').val(),
            'chapter_name': $('#manga-chapter-name').val(),
            'images_type': $('#select-manga-images-type').val(),
            'images': {},
            'imgur_url': $('#manga-imgur-images').val(),
        };
        data.images = chapterImages;
        ajaxRequest('/chapter/save', {
            'data': data,
        }).then(function (result) {
            if (result.status == true) {
                Notiflix.Notify.Success(result.message);
                $.pjax.reload({
                    container: '#content'
                });
            } else {
                Notiflix.Notify.Failure(result.message);
            }
            loader(true);
        });
    });

    //show login modal && login feature
    $(document).on('click', '#login-btn', function () {
        loader();
        ajaxRequest('/login-form', []).then(function (result) {
            if (result.status == true) {
                Swal.fire({
                    html: result.partial,
                    width: 600,
                    showConfirmButton: false,
                    showCloseButton: true,
                    showCancelButton: false,
                    focusConfirm: false,
                });
                document.querySelectorAll('.form-outline').forEach((formOutline) => {
                    new mdb.Input(formOutline).init();
                });
            } else {
                Notiflix.Notify.Failure(result.message);
            }
            loader(true);
        });
    });
    $(document).on('click', '#login-send-form', function (e) {
        e.preventDefault();
        loader();
        ajaxRequest('/login-form', {
            'LoginForm': {
                'username': $('#loginform-username').val(),
                'password': $('#loginform-password').val(),
                'rememberMe': Number($('#loginform-rememberme').is(':checked')),
            }
        }).then(function (result) {
            if (result.status == true) {
                syncReadList(0).then(() => {
                    location.reload();
                });
            } else {
                Notiflix.Notify.Failure(result.message);
            }
            loader(true);
        });
    });

    //forget password feature
    $(document).on('click', 'a[href="/request-password-reset"]', function (e) {
        e.preventDefault();
        Swal.close();
        loader();
        ajaxRequest('/request-password-reset', []).then(function (result) {
            if (result.status == true) {
                Swal.fire({
                    html: result.partial,
                    width: 600,
                    showConfirmButton: false,
                    showCloseButton: true,
                    showCancelButton: false,
                    focusConfirm: false,
                });
                document.querySelectorAll('.form-outline').forEach((formOutline) => {
                    new mdb.Input(formOutline).init();
                });
            } else {
                Notiflix.Notify.Failure(result.message);
            }
            loader(true);
        });
    });
    $(document).on('click', '#password-reset-send-form', function (e) {
        e.preventDefault();
        loader();
        ajaxRequest('/request-password-reset', {
            'PasswordResetRequestForm': {
                'email': $('#passwordresetrequestform-email').val(),
            }
        }).then(function (result) {
            if (result.status == true) {
                Notiflix.Notify.Success(result.message);
            } else {
                Notiflix.Notify.Failure(result.message);
            }
            loader(true);
        });
    });

    //registration feature
    $(document).on('click', 'a[href="/signup"]', function (e) {
        e.preventDefault();
        Swal.close();
        loader();
        ajaxRequest('/signup', []).then(function (result) {
            if (result.status == true) {
                Swal.fire({
                    html: result.partial,
                    width: 600,
                    showConfirmButton: false,
                    showCloseButton: true,
                    showCancelButton: false,
                    focusConfirm: false,
                });
                document.querySelectorAll('.form-outline').forEach((formOutline) => {
                    new mdb.Input(formOutline).init();
                });
            } else {
                Notiflix.Notify.Failure(result.message);
            }
            loader(true);
        });
    });
    $(document).on('click', '#signup-send-form', function (e) {
        e.preventDefault();
        loader();
        ajaxRequest('/signup', {
            'SignupForm': {
                'username': $('#signupform-username').val(),
                'email': $('#signupform-email').val(),
                'password': $('#signupform-password').val(),
            }
        }).then(function (result) {
            if (result.status == true) {
                Notiflix.Notify.Success(result.message);
            } else {
                Notiflix.Notify.Failure(result.message);
            }
            loader(true);
        });
    });

    //show notifications btn
    $(document).on('click', '.notifications-btn-parent', function (e) {
        var click_ = this;
        e.preventDefault();
        loader();
        ajaxRequest('/get-notifications', []).then(function (result) {
            if (result.status == true) {
                $(click_).closest('.has-notify').removeClass('has-notify');
                if (result.message) {
                    Notiflix.Notify.Info(result.message);
                } else {
                    $('.notifications-list ul').html(result.partial);
                    $('.notifications-content').removeClass('d-none');
                }
            } else {
                Notiflix.Notify.Failure(result.message);
            }
            loader(true);
        });
    });
    $(document).on('click', window, function () {
        if (!$('.notifications-content').hasClass('d-none')) {
            $('.notifications-content').addClass('d-none');
        }
    });

    $(document).on('click', '.account-btn-dropdown', function (e) {
        e.preventDefault();
    });

    //add manga to your follows
    $(document).on('click', '.follow-manga', function (e) {
        var click_ = this;
        e.preventDefault();
        loader();
        ajaxRequest('/manga/follow', {
            'id': $(this).attr('data-id')
        }).then(function (result) {
            loader(true);
            if (result.status == true) {
                if (result.type == 1) {
                    Notiflix.Notify.Info(result.message);
                    $(click_).find('.fa-bell').addClass('d-none');
                    $(click_).find('.fa-bell-slash').removeClass('d-none');
                } else if (result.type == 2) {
                    Notiflix.Notify.Success(result.message);
                    $(click_).find('.fa-bell').removeClass('d-none');
                    $(click_).find('.fa-bell-slash').addClass('d-none');
                }
            } else {
                Notiflix.Notify.Failure(result.message);
            }
        });
    })

    //add review to manga
    $(document).on('click', '.add-review', function (e) {
        e.preventDefault();
        loader();
        ajaxRequest('/manga/get-review-form', {
            'id': $(this).attr('data-id')
        }).then(function (result) {
            loader(true);
            if (result.status == true) {
                Swal.fire({
                    html: result.partial,
                    width: 600,
                    showConfirmButton: false,
                    showCloseButton: true,
                    showCancelButton: false,
                    focusConfirm: false,
                });
            } else {
                Notiflix.Notify.Failure(result.message);
            }
        });
    });
    $(document).on('click', '.set-rate', function (e) {
        e.preventDefault();
        var rating = Number($(this).attr('data-rate'));
        $.each($('.set-rate'), function (i, item) {
            var loopRating = Number($(item).attr('data-rate'));
            if (rating >= loopRating) {
                $(item).find('i').addClass('fas');
                $(item).find('i').removeClass('far');
            } else {
                $(item).find('i').removeClass('fas');
                $(item).find('i').addClass('far');
            }
        });
    });
    $(document).on('click', '#review-send-form', function (e) {
        e.preventDefault();
        loader();
        ajaxRequest('/manga/set-review', {
            'manga_id': $(this).attr('manga-id'),
            'review': $('#mangareviews-review').val(),
            'rate': $('.set-rate .fas').length,
        }).then(function (result) {
            loader(true);
            if (result.status == true) {
                Notiflix.Notify.Success(result.message);
                Swal.close();
                $.pjax.reload({
                    container: "#content"
                })
            } else {
                if (result.message) {
                    Notiflix.Notify.Failure(result.message);
                } else if (result.link) {
                    Swal.fire({
                        imageUrl: '/images/site/' + result.link,
                    })
                }
            }
        });
    });

    //change language
    $(document).on('click', '.change-language', function (e) {
        e.preventDefault();
        loader();
        ajaxRequest('/set-language', {
            'id': $(this).attr('data-id'),
            'main_language': $(this).attr('main-language'),
        }).then((result) => {
            loader(true);
            if (result.status == true) {
                location.reload();
            } else {
                Notiflix.Notify.Failure(result.message);
            }
        });
    });

    //search manga
    function searchManga(name, tags = '') {
        loader();
        let url = '/manga/list/1?name=' + name + tags
        $.pjax.reload({
            container: '#content',
            url: url,
        }).then(() => {
            loader(true);
            let input = $('#manga-list #search-form .search-input');
            let text = $(input).val();
            $(input).trigger('focus');
            $(input).val('');
            $(input).val(text);
        })
    }
    $(document).on('submit', '#search-form', function (e) {
        e.preventDefault();
        var tags = '';
        $.each($('.filters .tags input'), function (i, item) {
            if ($(item).is(':checked')) {
                tags = tags + $(item).attr('data-id') + '|';
            }
        });
        tags = tags.slice(0, -1);
        if (tags !== '') {
            searchManga($(this).find('.search-input').val(), '&tags=' + tags);
        } else {
            searchManga($(this).find('.search-input').val());
        }
    });
    $(document).on('submit', '#navbar-fast-search', function (e) {
        e.preventDefault();
        searchManga($(this).find('input').val());
    });
    $(document).on('click', '#manga-list .filter-button', function () {
        if ($('#manga-list .filters').css('display') == 'none') {
            $('#manga-list .filters').fadeIn(150)
        } else {
            $('#manga-list .filters').fadeOut(150)
        }
    });

    $(document).on('click', '.show-more-chapters', function (e) {
        e.preventDefault();
        $(".manga-chapters").animate({"max-height": 99999}, 600);
        $(this).remove();
    });

    $(document).ready(function () {
        loader();
        if ($('#reset-password').length) {
            ajaxRequest('/reset-password', {
                'token': $('#reset-password').attr('data-token')
            }).then(function (result) {
                loader(true);
                if (result.status == true) {
                    Swal.fire({
                        html: result.partial,
                        width: 600,
                        showConfirmButton: false,
                        showCloseButton: true,
                        showCancelButton: false,
                        focusConfirm: false,
                    });
                } else {
                    Notiflix.Notify.Failure(result.message);
                }
            });
        }
    });
    $(document).on('submit', '#reset-password-form', function (e) {
        e.preventDefault();
        loader();
        ajaxRequest('/reset-password', {
            'ResetPasswordForm': {
                'password': $('#resetpasswordform-password').val(),
            },
            'token': $('#reset-password').attr('data-token'),
        }).then(function (result) {
            loader(true);
            if (result.status == true) {
                Notiflix.Notify.Success(result.message);
                Swal.close();
            } else {
                Notiflix.Notify.Failure(result.message);
            }
        });
    });

    $(document).on('submit', '#group-create-form', function (e) {
        e.preventDefault();
        loader();
        ajaxRequest('/group-create-save',
            $(this).serialize()
        ).then(function (result) {
            if (result.status == true) {
                $.pjax({
                    'url': '/group-manage',
                    'container': '#content',
                }).then(function () {
                    Notiflix.Notify.Success(result.message);
                    loader(true);
                });
            } else {
                loader(true);
                Notiflix.Notify.Failure(result.message);
            }
        });
    });

    $(document).on('change', '#select-manga-images-type', function (e) {
        var type = $(this).val();
        var typeElement = $('#images-type-' + type);
        $('.images-type').css('display', 'none');
        if (typeElement.length) {
            $(typeElement).fadeIn(300)
        }
    });

    $(document).on('focusout', '#manga-imgur-images', function () {
        let this_ = this;
        if ($(this).val() !== '') {
            ajaxRequest('/imgur-check-images',
                {'url': $(this).val()}
            ).then(function (result) {
                $('#images-type-2').find('span').html(result.message);
                if (result.status == true) {
                    $('#images-type-2').find('span').removeClass('text-danger');
                    $('#images-type-2').find('span').addClass('text-success');
                } else {
                    $('#images-type-2').find('span').removeClass('text-success');
                    $('#images-type-2').find('span').addClass('text-danger');
                }
            });
        } else {
            $('#images-type-2').find('span').removeClass('text-danger');
            $('#images-type-2').find('span').removeClass('text-success');
            $('#images-type-2').find('span').html('');
        }
    });

    $(document).on('submit', '#comments-form', function (e) {
        e.preventDefault();
        loader();
        ajaxRequest('/comments/add',
            $(this).serialize()
        ).then(function (result) {
            loader(true);
            if (result.status == true) {
                Notiflix.Notify.Success(result.message);
                $('#comments-form').find('textarea').val('');
                $.pjax.reload({
                    'container': '#comment-list'
                })
            } else {
                Notiflix.Notify.Failure(result.message);
            }
        });
    });

    $(document).on('click', '.rate-comment', function (e) {
        e.preventDefault();
        loader();
        ajaxRequest('/comments/rate', {
            'id': $(this).attr('data-id'),
            'rate': $(this).attr('data-rate'),
        }).then(function (result) {
            loader(true);
            if (result.status == true) {
                Notiflix.Notify.Success(result.message);
                $.pjax.reload({
                    'container': '#comment-list'
                })
            } else {
                Notiflix.Notify.Failure(result.message);
            }
        });
    });

    $(document).on('click', '.comment-reply', function (e) {
        e.preventDefault();
        let $searchCommentForm = $(this).parent().find('#comments-form');
        if(!$searchCommentForm.length) {
            let clone = $(this).closest('.manga-comments').find('#comments-form').clone();
            $(this).closest('.rating-section').append(clone);
            $(clone).addClass('mt-3');
            $(clone).find('textarea').val('@' + $(this).closest('.comment-parent').attr('data-user') + ' ');
            let parent_id = $(this).closest('.comment-parent').attr('data-id');
            $(clone).find('input[name="parent_id"]').val(parent_id);
            document.querySelectorAll('.form-outline').forEach((formOutline) => {
                new mdb.Input(formOutline).init();
            });
        } else {
            $searchCommentForm.remove();
        }
    });

    $(document).on('click', '.remove-from-group', function (e) {
        e.preventDefault();
        Swal.fire({
            title: translate.modalConfirm,
            showCancelButton: true,
            confirmButtonText: translate.yes,
            cancelButtonText: translate.cancel,
        }).then((result) => {
            if (result.isConfirmed) {
                loader();
                ajaxRequest('/group/remove', {
                    'id': $(this).attr('data-id'),
                }).then(function (result) {
                    loader(true);
                    if (result.status == true) {
                        Notiflix.Notify.Success(result.message);
                        $.pjax.reload({
                            'container': '#group-manage'
                        })
                    } else {
                        Notiflix.Notify.Failure(result.message);
                    }
                });
            }
        });
    });

    $(document).on('click', '.invite-to-group', function (e) {
        e.preventDefault();
        Swal.fire({
            input: 'text',
            title: translate.modalInviteToGroup,
            showCancelButton: true,
            confirmButtonText: translate.submit,
            cancelButtonText: translate.cancel,
        }).then((result) => {
            if (result.isConfirmed) {
                loader();
                ajaxRequest('/group/invite', {
                    'username': result.value,
                }).then(function (result) {
                    loader(true);
                    if (result.status == true) {
                        Notiflix.Notify.Success(result.message);
                        $.pjax.reload({
                            'container': '#group-manage'
                        })
                    } else {
                        Notiflix.Notify.Failure(result.message);
                    }
                });
            }
        });
    });

    $(document).on('click', '.cancel-invitation', function (e) {
        e.preventDefault();
        loader();
        ajaxRequest('/group/cancel-invitation', {
            'id': $(this).attr('data-id'),
        }).then(function (result) {
            loader(true);
            if (result.status == true) {
                Notiflix.Notify.Success(result.message);
                $.pjax.reload({
                    'container': '#group-manage'
                })
            } else {
                Notiflix.Notify.Failure(result.message);
            }
        });
    });

    $(document).on('click', 'a, button', function (e) {
        if ($(this).hasClass('image-preview') && $(this).attr('data-preview') != '') {
            e.preventDefault();
            Swal.fire({
                imageUrl: $(this).attr('data-preview'),
            })
        }
    })

    $(document).on('change', 'input[type="file"]', function (e) {
        let this_ = this;
        if ($(this).parent().hasClass('custom-file-upload')) {
            let reader = new FileReader();
            reader.onload = function () {
                $(this_).parent().find('.image-preview-file').attr('data-preview', reader.result);
            }
            reader.readAsDataURL(e.target.files[0]);
        }
    });

    $(document).on('submit', '#group-manage-settings-form', function (e) {
        e.preventDefault();
        loader();
        ajaxRequest('/group/settings-save',
            $(this).serialize() +
            '&header-img=' + $('.header-image').parent().find('.image-preview-file').attr('data-preview') +
            '&footer-img=' + $('.footer-image').parent().find('.image-preview-file').attr('data-preview')
        ).then(function (result) {
            loader(true);
            if (result.status == true) {
                Notiflix.Notify.Success(result.message);
                $.pjax.reload({
                    'container': '#group-manage'
                })
            } else {
                Notiflix.Notify.Failure(result.message);
            }
        });
    });

    // scroll to top button
    $(document).on('click', '#scroll-to-top', () => {
        $('html, body').animate({
            scrollTop: 0
        }, 200);
    });

    // read status feature
    const setReadStatus = async (status, chapterID) => {
        //getting read list
        let readList = Cookies.get('readList');
        if(typeof readList === 'undefined') {
            readList = createReadList();
        }
        readList = JSON.parse(readList);

        //adding chapter to read list
        if(status) {
            if(!readList.list.includes(chapterID)) {
                readList.list.push(chapterID)
            } else {
                return !status;
            }
        } else {
            const index = readList.list.indexOf(chapterID);
            if (index > -1) {
                readList.list.splice(index, 1);
            }
        }

        //saving changes
        readList.lastUpdate = Math.floor(Date.now() / 1000);
        readList = JSON.stringify(readList);
        Cookies.set('readList', readList, {sameSite: 'strict'});
        return status;
    }
    const createReadList = () => {
        return JSON.stringify({
            lastUpdate: Math.floor(Date.now() / 1000),
            lastSync: null,
            list: []
        });
    }
    $(document).on('click', '.read-status', (e) => {
        e.preventDefault();
        e.stopPropagation();
        let el = $(e.currentTarget);
        let status = !Number(el.attr('data-status'));

        setReadStatus(status, $(e.currentTarget).attr('chapter-id')).then((rStatus) => {
            el.attr('data-status', Number(rStatus));
            syncReadList(1);
        });
    });
    $(document).on('click', '.read-chapter', (e) => {
        setReadStatus(1, $(e.currentTarget).attr('chapter-id'));
    });
    const initReadList = () => {
        let readList = Cookies.get('readList');
        if(typeof readList !== 'undefined') {
            readList = JSON.parse(readList).list;
            $('.read-status').each((i, item) => {
                let chapterID = $(item).attr('chapter-id');
                if (readList.includes(chapterID)) {
                    $(item).attr('data-status', 1);
                }
            });
        }
    }
    //status = 0 download, 1 - upload
    const syncReadList = async (status = 0) => {
        if(status === 0) {
            return ajaxRequest('/manga/sync-read-list', {
                'status': status
            }).then((result) => {
                if(!result.status && typeof result.message !== 'undefined') {
                    Notiflix.Notify.Failure(result.message);
                } else {
                    let readList;
                    if(typeof Cookies.get('readList') !== 'undefined') {
                        readList = JSON.parse(Cookies.get('readList'));
                    } else {
                        readList = {};
                    }
                    readList.list = JSON.parse(result.data);
                    Cookies.set('readList', JSON.stringify(readList), {sameSite: 'strict'});
                }
            });
        } else if(status === 1) {
            if (typeof Cookies.list !== 'undefined') {
                createReadList();
            }
            let list = JSON.parse(Cookies.get('readList')).list;
            return ajaxRequest('/manga/sync-read-list', {
                'list': JSON.stringify(list),
                'status': status
            }).then((result) => {
                if(!result.status && typeof result.message !== 'undefined') {
                    Notiflix.Notify.Failure(result.message);
                }
            });
        }
    }

}
App();