var notifications = {
    'error': [],
    'success': [],
};

$(document).ready(function() {
    $.each(notifications.error, function(i, item) {
        Notiflix.Notify.Failure(item);
    });
    $.each(notifications.success, function(i, item) {
        Notiflix.Notify.Success(item);
    });
});