<?php
/* @var $manga Array */
/* @var $tags Array */
/* @var $comments Array */
use yii\widgets\Pjax;

$this->title = $manga['name'] . ' ' . Yii::t('app', 'Manga');
?>
<?php Pjax::begin(['id'=>'content']); ?>
<h1 class="w-100 text-center text-light mt-5">
    <?= $manga['name'] ?>
</h1>
<img src="/images/site/carousel1.jpg" class="manga-background">
<div class="container mt-5 mb-5">
    <div class="row section p-3 manga-page bg-white shadow rounded-3">
        <div class="col-md-auto">
            <img src="/images/upload/manga/<?= $manga['image'] ?>"
                class="w-100 rounded">
        </div>
        <div class="col-md-8">
            <div class="stars">
                <h3>
                    <?php
                        $rating = $manga['rating'] / 2;
                        echo Yii::$app->controller->renderPartial('/manga/partials/stars', ['rating' => $rating])
                    ?>
                </h3>
            </div>
            <div class="informations">
                <?= Yii::$app->controller->renderPartial('partials/informations', ['manga' => $manga, 'tags' => $tags]) ?>
            </div>
            <div class="buttons mt-5">
                <?php if (!Yii::$app->user->isGuest): ?>
                <button type="button" class="btn btn-primary follow-manga"
                    data-id="<?= $manga['id'] ?>">
                    <i class="far fa-bell-slash <?= empty($manga['follow']) ? '' : 'd-none' ?>"></i>
                    <i class="fas fa-bell <?= !empty($manga['follow']) ? '' : 'd-none' ?>"></i>
                </button>
                <?php endif; ?>
                <button type="button" class="btn btn-primary show-description">
                    <?= Yii::t('app', 'Opis') ?>
                </button>
                <?php if (!Yii::$app->user->isGuest): ?>
                <button type="button" class="btn btn-primary add-review"
                    data-id="<?= $manga['id'] ?>">
                    <?= Yii::t('app', 'Oceń mange') ?>
                </button>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <div class="row mt-5 section p-3 manga-description card" style="display: none;">
        <span class="d-block">
            <h3>
                <?= Yii::t('app', 'Opis') ?>
                <?= $manga['name'] ?>
            </h3>
            <p>
                <?= $manga['description'] ?>
            </p>
        </span>
    </div>
    <?php if (!empty($chapters)): ?>
    <div class="row mt-5 justify-content-between">
        <a class="col-md-6 mw-49 btn btn-primary p-3 text-center" data-pjax
            href="/read/<?= $chapters[count($chapters)-1]['manga_id'] ?>/<?= $chapters[count($chapters)-1]['id'] ?>/<?= \app\models\Functions::mangaNameUrl($manga['name']) ?>">
            <?= Yii::t('app', 'Pierwszy chapter') ?>
        </a>
        <a class="col-md-6 btn mw-49 btn-primary p-3 text-center" data-pjax
            href="/read/<?= $chapters[0]['manga_id'] ?>/<?= $chapters[0]['id'] ?>/<?= \app\models\Functions::mangaNameUrl($manga['name']) ?>">
            <?= Yii::t('app', 'Najnowszy chapter') ?>
        </a>
    </div>
    <?php endif; ?>
    <div class="row mt-5 section manga-chapters mb-3 shadow rounded-3" style="max-height: 495px;overflow: hidden;transition: .8s;">
        <?= Yii::$app->controller->renderPartial('partials/chapters', ['chapters' => $chapters, 'manga' => $manga]) ?>
    </div>
    <?php if (count($chapters) > 6): ?>
    <div clsas="row" style="text-align: center;">
        <a href="#" class="btn btn-primary mb-3 show-more-chapters"><?= Yii::t('app', 'Pokaż więcej') ?></a>
    </div>
    <?php endif; ?>
    <div class="row mt-5 section manga-comments">
        <?= Yii::$app->controller->renderPartial('../comments/comments', ['manga' => $manga, 'comments' => $comments]) ?>
    </div>
</div>
<?php Pjax::end();
