<?php
/* @var $rating int */

for ($i2 = 1; $i2 <= 5; $i2++):
    if ($i2 == round($rating, 0) && strpos($rating, '.')):
        echo '<i class="fas fa-star-half-alt"></i>';
    else:
        if ($rating >= $i2):
            echo '<i class="fas fa-star"></i>';
        else:
            echo '<i class="far fa-star"></i>';
        endif;
    endif;
endfor;