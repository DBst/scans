<?php
/* @var $manga Array */
?>
<a href="/manga/<?= $manga['id'] ?>/<?= \app\models\Functions::mangaNameUrl($manga['name']) ?>"
   data-pjax class="horizontal-card d-flex card mt-2 row">
    <div class="col-auto ps-1 pe-0">
        <img src="/images/upload/manga/<?= $manga['image'] ?>"
             class="background rounded">
    </div>
    <div class="d-grid col-9">
        <h4 class="m-0">
            <?= $manga['name'] ?>
        </h4>
        <span class="stars">
            <h5>
                <?php
	                $rating = $manga['rating'] / 2;
					echo Yii::$app->controller->renderPartial('/manga/partials/stars', ['rating' => $rating]);
				?>
            </h5>
        </span>
        <span>
            <?= Yii::t('app', '{0} wyświetleń', $manga['views']) ?>
        </span>
    </div>
</a>