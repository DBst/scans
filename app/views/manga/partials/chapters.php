<?php
/* @var $chapter Object */
/* @var $manga Object */
if (!empty($chapters)): ?>
<div class="chapters container-fluid card shadow rounded-3">
    <div class="chapter p-2" chapter-id="">
        <?php
        $last_chapter_number = 0;
        foreach ($chapters as $chapter):
            if ($chapter['chapter_number'] !== $last_chapter_number) {
                $last_chapter_number = $chapter['chapter_number'];
                echo '<h5 class="p-0">Chapter ' . $chapter['chapter_number'] . '</h5>';
            }
        ?>
        <a href="/read/<?= $chapter['manga_id'] ?>/<?= $chapter['id'] ?>/<?= \app\models\Functions::mangaNameUrl($manga['name']) ?>"
            class="row justify-content-space-between read-chapter bg-light rounded-3 text-dark"
            chapter-id="<?= $chapter['id'] ?>" data-pjax>
            <span class="col-md-6 m-0 p-0">
                <span class="read-status" data-status="0"
                    chapter-id="<?= $chapter['id'] ?>">
                    <i class="fa-regular fa-eye ms-1"></i>
                    <i class="fa-regular fa-eye-slash text-secondary ms-1"></i>
                </span>
                <img src="/images/site/country-flags/<?= Yii::$app->init->country_flags[$chapter['language']] ?>.jpg"
                    class="ms-1 mb-1" width="24">
                <span><?= $chapter['chapter_name'] ?></span>
            </span>
            <span class="col-md-6 m-0 p-0 pe-2 text-end">
                <?php
                if (!empty($chapter['group_name'])) { ?>
                <svg viewBox="0 0 24 24" width="16" height="16" stroke="currentColor" stroke-width="2" fill="none"
                    stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1">
                    <path d="M17 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path>
                    <circle cx="9" cy="7" r="4"></circle>
                    <path d="M23 21v-2a4 4 0 0 0-3-3.87"></path>
                    <path d="M16 3.13a4 4 0 0 1 0 7.75"></path>
                </svg>
                <?= $chapter['group_name'] ?>,
                <?php } ?>
                <svg viewBox="0 0 24 24" width="16" height="16" stroke="currentColor" stroke-width="2" fill="none"
                    stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1">
                    <path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path>
                    <circle cx="12" cy="7" r="4"></circle>
                </svg>
                <?= $chapter['username'] ?>,
                <?= Yii::$app->formatter->asRelativeTime($chapter['added_at']) ?>
            </span>
        </a>
        <?php endforeach; ?>
    </div>
</div>
<?php else: ?>
<span class="text-center w-100 card">
    <?= Yii::t('app', 'Brak chapterów dla tej mangi') ?>
</span>
<?php endif;
