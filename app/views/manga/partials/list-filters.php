<?php
/* @var $tags array */
?>
<div class="row tags m-0">
    <?php
    $selectedTags = Yii::$app->request->get('tags');
    if (!empty($selectedTags)) {
        $selectedTags = explode('|', $selectedTags);
    } else {
        $selectedTags = [];
    }
    foreach ($tags as $k => $tag): ?>
    <div class="col-md-3 pt-1 pb-1">
        <div class="form-check">
            <input type="checkbox" class="form-check-input"
                id="filterTag<?=$tag->id?>"
                data-id="<?=$tag->id?>" <?= in_array($tag->id, $selectedTags) ? 'checked' : '' ?>>
            <label class="form-check-label"
                for="filterTag<?=$tag->id?>">
                <?= $tag->name ?>
            </label>
        </div>
    </div>
    <?php endforeach; ?>
</div>