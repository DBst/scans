<?php
/* @var $manga Array */
/* @var $tags Array */
?>
<table>
    <tbody>
        <tr>
            <th>
                <?= Yii::t('app', 'Średnia ocena') ?>
            </th>
            <td>
                <?= $manga['rating'] ?>/10 (<?=$manga['reviews_count']?>)
            </td>
        </tr>
        <tr>
            <th>
                <?= Yii::t('app', 'Wyświetlenia') ?>
            </th>
            <td>
                <?= $manga['views'] ?>
            </td>
        </tr>
        <tr>
            <th>
                <?= Yii::t('app', 'Typ') ?>
            </th>
            <td>
                <?= \app\models\Functions::getMangaType($manga['type']) ?>
            </td>
        </tr>
        <tr>
            <th>
                <?= Yii::t('app', 'Tagi') ?>
            </th>
            <td>
                <?php
                if (!empty($tags)) {
                    foreach ($tags as $k => $tag):
                        echo '<a class="text-dark" href="/manga/list?tags=' . $tag['tag_id'] . '" data-pjax>' . Yii::t('app', $tag['name']) . '</a>';
                    if ((int) $k !== (int) count((array)$tags)-1) {
                        echo ', ';
                    }
                    endforeach;
                } else {
                    echo '-';
                }
                ?>
            </td>
        </tr>
        <tr>
            <th>
                <?= Yii::t('app', 'Liczba chapterów') ?>
            </th>
            <td>
                <?= $manga['chapters_count'] ?>
            </td>
        </tr>
        <tr>
            <th>
                <?= Yii::t('app', 'Data dodania') ?>
            </th>
            <td>
                <?= \app\models\Functions::unixToDate($manga['added_at']) ?>
            </td>
        </tr>
        <tr>
            <th>
                <?= Yii::t('app', 'Data aktualizacji') ?>
            </th>
            <td>
                <?= \app\models\Functions::unixToDate($manga['updated_at']) ?>
            </td>
        </tr>
    </tbody>
</table>