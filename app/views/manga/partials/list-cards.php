<?php
/* @var $manga_list array */
foreach ($manga_list as $k => $manga):
	echo Yii::$app->controller->renderPartial('/manga/partials/card', ['manga' => $manga, 'k' => $k, 'slider' => false, 'col' => 'col-lg-2']);
endforeach;
