<?php
/* @var $page int */
/* @var $limit int */
/* @var $manga_count int */
$baseUrl =  "//{$_SERVER['HTTP_HOST']}/manga/list/{{page}}?{$_SERVER['QUERY_STRING']}"; ?>
<div class="text-center w-100 mt-4">
	<?php if($page > 1): ?>
		<a href="<?= str_replace('{{page}}', $page-1, $baseUrl) ?>" data-pjax="" class="btn btn-primary"><</a>
	    <?php if($page > 2): ?>
			<a href="<?= str_replace('{{page}}', $page-2, $baseUrl) ?>" data-pjax="" class="btn btn-primary"><?= $page-2 ?></a>
        <?php endif; ?>
		<a href="<?= str_replace('{{page}}', $page-1, $baseUrl) ?>" data-pjax="" class="btn btn-primary"><?= $page-1 ?></a>
	<?php endif; ?>
	<a href="#" data-pjax="" class="btn btn-primary disabled"><?= $page ?></a>
	<?php if($manga_count / ($page * $limit) > 1): ?>
		<a href="<?= str_replace('{{page}}', $page+1, $baseUrl) ?>" data-pjax="" class="btn btn-primary"><?= $page+1 ?></a>
	    <?php if($manga_count / ($page * $limit) > 2): ?>
			<a href="<?= str_replace('{{page}}', $page+2, $baseUrl) ?>" data-pjax="" class="btn btn-primary"><?= $page+2 ?></a>
        <?php endif; ?>
		<a href="<?= str_replace('{{page}}', $page+1, $baseUrl) ?>" data-pjax="" class="btn btn-primary">></a>
	<?php endif; ?>
</div>