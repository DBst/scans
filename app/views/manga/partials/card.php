<?php
/* @var $manga Array */
/* @var $col string */
/* @var $slider bool */
/* @var $k int */
?>
<span class="p-1 <?= $col ?> col-6 <?= $slider ? 'swiper-slide' : '' ?>">
	<a href="/manga/<?= $manga['id'] ?>/<?= \app\models\Functions::mangaNameUrl($manga['name']) ?>"
	   data-pjax="" id="mp-card-id-<?=$k?>" class="manga-card">
		<div class="manga-image" style="background-image: url('/images/upload/manga/<?= $manga['image'] ?>"')"></div>
	    <div class="stars">
	        <h5>
	            <?php
		            $rating = $manga['rating'] / 2;
	                echo Yii::$app->controller->renderPartial('/manga/partials/stars', ['rating' => $rating]);
	            ?>
	        </h5>
	    </div>
	    <p class="hide-title">
	        <?= $manga['name'] ?>
	    </p>
	    <p class="chapters-count bg-primary rounded">
	        <?= $manga['chapters_count'] ?>
	    </p>
	    <div class="additional-info d-none">
	        <h3>
	            <?= $manga['name'] ?>
	        </h3>
	        <p>
	            <?= $manga['description'] ?>
	        </p>
	    </div>
	</a>
</span>