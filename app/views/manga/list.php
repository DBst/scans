<?php
/* @var $manga_count integer */
/* @var $page integer */
/* @var $tags Array */
/* @var $manga_list Array */

use yii\widgets\Pjax;

$this->title = Yii::t('app', 'Lista Mang');
?>
<?php Pjax::begin(['id'=>'content']); ?>

<div class="container mt-4 mb-4" id="manga-list">
    <div class="row section">
        <div class="col-md-12 p-0">

	        <div class="container-fluid">
            <div class="row justify-content-center">
                <form id="search-form" class="col-md-8 row mt-5 mb-3 p-0">
                    <div class="col-lg-2 col-3 text-end">
                        <button type="submit" class="search-button btn btn-primary">
                            <i class="fas fa-search"></i>
                        </button>
                    </div>
	                <div class="form-outline col-lg-8 col-6">
		                <input type="text" id="form12" class="form-control search-input" />
		                <label class="form-label" for="form12">
                            <?= Yii::t('app', 'Szukaj...') ?>
		                </label>
	                </div>
                    <div class="col-lg-2 col-3">
                        <button type="button" class="filter-button btn btn-primary">
                            <i class="fas fa-filter"></i>
                        </button>
                    </div>
                </form>
            </div>
            <div class="row filters w-100" <?= empty(Yii::$app->request->get('tags')) ? 'style="display: none;"' : '' ?>>
                <?= Yii::$app->controller->renderPartial('partials/list-filters', ['tags' => $tags]) ?>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?php if (!empty(Yii::$app->request->get('name'))): ?>
                    <h4><?= $manga_count ?>
                        <?= Yii::t('app', 'wyników dla: {0}', Yii::$app->request->get('name')) ?>
                    </h4>
                    <?php else: ?>
                    <h4>
                        <?= Yii::t('app', 'Znaleziono {0} wyników, wyświetlono {1} wyników', [$manga_count,  count($manga_list)]) ?>
                    </h4>
                    <?php endif; ?>
                </div>
                <div class="col-md-12 mt-4 manga-cards">
	                <div class="row">
	                    <?= Yii::$app->controller->renderPartial('partials/list-cards', ['manga_list' => $manga_list]) ?>
	                    <?= Yii::$app->controller->renderPartial('partials/list-pagination', ['page' => $page, 'manga_count' => $manga_count, 'limit' => $limit]) ?>
	                </div>
                </div>
            </div>
            </div>
        </div>
    </div>
</div>
<?php Pjax::end();
