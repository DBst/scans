<?php
/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */
/* @var $mangaList array */
/* @var $maxFileSize int */

use yii\widgets\Pjax;

$this->title = Yii::t('app', 'Dodaj Chapter');
?>
<?php Pjax::begin(['id'=>'content']); ?>
<div class="container upload-chapter mt-4 mb-4 custom-form-style">
    <div class="row section justify-content-center pb-4">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <input type="hidden" name="maxFileSize" value="<?= $maxFileSize ?>">
                <div class="w-100 mt-3 mb-4">
                    <h2 class="text-center">
                        <?= Yii::t('app', 'Dodaj Chapter') ?>
                    </h2>
                </div>
                <?php
                    $verified = \common\models\User::isVerified();
                ?>
                <div
                    class="w-100 text-dark mb-3 <?= !$verified ? 'd-none' : '' ?>">
                    <div class="form-group w-100 mb-2">
                        <select class="form-control" id="select-manga-images-type">
                            <option value="">
                                <?= Yii::t('app', 'Wybierz tryb dodawania zdjęć') ?>
                            </option>
                            <option value="2">
                                    <?= Yii::t('app', 'Dodawanie z imgur') ?>
                            </option>
                            <option value="3" <?= !$verified ? 'selected' : '' ?>>
                                <?= Yii::t('app', 'Dodawanie z dysku') ?>
                            </option>
                        </select>
                    </div>
                </div>
                <div class="w-100 text-dark justify-content-center images-type mb-3" id="images-type-2"
                    style="display: none;">
                    <div class="form-group w-100 mb-2">
                        <input type="text" class="form-control" id="manga-imgur-images"
                            placeholder="<?= Yii::t('app', 'Link do galerii zdjęć z imgur') ?>">
                    </div>
                    <span class="text-center"></span>
                </div>
                <div class="w-100 images-type" id="images-type-3" <?= $verified ? 'style="display: none;"' : '' ?>>
    <!--                <form class="dropzone needsclick mb-4" id="upload-chapter" action="/chapter/upload">-->
    <!--                    <div class="dz-message needsclick">-->
    <!--                        <svg viewBox="0 0 24 24" width="128" height="128" stroke="currentColor" stroke-width="2"-->
    <!--                            fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1">-->
    <!--                            <polyline points="16 16 12 12 8 16"></polyline>-->
    <!--                            <line x1="12" y1="12" x2="12" y2="21"></line>-->
    <!--                            <path d="M20.39 18.39A5 5 0 0 0 18 9h-1.26A8 8 0 1 0 3 16.3"></path>-->
    <!--                            <polyline points="16 16 12 12 8 16"></polyline>-->
    <!--                        </svg><br>-->
    <!--                        <h4>--><?//= Yii::t('app', 'Wrzuć tutaj zdjęcia')?><!--!-->
    <!--                        </h4>-->
    <!--                        <div class="note needsclick">(--><?//= Yii::t('app', 'Maksymalny rozmiar jednego zdjęcia') ?>
    <!--                            --><?//= $maxFileSize ?><!--MB)</div>-->
    <!--                    </div>-->
    <!--                </form>-->
                    <input type="file" id="chapter-images"
                           class="filepond"
                           name="filepond"
                           multiple
                           data-allow-reorder="true"
                           data-max-file-size="20MB">
                </div>
                <div class="w-100 text-dark justify-content-center">
                    <div class="form-group w-100 mb-3">
                        <select class="form-control" id="select-manga" placeholder="<?= Yii::t('app', 'Wpisz tytuł mangi...') ?>">
                            <option value=""><?= Yii::t('app', 'Wybierz mangę') ?>
                            </option>
                            <?php foreach ($mangaList as $manga): ?>
                            <option value="<?= $manga->id ?>"><?= $manga->name ?>
                            </option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-outline w-100 mb-3">
                        <input type="number" class="form-control" id="manga-chapter-number">
                        <label class="form-label" for="manga-chapter-number">
                            <?= Yii::t('app', 'Numer chaptera') ?>
                        </label>
                    </div>
                    <div class="form-group w-100 mb-3">
                        <select class="form-control" id="select-manga-language">
                            <option value="">
                                <?= Yii::t('app', 'Wybierz język') ?>
                            </option>
                            <?php foreach (Yii::$app->init->languages as $language): ?>
                            <option value="<?= $language->id ?>">
                                <?= strtoupper($language->shortname) ?>
                            </option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-outline w-100 mb-3">
                        <input type="text" class="form-control" id="manga-chapter-name">
                        <label class="form-label" for="manga-chapter-name">
                            <?= Yii::t('app', 'Nazwa chaptera') ?>
                        </label>
                    </div>
                    <div class="form-group w-100 text-center mt-4">
                        <button type="button" class="form-control btn btn-primary w-50" id="manga-save"><?= Yii::t('app', 'Dodaj') ?></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php Pjax::end();
