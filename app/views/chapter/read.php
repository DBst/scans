<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */
/* @var $chapter Object */
/* @var $images Array */

use yii\widgets\Pjax;

$this->title = $chapter->manga->name . ' ' . Yii::t('app', 'chapter') . ' ' . $chapter->chapter_number;
?>
<?php Pjax::begin(['id'=>'content']); ?>
<!-- chapter images -->
<div class="text-center reader position-relative mt-5">
    <?php foreach ($images as $image): ?>
    <div class="chapter-image chapter-image-loading position-relative">
        <img src="<?= $image->link ?>">
	    <div class="image-loader"><div></div><div></div><div></div><div></div></div>
    </div>
    <?php endforeach; ?>
</div>
<!-- end chapter images -->
<div class="container mt-5 mb-5">
<!-- pagination -->
	<div class="row mt-5 mb-5 justify-content-between">
		<div class="col-md-3 p-0">
			<?php if(!empty($prevChapterId)): ?>
			<a href="/read/<?= $chapter->manga_id ?>/<?= $prevChapterId ?>/<?= \app\models\Functions::mangaNameUrl($chapter->manga->name) ?>"
			   data-pjax="" class="btn btn-primary w-100">
				<?= Yii::t('app', 'Poprzedni chapter') ?>
			</a>
			<?php endif; ?>
		</div>
		<div class="col-md-6">
			<a href="/manga/<?= $chapter->manga_id ?>/<?= \app\models\Functions::mangaNameUrl($chapter->manga->name) ?>"
			   data-pjax="" class="btn btn-primary w-100">
				<?= Yii::t('app', 'Wróć do mangi') ?>
			</a>
		</div>
		<div class="col-md-3 p-0">
			<?php if(!empty($nextChapterId)): ?>
				<a href="/read/<?= $chapter->manga_id ?>/<?= $nextChapterId ?>/<?= \app\models\Functions::mangaNameUrl($chapter->manga->name) ?>"
				   data-pjax="" class="btn btn-primary w-100">
					<?= Yii::t('app', 'Następny chapter') ?>
				</a>
			<?php endif; ?>
		</div>
	</div>
<!-- end pagination -->
<!-- comments section -->
    <div class="row mt-5 section manga-comments">
        <?= Yii::$app->controller->renderPartial('../comments/comments', ['chapter' => !empty($chapter) ? $chapter : null, 'manga' => $chapter->manga, 'comments' => !empty($comments) ? $comments : null]) ?>
    </div>
<!-- end comments section -->
</div>

<?php Pjax::end();
