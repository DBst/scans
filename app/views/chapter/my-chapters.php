<?php
    use yii\widgets\Pjax;

?>
<?php Pjax::begin(['id'=>'content']); ?>
<div class="container">
    <div class="row mt-5 mb-5">
        <div class="col-md-12 section p-3">
            <div class="w-100 text-center">
                <h3>
                    <?= Yii::t('app', 'Moje chaptery') ?>
                </h3>
            </div>
            <?php
                $offset = !empty(Yii::$app->request->get('offset')) ? (int) Yii::$app->request->get('offset') : 0;
            ?>
            <table class="table custom-table text-white mt-5 text-center">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col"><?= Yii::t('app', 'Zdjęcie') ?>
                        </th>
                        <th scope="col"><?= Yii::t('app', 'Manga') ?>
                        </th>
                        <th scope="col"><?= Yii::t('app', 'Dodane') ?>
                        </th>
                        <th scope="col"><?= Yii::t('app', 'Wyświetlenia') ?>
                        </th>
                        <th scope="col"><?= Yii::t('app', 'Status') ?>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($chapters as $k => $chapter): ?>
                    <?php if ($k !== 10): ?>
                    <tr>
                        <th scope="row"><?= $offset + (int) $k + 1 ?>
                        </th>
                        <td><img src="/images/upload/manga/<?= $chapter->manga->image ?>"
                                style="max-height: 75px"></td>
                        <td><?= $chapter->manga->name ?>
                        </td>
                        <td><?= Yii::$app->formatter->asRelativeTime($chapter->added_at) ?>
                        </td>
                        <td><?= $chapter->views ?>
                        </td>
                        <td><?= $chapter::getStatusName($chapter->status) ?>
                        </td>
                    </tr>
                    <?php endif; ?>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <div class="row">
                <div class="col-md-6 mt-2 mb-2">
                    <?php if (!empty($offset)): ?>
                    <a class="btn btn-primary ms-2"
                        href="/chapter/my-chapters?offset=<?= $offset - 10 < 0 ? 0 : $offset - 10 ?>"
                        data-pjax>
                        <?= Yii::t('app', 'Poprzednia strona') ?>
                    </a>
                    <?php endif; ?>
                </div>
                <div class="col-md-6 text-end mt-2 mb-2">
                    <?php if (count($chapters) > 10): ?>
                    <a class="btn btn-primary me-2"
                        href="/chapter/my-chapters?offset=<?= $offset + 10 ?>"
                        data-pjax>
                        <?= Yii::t('app', 'Następna strona') ?>
                    </a>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php Pjax::end();
