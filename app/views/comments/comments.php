<?php
    use yii\widgets\Pjax;
?>
<div class="col-md-12 p-3 text-center">
    <h4>
        <?= Yii::t('app', 'Komentarze')?>
    </h4>
</div>
<div class="col-md-12 p-3">
    <?= Yii::$app->controller->renderPartial('../comments/comment-form', ['manga' => !empty($manga) ? $manga : null, 'chapter' => !empty($chapter) ? $chapter : null]) ?>
</div>
<div class="col-md-12 comment-list">
    <?php Pjax::begin(['id'=>'comment-list']); ?>
        <?php if(!empty($comments)): ?>
            <?php foreach($comments as $comment): ?>
                <?= Yii::$app->controller->renderPartial('../comments/comment', ['comment' => $comment, 'child' => false]) ?>
            <?php endforeach; ?>
        <?php endif; ?>
    <?php Pjax::end();?>
</div>