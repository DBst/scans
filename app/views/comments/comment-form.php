<?php
/* @var $manga Array */
/* @var $chapter Array */
?>
<form id="comments-form">
    <input type="hidden" name="manga_id" value="<?= !empty($manga) ? $manga['id'] : '' ?>">
    <input type="hidden" name="chapter_id" value="<?= !empty($chapter) ? $chapter['id'] : '' ?>">
    <input type="hidden" name="parent_id" value="">
	<div class="form-outline">
		<textarea class="form-control w-100" id="comment" name="comment" rows="3"></textarea>
		<label class="form-label" for="comment">
			<?= Yii::t('app', 'Treść komentarza') ?>
		</label>
	</div>
    <div class="row mt-3">
        <div class="col-md-10">

        </div>
        <div class="col-md-2">
            <button type="submit" class="btn btn-primary w-100"><?= Yii::t('app', 'Dodaj')?></button>
        </div>
    </div>
</form>