<?php
/* @var $child bool */
/* @var $comment Object */
?>
<?php if (!$child): ?>
<div class="comment-parent" data-id="<?= $comment->id ?>"
    data-user="<?= $comment->user->username ?>">
    <?php endif; ?>
    <div class="comment shadow rounded-3 p-1 mt-2 <?= $child ? 'ms-4' : ''?> row"
        data-id="<?= $comment->id ?>">
	    <div class="col-auto pe-1">
	        <img class="avatar rounded-pill mt-2"
	            src="/images/upload/avatars/<?= $comment->added_by ?>.jpg" height="60">
	    </div>
	    <div class="col">
	        <p class="user-info mt-2 mb-0">
	            <?= $comment->user->username ?>, <?= Yii::$app->formatter->asRelativeTime($comment->added_at) ?>
	        </p>
	        <div class="content">
	            <div class="text text-dark mb-1 mt-1">
	                <?= $comment->comment ?>
	            </div>
	            <div class="rating-section">
	                <a href="javascript:void(0)" class="rate-comment me-1 text-success"
	                    data-id="<?=$comment->id?>" data-rate="plus">
	                    <i class="fas fa-thumbs-up"></i>
	                </a>
	                <span class="me-1"><?= $comment->rate ?></span>
	                <a href="javascript:void(0)" class="rate-comment me-1 text-danger"
	                    data-id="<?=$comment->id?>" data-rate="minus">
	                    <i class="fas fa-thumbs-down"></i>
	                </a>
	                <a href="javascript:void(0)" class="comment-reply me-1 text-dark"
	                    data-id="<?=$comment->id?>">
	                    <?= Yii::t('app', 'Odpowiedz') ?>
	                </a>
	            </div>
	        </div>
        </div>
    </div>
    <?php foreach ($comment->childs as $comment): ?>
    <?= Yii::$app->controller->renderPartial('../comments/comment', ['comment' => $comment, 'child' => true]) ?>
    <?php endforeach; ?>
    <?php if (!$child): ?>
</div>
<?php endif;
