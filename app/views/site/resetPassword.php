<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap4\ActiveForm */
/* @var $model \app\models\ResetPasswordForm */

use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;

?>
<div class="site-reset-password justify-content-center d-flex">
    <div class="row w-75">
        <div class="col-lg-12">
            <h1 class="pb-3 pt-5">
                <?= Yii::t('app', 'Resetowanie hasła') ?>
            </h1>
        </div>
        <div class="col-lg-12 mt-4">

            <form id="reset-password-form" class="text-start">

                <div class="form-outline mb-4">
                    <input type="email" id="resetpasswordform-password" class="form-control" name="ResetPasswordForm[password]" autofocus />
                    <label class="form-label" for="resetpasswordform-password">
                        <?= Yii::t('app', 'Podaj nowe hasło') ?>
                    </label>
                </div>

                <div class="form-group">
                    <?= Html::submitButton(Yii::t('app', 'Zmień hasło'), ['class' => 'btn btn-primary mb-2 w-100']) ?>
                </div>

            </form>

        </div>
    </div>
</div>