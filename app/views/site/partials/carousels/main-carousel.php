<div class="swiper index-carousel mb-4"
    style='style=" --swiper-navigation-color: #fff; --swiper-pagination-color: #fff'>
    <div class="parallax-bg" style="background-image: url('/images/site/carousel1.jpg');" data-swiper-parallax="-23%">
    </div>
    <div class="swiper-wrapper">
        <?php foreach ($main_carousel as $carousel): ?>
        <div class="swiper-slide text-center">
            <div class="title" data-swiper-parallax="-300"></div>
            <div class="subtitle container" data-swiper-parallax="-200">
                <h1>
                    <?= $carousel->title ?>
                </h1>
            </div>
            <div class="text container" data-swiper-parallax="-100">
                <p>
                    <?= $carousel->description ?>
                </p>
            </div>
        </div>
        <?php endforeach; ?>
    </div>
    <div class="swiper-button-next"></div>
    <div class="swiper-button-prev"></div>
    <div class="swiper-pagination"></div>
</div>