<?php
/* @var $most_popular array */
?>
<h2 class="p-1 bg-primary text-white card shadow">
    <?= Yii::t('app', 'Najpopularniejsze') ?>
</h2>
<div class="cards row mt-3 me-0 ms-0">
    <?php foreach ($most_popular as $k => $manga): ?>
		<?= Yii::$app->controller->renderPartial('/manga/partials/card', ['manga' => $manga, 'k' => $k, 'slider' => false, 'col' => 'col-lg-3']) ?>
    <?php endforeach; ?>
    <div class="w-100 text-center">
        <a href="/manga/list?sort=popular" data-pjax class="btn btn-primary mt-3">
            <?= Yii::t('app', 'Pokaż więcej') ?>
        </a>
    </div>
</div>