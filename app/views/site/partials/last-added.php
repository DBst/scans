<?php
/* @var $last_added array */
?>
<h3 class="w-100 text-center mt-3 mb-3">
    <?= Yii::t('app', 'Ostatnio dodane') ?>
</h3>
<div class="p-2 pb-3 shadow card">
	<div class="last-added">
	    <div class="swiper-wrapper">
	        <?php foreach ($last_added as $k => $manga): ?>
	            <?= Yii::$app->controller->renderPartial('/manga/partials/card', ['manga' => $manga, 'k' => $k, 'slider' => true, 'col' => 'col-lg-3']) ?>
	        <?php endforeach; ?>
	    </div>
	    <div class="swiper-pagination"></div>
	</div>
</div>