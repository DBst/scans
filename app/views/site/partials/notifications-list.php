<?php foreach ($notifications as $notify): ?>
<li class="row justify-content-end">
    <?php if ($notify->status == 1): ?>
    <i class="far fa-check-circle col-md-2 p-0"></i>
    <?php elseif ($notify->status == 2): ?>
    <i class="fas fa-times col-md-2 p-0"></i>
    <?php elseif ($notify->status == 3): ?>
    <i class="fas fa-info-circle col-md-2 p-0"></i>
    <?php endif; ?>
    <div class="col-md-10">
        <p class="m-0"><?= $notify->message ?>
        </p>
        <p class="text-muted m-0">
            <?= Yii::$app->formatter->asRelativeTime($notify->added_at) ?>
        </p>
    </div>
</li>
<?php endforeach;
