<?php
/* @var $last_updated array */
?>
<div class="cards" style="display: grid;">
    <?php foreach ($last_updated as $k => $manga): ?>
        <?= Yii::$app->controller->renderPartial('/manga/partials/horizontal-card', ['manga' => $manga, 'k' => $k]) ?>
    <?php endforeach; ?>
</div>