<?php

/* @var $review Object */
/* @var $this yii\web\View */
/* @var $form yii\bootstrap4\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;

?>
<div class="site-review justify-content-center d-flex">
    <div class="row w-75">
        <div class="col-lg-12">
            <h1><?= Yii::t('app', 'Oceń mangę') ?>
            </h1>
        </div>
        <div class="col-lg-12 mt-4">
            <?php $form = ActiveForm::begin(['id' => 'review-form']); ?>

            <?= $form->field($review, 'review')->textarea(['autofocus' => true, 'rows' => 3])->label(Yii::t('app', 'Opinia (opcjonalnie)')) ?>
            <h4 class="stars">
                <?php
                    $rating = $review->rating;
                    for ($i = 1; $i <= 10; $i++) {
                        if ($i <= $rating) {
                            echo '<span class="set-rate" data-rate="' . $i . '"><i class="fas fa-star"></i></span>';
                        } else {
                            echo '<span class="set-rate" data-rate="' . $i . '"><i class="far fa-star"></i></span>';
                        }
                    }

                ?>
            </h4>
            <div class="form-group">
                <?= Html::submitButton(Yii::t('app', 'Oceń mangę'), ['class' => 'btn btn-primary w-50 mt-3', 'manga-id' => $manga_id, 'name' => 'review-button', 'id' => 'review-send-form']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>