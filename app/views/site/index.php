<?php
use yii\widgets\Pjax;

$this->title = Yii::t('app', 'Strona Główna');
?>
<?php Pjax::begin(['id'=>'content']); ?>

<div class="container mb-4">
    <div class="row">
        <div class="col-md-8 text-center mt-4">
            <?= Yii::$app->controller->renderPartial('partials/carousels/main-carousel', ['main_carousel' => $main_carousel]); ?>
            <?= Yii::$app->controller->renderPartial('partials/most-popular', ['most_popular' => $most_popular]); ?>
        </div>
        <div class="col-md-4 pb-3 mt-4">
	        <h3 class="text-center bg-primary rounded shadow text-white p-1">
                <?= Yii::t('app', 'Ostatnio aktualizowane') ?>
	        </h3>
	        <div class="last-updated ps-3">
            <?= Yii::$app->controller->renderPartial('partials/last-updated', ['last_updated' => $last_updated]); ?>
	        </div>
        </div>
    </div>
    <div class="row mt-3">
        <div class="col-md-12 section">
            <?= Yii::$app->controller->renderPartial('partials/last-added', ['last_added' => $last_added]); ?>
        </div>
    </div>
</div>
<?php Pjax::end();
