<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap4\ActiveForm */
/* @var $model \app\models\PasswordResetRequestForm */

use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;

?>
<div class="site-request-password-reset justify-content-center d-flex">
    <div class="row w-75">
        <div class="col-lg-12">
            <h1 class="pb-3 pt-5">
                <?= Yii::t('app', 'Resetowanie hasła') ?>
            </h1>
        </div>
        <div class="col-lg-12 mt-4 mb-4">

            <form id="request-password-reset-form" class="text-start">

                <div class="form-outline mb-4">
                    <input type="text" id="passwordresetrequestform-email" class="form-control" autofocus name="PasswordResetRequestForm[email]" />
                    <label class="form-label" for="passwordresetrequestform-email">
                        <?= Yii::t('app', 'Adres e-mail') ?>
                    </label>
                </div>

                <div class="form-group">
                    <?= Html::submitButton(Yii::t('app', 'Zresetuj hasło'), ['class' => 'btn btn-primary mb-2 w-100', 'id' => 'password-reset-send-form']) ?>
                </div>

            </form>

            <br>
            <hr>

            <small style="color:#999;margin:1em 0">
                <?= Yii::t('app', 'Nie masz konta?') ?>
                <?= Html::a(Yii::t('app', 'Zarejestruj się'), ['/signup']) ?>!
            </small>

        </div>
    </div>
</div>