<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap4\ActiveForm */
/* @var $model \app\models\SignupForm */

use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;

?>
<div class="site-login justify-content-center d-flex">
    <div class="row w-75">
        <div class="col-lg-12">
            <h1 class="pb-3 pt-5">
                <?= Yii::t('app', 'Rejestracja') ?>
            </h1>
        </div>
        <div class="col-lg-12 mt-4 mb-4">

            <form id="form-signup" class="text-start">

                <div class="form-outline mb-4">
                    <input type="text" id="signupform-username" class="form-control" name="SignupForm[username]" />
                    <label class="form-label" for="signupform-username">
                        <?= Yii::t('app', 'Nazwa użytkownika') ?>
                    </label>
                </div>
                <div class="form-outline mb-4">
                    <input type="text" id="signupform-email" class="form-control" name="SignupForm[email]" />
                    <label class="form-label" for="signupform-email">
                        <?= Yii::t('app', 'Adres e-mail') ?>
                    </label>
                </div>
                <div class="form-outline mb-4">
                    <input type="text" id="signupform-password" class="form-control" name="SignupForm[password]" />
                    <label class="form-label" for="signupform-password">
                        <?= Yii::t('app', 'Hasło') ?>
                    </label>
                </div>

                <div class="form-group">
                    <?= Html::submitButton(Yii::t('app', 'Zarejestruj'), ['class' => 'btn btn-primary mb-2 w-100', 'name' => 'signup-button', 'id' => 'signup-send-form']) ?>
                </div>

            </form>

            <br>
            <hr>

            <small style="color:#999;margin:1em 0">
                <?= Yii::t('app', 'Masz już konto?') ?>
                <a href="javascript:void(0)" id="login-btn">
                    <?= Yii::t('app', 'Zaloguj się') ?>!
                </a>
            </small>

        </div>
    </div>
</div>