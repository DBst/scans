<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap4\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;

?>
<div class="site-login justify-content-center d-flex">
    <div class="row w-75">
        <div class="col-lg-12">
            <h1 class="pb-3 pt-5">
                <?= Yii::t('app', 'Logowanie') ?>
            </h1>
        </div>
        <div class="col-lg-12 mt-4 mb-4">

            <form id="login-form" class="text-start">

                <div class="form-outline mb-4">
                    <input type="text" id="loginform-username" class="form-control" name="LoginForm[username]" />
                    <label class="form-label" for="loginform-username">
                        <?= Yii::t('app', 'Nazwa użytkownika') ?>
                    </label>
                </div>

                <div class="form-outline mb-4">
                    <input type="password" id="loginform-password" class="form-control" name="LoginForm[password]" />
                    <label class="form-label" for="loginform-password">
                        <?= Yii::t('app', 'Hasło') ?>
                    </label>
                </div>

                <div class="d-flex ms-0">
                    <div class="form-check mb-4 col-6">
                        <input class="form-check-input" type="checkbox" value="" id="loginform-rememberme" name="LoginForm[rememberMe]" />
                        <label class="form-check-label" for="loginform-rememberme">
                            <?= Yii::t('app', 'Zapamiętaj mnie') ?>
                        </label>
                    </div>
                    <a href="/request-password-reset" class="col-6 text-end">
                        <?= Yii::t('app', 'Zapomniałeś hasła?') ?>
                    </a>
                </div>

                <div class="form-group">
                    <?= Html::submitButton(Yii::t('app', 'Zaloguj się'), ['class' => 'btn btn-primary w-100 mb-2', 'name' => 'login-button', 'id' => 'login-send-form']) ?>
                </div>

            </form>

            <br>
            <hr>

            <small style="color:#999;margin:1em 0">
                <?= Yii::t('app', 'Nie masz konta?') ?>
                <?= Html::a(Yii::t('app', 'Zarejestruj się'), ['/signup']) ?>!
            </small>

        </div>
    </div>
</div>