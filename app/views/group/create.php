<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

$this->title = Yii::t('app', 'Tworzynie grupy');
?>
<?php Pjax::begin(['id'=>'content']); ?>
<div class="group-create section container mt-4 mb-4">
    <div class="row mt-3 mb-5 justify-content-center">
        <h2>
            <?= Yii::t('app', 'Tworzenie grupy') ?>
        </h2>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-6">
            <?php
            $languages = [
                '' => Yii::t('app', 'Wybierz język')
            ];
            foreach (Yii::$app->init->languages as $language) {
                $languages[$language->code] = strtoupper($language->shortname);
            }
            $form = ActiveForm::begin([
                'id' => 'group-create-form',
                'options' => ['class' => 'custom-form-style'],
            ]) ?>

            <?= $form->field($groups, 'name')->textInput(['placeholder' => Yii::t('app', 'Podaj nazwę')])->label(Yii::t('app', 'Nazwa grupy')) ?>
            <?= $form->field($groups, 'description')->textarea(['rows' => '4', 'placeholder' => Yii::t('app', 'Podaj opis')])->label(Yii::t('app', 'Opis grupy')) ?>
            <?= $form->field($groups, 'url')->textInput(['placeholder' => Yii::t('app', 'Podaj url')])->label(Yii::t('app', 'Link do strony grupy (opcjonalnie)')) ?>
            <?= $form->field($groups, 'language')->dropDownList($languages)->label(Yii::t('app', 'Domyślny język grupy (opcjonalnie)')) ?>

            <div class="form-group">
                <div class="col-md-12 text-center">
                    <?= Html::submitButton(Yii::t('app', 'Utwórz grupę!'), ['class' => 'btn btn-primary w-50 mt-4']) ?>
                </div>
            </div>
            <?php ActiveForm::end() ?>
        </div>
    </div>
</div>

<?php Pjax::end();
