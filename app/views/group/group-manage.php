<?php
use yii\widgets\Pjax;

$this->title = Yii::t('app', 'Zarządzanie grupą');
?>
<?php Pjax::begin(['id'=>'content']); ?>
<div class="container mt-5 mb-5 group-manage">
    <div class="row justify-content-between">
        <div class="col-md-3 section">
            <ul class="nav flex-column">
                <li class="nav-item">
                    <a class="nav-link active" data-page="dashboard" aria-current="page" href="/group" data-pjax>
                        <i class="fas fa-table me-1"></i>
                        <?= Yii::t('app', 'Tablica') ?>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-page="members" data-pjax href="/group/members">
                        <i class="fas fa-users me-1"></i>
                        <?= Yii::t('app', 'Członkowie')?>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-pjax href="/group/chapters">
                        <i class="fas fa-stream me-1" aria-hidden="true"></i>
                        <?= Yii::t('app', 'Dodane chaptery')?>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-pjax href="/group/settings">
                        <i class="fas fa-cog me-1"></i>
                        <?= Yii::t('app', 'Ustawienia')?>
                    </a>
                </li>
            </ul>
        </div>
        <div class="col-md-8 section">
            <?php Pjax::begin(['id'=>'group-manage']); ?>
            <?= Yii::$app->controller->renderPartial('partials/' . $path, ['group' => $group, 'chapters' => isset($chapters) ? $chapters : null]) ?>
            <?php Pjax::end(); ?>
        </div>
    </div>
</div>
<?php Pjax::end();
