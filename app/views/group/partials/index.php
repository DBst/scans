<h3 class="mt-3 text-center">
    <?= Yii::t('app', 'Tablica') ?>
</h3>
<h4 class="mt-5 mb-4 ms-2">
    <?= Yii::t('app', 'Informacje') ?>
</h4>
<b>
    <?= Yii::t('app', 'Ilość członków') ?>:
</b>
<span>
    <?= count($group->users); ?>
</span><br>
<b>
    <?= Yii::t('app', 'Ilość chapterów') ?>:
</b>
<span>
    <?= count($group->chapters) ?>
</span>