<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>
<h3 class="mt-3 text-center">
    <?= Yii::t('app', 'Ustawienia') ?>
</h3>
<?php
$form = ActiveForm::begin([
    'id' => 'group-manage-settings-form',
    'options' => ['class' => 'custom-form-style']
]); ?>
<div class="row mt-5">
    <div class="col-md-6">
        <?= $form->field($group, 'name')->textInput(['placeholder' => Yii::t('app', 'Nazwa grupy')])->label(Yii::t('app', 'Nazwa grupy')) ?>
    </div>
    <div class="col-md-6">
        <?= $form->field($group, 'description')->textInput(['placeholder' => Yii::t('app', 'Opis grupy')])->label(Yii::t('app', 'Opis grupy')) ?>
    </div>
    <div class="col-md-6">
        <?= $form->field($group, 'url')->textInput(['placeholder' => Yii::t('app', 'Link do strony grupy')])->label(Yii::t('app', 'Link do strony grupy')) ?>
    </div>
    <div class="col-md-6">
        <?php
            $languages = [
                '' => Yii::t('app', 'Wybierz język')
            ];
            foreach(Yii::$app->init->languages as $lang) {
                $languages[$lang->id] = Yii::t('app', $lang->name);
            }
        ?>
        <?= $form->field($group, 'language')->dropDownList($languages) ?>
    </div>
    <div class="col-md-6">
        <span>
            <?= Yii::t('app', 'Zdjęcie nagłówkowe') ?>
            <i class="fas fa-question-circle tip"
                title="<?= Yii::t('app', 'To zdjęcie będzie widocznie na początku każdego chaptera') ?>"></i>
        </span>
        <label class="custom-file-upload">
            <input class="d-none header-image" type="file" accept="image/*" />
            <span>
                <?= Yii::t('app', 'Wybierz zdjęcie') ?>
            </span>
            <a href="#" class="image-preview image-preview-file" data-preview="<?=
            \app\models\Functions::imageExists('/images/upload/groups/' . $group->id . '_header.webp') ? '/images/upload/groups/' . $group->id . '_header.webp' : ''
            ?>">
                <i class="fas fa-eye"></i>
            </a>
        </label>
    </div>
    <div class="col-md-6">
        <span>
            <?= Yii::t('app', 'Zdjęcie stopkowe') ?>
            <i class="fas fa-question-circle tip"
                title="<?= Yii::t('app', 'To zdjęcie będzie widocznie na końcu każdego chaptera') ?>"></i>
        </span>
        <label class="custom-file-upload">
            <input class="d-none footer-image" type="file" accept="image/*" />
            <span>
                <?= Yii::t('app', 'Wybierz zdjęcie') ?>
            </span>
            <a href="#" class="image-preview image-preview-file" data-preview="<?=
            \app\models\Functions::imageExists('/images/upload/groups/' . $group->id . '_footer.webp') ? '/images/upload/groups/' . $group->id . '_footer.webp' : ''
            ?>">
                <i class="fas fa-eye"></i>
            </a>
        </label>
    </div>
    <div class="col-md-12 text-end mt-3">
        <div class="form-group">
            <?= Html::submitButton('Zapisz', ['class' => 'btn btn-primary w-25']) ?>
        </div>
    </div>
</div>
<?php ActiveForm::end();