<h3 class="text-center mt-3">
    <?= Yii::t('app', 'Członkowie grupy') ?>
</h3>
<table class="table text-white mt-5">
    <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col"><?= Yii::t('app', 'Użytkownik') ?>
            </th>
            <th scope="col"><?= Yii::t('app', 'Status') ?>
            </th>
            <th scope="col" class="text-center"><?= Yii::t('app', 'Akcje') ?>
            </th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($group->users as $k => $user): ?>
        <tr>
            <th scope="row"><?= $k+1 ?>
            </th>
            <td><?= $user->username ?>
            </td>
            <td><?= \common\models\User::getRoleName($user) ?>
            </td>
            <td class="text-center">
                <?php if ($user->id !== $group->owner_id): ?>
                <a href="#" class="text-white remove-from-group"
                    data-id="<?= $user->id ?>">
                    <i class="fas fa-trash"></i>
                    <?= Yii::t('app', 'Wyrzuć') ?>
                </a>
                <?php else: ?>
                -
                <?php endif; ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?php if (!empty($group->invitations)): ?>
<h3 class="text-center mt-5">
    <?= Yii::t('app', 'Zaproszenia') ?>
</h3>
<table class="table text-white mt-5">
    <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col"><?= Yii::t('app', 'Użytkownik') ?>
            </th>
            <th scope="col"><?= Yii::t('app', 'Status') ?>
            </th>
            <th scope="col" class="text-center"><?= Yii::t('app', 'Akcje') ?>
            </th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($group->invitations as $k => $user): ?>
        <?php $user = $user->user; ?>
        <tr>
            <th scope="row"><?= $k+1 ?>
            </th>
            <td><?= $user->username ?>
            </td>
            <td><?= \common\models\User::getRoleName($user) ?>
            </td>
            <td class="text-center">
                <?php if ($user->id !== $group->owner_id): ?>
                <a href="#" class="text-white cancel-invitation"
                    data-id="<?= $user->id ?>">
                    <i class="fas fa-times"></i>
                    <?= Yii::t('app', 'Anuluj') ?>
                </a>
                <?php else: ?>
                -
                <?php endif; ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?php endif; ?>
<div class="w-100 text-end">
    <a href="#" class="btn btn-primary invite-to-group">
        <i class="fas fa-user-plus me-1"></i>
        <?= Yii::t('app', 'Zaproś użytkownika') ?>
    </a>
</div>