<?php
    $offset = !empty(Yii::$app->request->get('offset')) ? (int) Yii::$app->request->get('offset') : 0;
?>
<h3 class="text-center mt-3">
    <?= Yii::t('app', 'Dodane chaptery') ?>
</h3>
<table class="table custom-table text-white mt-5">
    <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col"><?= Yii::t('app', 'Zdjęcie') ?>
            </th>
            <th scope="col"><?= Yii::t('app', 'Manga') ?>
            </th>
            <th scope="col"><?= Yii::t('app', 'Dodane przez') ?>
            </th>
            <th scope="col"><?= Yii::t('app', 'Status') ?>
            </th>
            <th scope="col"><?= Yii::t('app', 'Dodane') ?>
            </th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($chapters as $k => $chapter): ?>
        <?php if ($k !== 10): ?>
        <tr>
            <th scope="row"><?= $offset + $k + 1 ?>
            </th>
            <td><img src="/images/upload/manga/<?= $chapter->manga->image ?>"
                    style="max-height: 75px"></td>
            <td><?= $chapter->manga->name ?>
            </td>
            <td><?= $chapter->user->username ?>
            </td>
            <td><?= \common\models\User::getRoleName($chapter->user) ?>
            </td>
            <td><?= Yii::$app->formatter->asRelativeTime($chapter->added_at) ?>
            </td>
        </tr>
        <?php endif; ?>
        <?php endforeach; ?>
    </tbody>
</table>
<div class="row">
    <div class="col-md-6 mt-2 mb-2">
        <?php if (!empty($offset)): ?>
        <a class="btn btn-primary ms-2"
            href="/group/chapters?offset=<?= $offset - 10 < 0 ? 0 : $offset - 10 ?>"
            data-pjax>
            <?= Yii::t('app', 'Poprzednia strona') ?>
        </a>
        <?php endif; ?>
    </div>
    <div class="col-md-6 text-end mt-2 mb-2">
        <?php if (count($chapters) > 10): ?>
        <a class="btn btn-primary me-2"
            href="/group/chapters?offset=<?= $offset + 10 ?>"
            data-pjax>
            <?= Yii::t('app', 'Następna strona') ?>
        </a>
        <?php endif; ?>
    </div>
</div>