<?php

/* @var $navbarElements array */
/* @var $dropdown bool */

foreach ($navbarElements as $element):
    if(!navCheckGroup($element['group'])): ?>
		<li class="nav-item <?= !empty($element['childs']) ? 'dropdown' : '' ?>">
            <?php if(!empty($element['childs'])): //if element have childs ?>
				<a class="nav-link <?= $dropdown ? 'dropdown-item' : '' ?> dropdown-toggle" href="<?= $element['url'] ?>" id="navbarDropdown" role="button"
				   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" <?= $element['blank'] ? ' target="_blank"' : '' ?>>
					<i class="<?= $element['icon'] ?>"></i>
                    <?= $element['name'] ?>
				</a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <?php foreach($element['childs'] as $child): ?>
                        <?php if(!navCheckGroup($child['group'])): ?>
						<a class="dropdown-item" href="<?= $child['url'] ?>" <?= $child['pjax'] ? 'data-pjax' : '' ?>
                            <?= $child['blank'] ? ' target="_blank"' : '' ?>>
							<i class="<?= $child['icon'] ?>"></i>
                            <?= $child['name'] ?>
						</a>
                        <?php endif; ?>
                    <?php endforeach; ?>
				</div>
				<?php else: ?> <!-- if element does not have childs -->
				<a class="nav-link <?= $dropdown ? 'dropdown-item' : '' ?>" href="<?= $element['url'] ?>" <?= $element['pjax'] ? 'data-pjax' : '' ?>
                    <?= $element['blank'] ? ' target="_blank"' : '' ?>>
					<i class="<?= $element['icon'] ?>"></i>
                    <?= $element['name'] ?>
				</a>
            <?php endif; ?>
		</li>
    <?php endif;
endforeach;
?>