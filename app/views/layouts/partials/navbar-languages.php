
<span class="dropdown change-language-dropdown h-100 d-inline-block pe-2 ps-2 z-index-9" href="#">
    <span class="position-relative vertical-center d-block">
        <a href="#" class="dropdown-toggle language-btn-dropdown" role="button"
           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-globe"></i>
        </a>
        <div class="dropdown-menu">
            <div class="row">
                <div class="col-6">
                    <span class="mt-1 mb-1 d-block fw-bold">
                        <?= Yii::t('app', 'Język strony') ?>
                    </span>
                    <?php
                    $firstAutoTranslate = true;
                    foreach (Yii::$app->init->languages as $language):
                        $icon = $language->shortname;
                        if ($icon == 'en') {
                            $icon = 'gb';
                        } ?>
                        <li class="nav-item">
                        <a class="dropdown-item change-language"
                           data-id="<?=$language->id?>"
                           href="#" main-language="1">
                            <img class="ms-1 mb-1" width="24"
                                 src="/images/site/country-flags/<?=$icon?>.jpg">
                            <span>
                                <?=$language->name?>
                                <?php if ($language->code == Yii::$app->language): ?>
                                    <span class="text-success selected-language">
                                    <i class="fas fa-check"></i>
                                </span>
                                <?php endif; ?>
                            </span>
                        </a>
                    </li>
                    <?php endforeach; ?>
                </div>
                <div class="col-6">
                    <span class="text-end mt-1 mb-1 d-block fw-bold">
                        <?= Yii::t('app', 'Język rozdziałów') ?>
                    </span>
                    <?php
                    $firstAutoTranslate = true;
                    foreach (Yii::$app->init->chapter_languages as $language):
                        $icon = $language->shortname;
                        if ($icon == 'en') {
                            $icon = 'gb';
                        } ?>
                        <li class="nav-item">
                        <a class="dropdown-item change-language"
                           data-id="<?=$language->id?>"
                           href="#" main-language="0">
                            <img class="ms-1 mb-1" width="24"
                                 src="/images/site/country-flags/<?=$icon?>.jpg">
                            <span>
                                <?=$language->name?>
                                <?php if (in_array($language->id, Yii::$app->init->user_languages)): ?>
                                    <span class="text-success selected-language">
                                    <i class="fas fa-check"></i>
                                </span>
                                <?php endif; ?>
                            </span>
                        </a>
                    </li>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </span>
</span>