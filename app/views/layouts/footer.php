<?php

use yii\bootstrap4\Html;

?>
<div class="shadow pt-3 pb-3">
	<div class="container">
		<div class="row">
		    <p class="m-0 col-lg-6">
		        &copy; <a
		            href="<?= Yii::$app->init->config->protocol ?><?= Yii::$app->init->config->domain ?>"
		            class="text-dark" style="text-transform: capitalize;">
		            <?= Html::encode(Yii::$app->init->config->domain) ?>
		        </a>
		        2021 - <?= date('Y') ?>
		    </p>
		    <p class="m-0 col-lg-6">
		        Coded with <i class="far fa-heart"></i> by
		        <a href="https://www.trojansky.pl" class="text-dark">Trojansky.pl</a>
		    </p>
		</div>
	</div>
</div>