
<script>
    const translate = {
        'defaultProblem': '<?= Yii::t('app', 'Wystąpił problem! Spróbuj ponownie za chwilę.') ?>',
        'modalConfirm': '<?= Yii::t('app', 'Czy jesteś pewny że chcesz to zrobić?') ?>',
        'modalInviteToGroup': '<?= Yii::t('app', 'Wpisz nazwę użytkownika którego chcesz zaprosić') ?>',
        'yes': '<?= Yii::t('app', 'Tak') ?>',
        'no': '<?= Yii::t('app', 'Nie') ?>',
        'cancel': '<?= Yii::t('app', 'Anuluj') ?>',
        'submit': '<?= Yii::t('app', 'Wyślij') ?>',
    };
</script>