<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\widgets\Pjax;
use yii\helpers\Html;

$this->title = $name;
?>
<?php Pjax::begin(['id'=>'content']); ?>
<div class="container mb-5" style="margin-top: 240px;">
    <div class="row section">
        <div class="site-error m-5 text-center w-100">
            <h1 style="font-size: 600%;font-weight: bold;">
                <?= !empty(Yii::$app->errorHandler->exception) ? '#' . Yii::$app->errorHandler->exception->statusCode : (!empty($error) ? '#' . $error : '') ?>
            </h1>

            <h2 class="">
                <?= nl2br(Html::encode($message)) ?>
            </h2>

            <p>
                <?= Yii::t('app', 'Jeżeli uważasz że to błąd, skontaktuj się z administratorem.') ?>
            </p>

        </div>
    </div>
</div>

<?php Pjax::end();
