<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\assets\AppAsset;
use yii\bootstrap4\Html;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="h-100">

    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="icon" type="image/png" href="/images/site/logo.png" />
        <?php $this->registerCsrfMetaTags() ?>
        <title>
            <?= Html::encode($this->title . ( !empty($this->title) ? ' - ' : '') . Yii::$app->init->config->title) ?>
        </title>
        <?php $this->head() ?>
    </head>

    <body class="d-flex flex-column min-vh-100">
        <input type="hidden" name="site-title" value="<?= Html::encode(Yii::$app->init->config->title) ?>" />

        <?= Yii::$app->controller->renderPartial('/layouts/silent-loader') ?>

        <?php if (Yii::$app->request->get('reset-password') == 'true'): ?>
        <input type="hide" class="d-none" id="reset-password"
            data-token="<?= Yii::$app->request->get('token') ?>">
        <?php endif; ?>
        <?php $this->beginBody() ?>

        <header>
            <?= $this->render('/layouts/navbar.php') ?>
        </header>

        <main role="main" class="content">
            <?= $content ?>
        </main>

        <footer class="footer mt-auto">
            <?= $this->render('/layouts/footer.php') ?>
        </footer>

        <?php $this->endBody() ?>

        <a href="#" id="scroll-to-top" class="d-none bg-primary text-light shadow rounded">
            <i class="fas fa-arrow-up"></i>
        </a>

        <script>
            <?php
            if (!empty(Yii::$app->session->getFlash('error'))) { ?>
            notifications.error.push(
                '<?= Yii::t('app', Yii::$app->session->getFlash('error')) ?>'
            );
            <?php }
            if (!empty(Yii::$app->session->getFlash('success'))) { ?>
            notifications.success.push(
                '<?= Yii::t('app', Yii::$app->session->getFlash('success')) ?>'
            );
            <?php } ?>
        </script>
        <?= Yii::$app->controller->renderPartial('/layouts/jsTranslate') ?>
    </body>

</html>
<?php $this->endPage();
