<?php

if (!function_exists('navCheckGroup')) {
    function navCheckGroup($group): bool
    {
        if ($group == -1 && !Yii::$app->user->isGuest) {
            return true;
        } elseif ($group == 1 && Yii::$app->user->isGuest) {
            return true;
        } elseif ($group == 2 && !\common\models\User::isAdmin()) {
            return true;
        } elseif ($group == 10 && !empty(Yii::$app->user->identity->group_id)) {
            return true;
        } elseif ($group == 11 && empty(Yii::$app->user->identity->group_id)) {
            return true;
        }
        return false;
    }
}
$navbarElements = [
    [
        'name' => Yii::t('app', 'Strona Główna'),
        'url' => '/',
        'icon' => 'fas fa-home',
        'group' => 0, //-1 - not logged in, 0 - all, 1 - user, 2 - admin, 10 - user without group, 11 - user with group
        'pjax' => true,
        'blank' => false,
        'childs' => [],
    ], [
        'name' => Yii::t('app', 'Mangi'),
        'url' => '/manga/list',
        'icon' => 'fas fa-stream',
        'group' => 0,
        'pjax' => true,
        'blank' => false,
        'childs' => [],
    ], [
        'name' => Yii::t('app', 'Zarejestruj się'),
        'url' => '/signup',
        'icon' => 'fas fa-sign-in-alt',
        'group' => -1,
        'pjax' => false,
        'blank' => false,
        'childs' => [],
    ], [
        'name' => Yii::t('app', 'Dla twórców'),
        'url' => '#',
        'icon' => 'fas fa-folder',
        'group' => 1,
        'pjax' => false,
        'blank' => false,
        'childs' => [
            [
                'name' => Yii::t('app', 'Dodaj chapter'),
                'url' => '/chapter/upload',
                'icon' => 'fas fa-cloud-upload-alt',
                'group' => 1,
                'pjax' => true,
                'blank' => false,
            ], [
                'name' => Yii::t('app', 'Moje chaptery'),
                'url' => '/chapter/my-chapters',
                'icon' => 'fas fa-stream',
                'group' => 1,
                'pjax' => true,
                'blank' => false,
            ], [
                'name' => Yii::t('app', 'Utwórz grupę'),
                'url' => '/group/create',
                'icon' => 'fas fa-users',
                'group' => 10,
                'pjax' => true,
                'blank' => false,
            ], [
                'name' => Yii::t('app', 'Zarządzanie grupą'),
                'url' => '/group',
                'icon' => 'fas fa-users',
                'group' => 11,
                'pjax' => true,
                'blank' => false,
            ], [
                'name' => Yii::t('app', 'Zaproponuj mangę'),
                'url' => '/suggest-manga',
                'icon' => 'fas fa-book',
                'group' => 1,
                'pjax' => true,
                'blank' => false,
            ],
        ],
    ], [
        'name' => Yii::t('app', 'Panel Admina'),
        'url' => Yii::$app->init->config->protocol . 'admin.' . Yii::$app->init->config->domain,
        'icon' => 'fas fa-user-shield',
        'group' => 2,
        'pjax' => false,
        'blank' => false,
        'childs' => [],
    ],
];
?>
<!--first navbar with feature to position fixed after scrolling -->
<div class="header-navbar-container">
    <div class="header-navbar">
        <div class="container">
            <div class="row" style="justify-content: space-between">
                <div class="col-lg-auto col-9 pt-1 pb-1 d-flex position-relative">
                    <span class="dropdown mini-menu-dropdown h-100 d-inline-block z-index-9" href="#">
	                    <span class="position-relative vertical-center d-block">
	                        <a href="#" class="dropdown-toggle account-btn-dropdown mini-second-menu me-1" role="button"
	                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
	                            <i class="fas fa-bars m-0"></i>
	                        </a>
	                        <div class="dropdown-menu" style="left: 0 !important;top: 25px !important;">
	                            <?= Yii::$app->controller->renderPartial('/layouts/partials/navbar-items', ['navbarElements' => $navbarElements, 'dropdown' => true]); ?>
	                        </div>
	                    </span>
                    </span>

                    <?= Yii::$app->controller->renderPartial('/layouts/partials/navbar-languages') ?>

                    <form class="btn btn-outline-light my-2 my-sm-0 rounded-0 border-0 btn-search p-0"
                        id="navbar-fast-search">
	                    <div class="input-group rounded">
		                    <input type="search" class="form-control rounded" placeholder="Search" aria-label="Search" aria-describedby="search-addon" />
		                    <span class="input-group-text border-0" id="search-addon">
						    <button type="submit" class="btn btn-primary fas fa-search"></button>
						  </span>
	                    </div>
                    </form>
                </div>
                <div class="col-lg-auto col-3 text-end right-section pt-1 pb-1">
                    <?php if (!Yii::$app->user->isGuest): ?>
                    <span class="d-inline-block h-100">
	                    <span class="position-relative vertical-center d-block">
		                    <a class="notifications-btn-parent <?= !Yii::$app->user->isGuest && !empty(\common\models\Notifications::find()->where(['user_id' => (int) Yii::$app->user->id])->one()) ? 'has-notify' : '' ?>"
		                        href="#">
		                        <i class="far fa-bell notifications-btn"></i>
		                    </a>
	                    </span>
                    </span>
                    <div class="notifications-content d-none">
                        <p class="text-center font-weight-bold">
                            <?= Yii::t('app', 'Powiadomienia') ?>
                        </p>
                        <div class="notifications-list">
                            <ul></ul>
                        </div>
                    </div>
                    <?php endif; ?>
                    <span class="dropdown account-dropdown d-inline-block h-100 ps-2">
	                    <span class="position-relative vertical-center d-block">
	                        <a href="#" class="dropdown-toggle account-btn-dropdown" role="button" data-toggle="dropdown"
	                            aria-haspopup="true" aria-expanded="false">
	                            <i class="far fa-user" style="font-size: 21px;"></i>
	                        </a>
	                        <div class="dropdown-menu dropdown-menu-end">
	                            <?php if (Yii::$app->user->isGuest): ?>
	                            <a class="dropdown-item" id="login-btn" href="#">
	                                <span>
	                                    <i class="fas fa-sign-in-alt"></i>
	                                    <?= Yii::t('app', 'Zaloguj się') ?>
	                                </span>
	                            </a>
	                            <?php else: ?>
	                            <a class="dropdown-item" href="/followed" data-pjax>
	                                <span>
	                                    <i class="fas fa-bell"></i>
	                                    <?= Yii::t('app', 'Obserwowane') ?>
	                                </span>
	                            </a>
	                            <a class="dropdown-item" href="/settings">
	                                <span>
	                                    <i class="fas fa-cog"></i>
	                                    <?= Yii::t('app', 'Ustawienia') ?>
	                                </span>
	                            </a>
	                            <div class="dropdown-divider"></div>
	                            <a class="dropdown-item" href="/logout">
	                                <span>
	                                    <i class="fas fa-sign-out-alt"></i>
	                                    <?= Yii::t('app', 'Wyloguj się') ?>
	                                </span>
	                            </a>
	                            <?php endif; ?>
	                        </div>
	                    </span>
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>
<!--second navbar-->
<nav class="navbar navbar-expand-lg navbar-dark shadow bg-primary">
    <div class="container p-0">
        <a class="navbar-brand" href="/index" data-pjax="">
            <img src="/images/site/logo.png" class="navbar-logo pe-1">
            <?= Yii::$app->init->config->title ?>
        </a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse ms-3 me-3" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto">
                <?= Yii::$app->controller->renderPartial('/layouts/partials/navbar-items', ['navbarElements' => $navbarElements, 'dropdown' => false]) ?>
            </ul>
        </div>
    </div>
</nav>