<?php

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main app application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'plugins/bootstrap-5.1.3-dist/css/bootstrap.min.css', #https://getbootstrap.com/docs/5.1
        'plugins/MDBootstrap-3.10.2/css/mdb.min.css', #https://mdbootstrap.com
        'https://unpkg.com/swiper@7/swiper-bundle.min.css',
        'https://unpkg.com/dropzone@6.0.0-beta.1/dist/dropzone.css',
        'https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css',
//        'css/site.css',
        'css/app.css',
        'css/themes/default.css',
    ];
    public $js = [
        'plugins/bootstrap-5.1.3-dist/js/bootstrap.min.js', #https://getbootstrap.com/docs/5.1
        'plugins/MDBootstrap-3.10.2/js/mdb.min.js', #https://mdbootstrap.com
        'https://kit.fontawesome.com/bc6b5334c0.js', #https://fontawesome.com ver 6
        'https://unpkg.com/swiper@7/swiper-bundle.min.js', #https://swiperjs.com/
        'https://cdn.jsdelivr.net/npm/sweetalert2@11', #https://sweetalert2.github.io
        '/plugins/Notiflix-2.7.0/notiflix-aio.js', #https://www.notiflix.com/
        'https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.7.6/jquery.nicescroll.min.js', #https://nicescroll.areaaperta.com/
        'https://unpkg.com/dropzone@6.0.0-beta.1/dist/dropzone-min.js', #https://www.dropzonejs.com
        'https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js', #https://select2.org
        '/plugins/JS-Cookie/js.cookie.min.js', #https://github.com/js-cookie/js-cookie
        '/js/site.js',
        '/js/notifications.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];
}
