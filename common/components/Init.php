<?php

namespace common\components;

use Yii;
use yii\base\Component;
use yii\base\InvalidConfigException;
 
class Init extends Component
{
    public $config;
    public $languages;
    public $user_languages = [];
    public $chapter_languages;
    public $country_flags;

    public function __construct($config)
    {
        Yii::$app->cache->flush(); //draft
        $this->config = $this->loadConfig();
        $languages = \common\models\Languages::find()->orderBy(['auto_translate' => SORT_ASC])->cache(60 * 60 * 24 * 30)->all();
        $this->chapter_languages = $languages;
        foreach ($languages as $language) {
            if($language->translated) {
                $this->languages[] = $language;
            }
            if($language->translated) {
                $this->country_flags[$language->id] = $language->flag;
            }
        }
        $languages = \common\models\UserLanguages::find()
            ->where(['user_id' => Yii::$app->user->id])
            ->cache(60 * 60 * 24 * 30)
            ->all();
        foreach ($languages as $language) {
            $this->user_languages[] = $language->language_id;
        }
        parent::__construct($config);
    }

    public function loadConfig()
    {
        if (empty(self::$config)) {
            return \common\models\Config::loadData();
        }
    }

}
