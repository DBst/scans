<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var common\models\User $user */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/reset-password', 'token' => $user->password_reset_token]);
?>
<div class="password-reset">
    <p>
        <?= Yii::t('app', 'Witaj') ?>
        <?= Html::encode($user->username) ?>,
    </p>
    <p>
        <?= Yii::t('app', 'Wejdź w poniższy link by zresetować hasło') ?>:
    </p>
    <p>
        <?= Html::a(Html::encode($resetLink), $resetLink) ?>
    </p>
</div>