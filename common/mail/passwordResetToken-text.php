<?php

/** @var yii\web\View $this */
/** @var common\models\User $user */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/reset-password', 'token' => $user->password_reset_token]);
?>
<?= Yii::t('app', 'Witaj') ?>
<?= $user->username ?>,

<?= Yii::t('app', 'Wejdź w poniższy link by zresetować hasło') ?>:

<?= $resetLink;
