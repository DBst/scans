<?php
return [
    'adminEmail' => 'admin@hyakki.pl',
    'supportEmail' => 'support@hyakki.pl',
    'senderEmail' => 'no-reply@hyakki.pl',
    'senderName' => 'Hyakki',
    'user.passwordResetTokenExpire' => 3600,
    'user.passwordMinLength' => 8,
];
