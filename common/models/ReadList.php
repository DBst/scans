<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "read_list".
 *
 * @property int $id
 * @property int|null $user_id
 * @property string|null $list
 */
class ReadList extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'read_list';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id'], 'integer'],
            [['list'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'list' => 'List',
        ];
    }
}
