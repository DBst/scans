<?php

namespace common\models;

use app\models\PasswordResetRequestForm;
use app\models\ResendVerificationEmailForm;
use app\models\ResetPasswordForm;
use app\models\SignupForm;
use app\models\VerifyEmailForm;
use Yii;
use yii\base\InvalidArgumentException;
use yii\web\BadRequestHttpException;

/**
 * This is the model class for table "users".
 *
 * @property int $id
 * @property string|null $username
 * @property string $password_hash
 * @property string|null $password_reset_token
 * @property string|null $verification_token
 * @property string|null $email
 * @property string $auth_key
 * @property int|null $status
 * @property int $role
 * @property int|null $group_id
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Groups $group
 * @property MangaReviews[] $mangaReviews
 */
class Users extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['password_hash', 'auth_key', 'created_at', 'updated_at'], 'required'],
            [['status', 'role', 'group_id', 'created_at', 'updated_at'], 'integer'],
            [['username'], 'string', 'max' => 16],
            [['password_hash', 'password_reset_token', 'verification_token', 'email', 'auth_key'], 'string', 'max' => 255],
            [['group_id'], 'exist', 'skipOnError' => true, 'targetClass' => Groups::className(), 'targetAttribute' => ['group_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => Yii::t('app', 'Nazwa użytkownika'),
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'verification_token' => 'Verification Token',
            'email' => 'Email',
            'auth_key' => 'Auth Key',
            'status' => Yii::t('app', 'Status'),
            'role' => 'Role',
            'group_id' => 'Group ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public static function updateUserRole()
    {
        if (!Yii::$app->user->isGuest) {
            $user = self::find()->where(['id' => Yii::$app->user->id])->one();
            $user->uploaded_chapters = $user->uploaded_chapters + 1;
            if ($user->uploaded_chapters >= 24 && $user->role == 0) {
                $user->role = 1;
            } elseif ($user->uploaded_chapters >= 99 && $user->role == 1) {
                $user->role = 2;
            }
            if ($user->save() !== false) {
                return true;
            }
        }
        return false;
    }

    public static function signupForm()
    {
        Yii::$app->response->format = 'json';
        try {
            $model = new SignupForm();
            if (!empty(Yii::$app->request->post())) {
                if ($model->load(Yii::$app->request->post()) && $model->signup()) {
                    return ['status' => true, 'message' => Yii::t('app', 'Rejestracja przebiegła pomyślnie. Sprawdź swoją skrzynkę mailową i potwierdź adres e-mail.')];
                }
                return ['status' => false, 'message' => $model->getErrorSummary(true)[0]];
            }

            return ['status' => true, 'partial' => Yii::$app->controller->renderPartial('signup', ['model' => $model])];
        } catch (Exception $a) {
            Yii::error($a);
            return ['status' => false, 'message' => Yii::t('app', 'Wystąpił problem! Spróbuj ponownie za chwilę.')];
        }
    }

    public static function requestPasswordReset()
    {
        Yii::$app->response->format = 'json';
        try {
            $model = new PasswordResetRequestForm();
            $post = Yii::$app->request->post('PasswordResetRequestForm');
            if (isset($post)) {
                if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                    if ($model->sendEmail()) {
                        return ['status' => true, 'message' => Yii::t('app', 'Sprawdź swoją skrzynkę mailową.')];
                    }
                    return ['status' => false, 'message' => Yii::t('app', 'Przepraszamy, nie możemy zresetować hasła dla podanego adresu e-mail.')];
                }
                return ['status' => false, 'message' => Yii::t('app', 'Nie znaleziono takiego adresu e-mail.')];
            }
            return ['status' => true, 'partial' => Yii::$app->controller->renderPartial('requestPasswordResetToken', ['model' => $model])];
        } catch (Exception $a) {
            Yii::error($a);
            return ['status' => false, 'message' => Yii::t('app', 'Wystąpił problem! Spróbuj ponownie za chwilę.')];
        }
    }

    public static function loginForm()
    {
        Yii::$app->response->format = 'json';
        try {
            if (!Yii::$app->user->isGuest) {
                return ['status' => false, 'message' => Yii::t('app', 'Już jesteś zalogowany!')];
            }
            $model = new LoginForm();
            if (!empty(Yii::$app->request->post())) {
                if ($model->load(Yii::$app->request->post()) && $model->login()) {
                    Yii::$app->session->setFlash('success', Yii::t('app', 'Pomyślnie zalogowano.'));
                    return ['status' => true];
                }
                return ['status' => false, 'message' => Yii::t('app', 'Wprowadzone dane nie są poprawne.')];
            }

            $model->password = '';

            return ['status' => true, 'partial' => Yii::$app->controller->renderPartial('login', ['model' => $model])];
        } catch (Exception $a) {
            Yii::error($a);
            return ['status' => false, 'message' => Yii::t('app', 'Wystąpił problem! Spróbuj ponownie za chwilę.')];
        }
    }

    public static function resetPassword()
    {
        Yii::$app->response->format = 'json';
        try {
            $token = strip_tags(Yii::$app->request->post('token'));
            $model = new ResetPasswordForm($token);

            if ($model->load(Yii::$app->request->post())) {
                if ($model->validate() && $model->resetPassword()) {
                    return ['status' => true, 'message' => Yii::t('app', 'Pomyślnie zmieniono hasło.')];
                } else {
                    return [
                        'status' => false,
                        'message' => !empty($model->getErrors()['password']) ? $model->getErrors()['password'][0] : Yii::t('app', 'Wystąpił problem! Spróbuj ponownie za chwilę.')
                    ];
                }
            }

            return ['status' => true, 'partial' => Yii::$app->controller->renderPartial('resetPassword', [
                'model' => $model,
            ])];
        } catch (Exception $a) {
            Yii::error($a);
            return ['status' => false, 'message' => Yii::t('app', 'Wystąpił problem! Spróbuj ponownie za chwilę.')];
        }
    }

    public static function verifyEmail($token)
    {
        try {
            $model = new VerifyEmailForm($token);
        } catch (InvalidArgumentException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }
        if (($user = $model->verifyEmail()) && Yii::$app->user->login($user)) {
            Yii::$app->session->setFlash('success', Yii::t('app', 'Pomyślnie potwierdzono adres e-mail!'));
            return Yii::$app->controller->goHome();
        }

        Yii::$app->session->setFlash('error', Yii::t('app', 'Przepraszamy, nie możemy zweryfikować twojego konta za pomocą dostarczonego tokenu.'));
        return Yii::$app->controller->goHome();
    }

    public static function resendVerificationEmail()
    {
        $model = new ResendVerificationEmailForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', Yii::t('app', 'Sprawdź swoją skrzynkę e-mail w celu sprawdzenia dalszych instrukcji.'));
                return Yii::$app->controller->goHome();
            }
            Yii::$app->session->setFlash('error', Yii::t('app', 'Przepraszamy, nie byliśmy w stanie ponownie wysłać wiadomości e-mail. Spróbuj ponownie za chwilę.'));
        }

        return Yii::$app->controller->render('resendVerificationEmail', [
            'model' => $model
        ]);
    }

    /**
     * Gets query for [[Group]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(Groups::className(), ['id' => 'group_id']);
    }

    /**
     * Gets query for [[MangaReviews]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMangaReviews()
    {
        return $this->hasMany(MangaReviews::className(), ['added_by' => 'id']);
    }
}
