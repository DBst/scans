<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "manga_images".
 *
 * @property int $id
 * @property int|null $chapter_id
 * @property string|null $image
 */
class MangaImages extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'manga_images';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['chapter_id'], 'integer'],
            [['image'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'chapter_id' => 'Chapter ID',
            'image' => 'Image',
        ];
    }

    public static function imgurCheckExists()
    {
        Yii::$app->response->format = 'json';
        try {
            if (!Yii::$app->user->isGuest) {
                $url = strip_tags(Yii::$app->request->post('url'));
                if (filter_var($url, FILTER_VALIDATE_URL)) {
                    $imgurUrl = explode('https://', $url);
                    if (!empty($imgurUrl[1]) && str_starts_with($imgurUrl[1], 'imgur.com')) {
                        $id = explode('/', $url);
                        $content = @file_get_contents('https://api.imgur.com/3/gallery/' . $id[count($id)-1] . '?client_id=' . Yii::$app->init->config->imgur_client_id . '&response_type=JSON');
                        if ($content) {
                            $json = json_decode($content);
                            $count = count($json->data->images);
                            if ($count > 0) {
                                return ['status' => true, 'message' => Yii::t('app', 'Znaleziono {0} zdjęć.', $count)];
                            }
                            return ['status' => false, 'message' => Yii::t('app', 'Nie znaleziono zdjęć!')];
                        } else {
                            return ['status' => false, 'message' => Yii::t('app', 'Podany link jest nieprawidłowy!') . '<br>' .  Yii::t('app', 'Sprawdź czy przy dodawaniu zdjęć na imgur ustawiłeś galerię zdjęć jako publiczną (musisz wcześniej podać tytuł oraz kliknąć przycisk "To Community")')];
                        }
                    } else {
                        return ['status' => false, 'message' => Yii::t('app', 'Podany link powinien wyglądać tak:') . ' https://imgur.com/gallery/XYZ'];
                    }
                }
                return ['status' => false, 'message' => Yii::t('app', 'Podany link jest nieprawidłowy!')];
            }
            return ['status' => false];
        } catch (Exception $a) {
            return ['status' => false];
        }
    }
}
