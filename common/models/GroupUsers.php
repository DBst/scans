<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "group_users".
 *
 * @property int $id
 * @property int|null $group_id
 * @property int|null $user_id
 */
class GroupUsers extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'group_users';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['group_id', 'user_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'group_id' => 'Group ID',
            'user_id' => 'User ID',
        ];
    }

    public function getGroups()
    {
        return $this->hasOne(Groups::className(), ['id' => 'group_id']);
    }
}
