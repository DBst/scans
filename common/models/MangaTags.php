<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "manga_tags".
 *
 * @property int $id
 * @property int|null $manga_id
 * @property int|null $tag_id
 */
class MangaTags extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'manga_tags';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['manga_id', 'tag_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'manga_id' => 'Manga ID',
            'tag_id' => 'Tag ID',
        ];
    }
}
