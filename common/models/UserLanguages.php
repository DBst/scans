<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "user_languages".
 *
 * @property int $id
 * @property int|null $user_id
 * @property int|null $language_id
 */
class UserLanguages extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_languages';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'language_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'language_id' => 'Language ID',
        ];
    }
}
