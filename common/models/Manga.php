<?php

namespace common\models;

use app\models\Functions;
use Yii;

/**
 * This is the model class for table "manga".
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $description
 * @property string|null $image
 * @property int|null $type
 * @property int|null $added_at
 * @property int|null $added_by
 */
class Manga extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'manga';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['description'], 'string'],
            [['type', 'added_at', 'added_by'], 'integer'],
            [['name', 'image'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
            'image' => 'Image',
            'type' => 'Type',
            'added_at' => 'Added At',
            'added_by' => 'Added By',
        ];
    }

    public static function getMangaPage()
    {
        try {
            $id = (int) Yii::$app->request->get('id');
            if (!empty($id)) {
                $user_id = Yii::$app->user->id;

                $languages = '';
                if(!empty(Yii::$app->init->user_languages)) {
                    $languages = ' AND ( ';
                    foreach (Yii::$app->init->user_languages as $language) {
                        $languages .= " language = $language OR";
                    }
                    $languages = substr($languages, 0, -2);
                    $languages .= ' ) ';
                }

                $sql = "
SELECT *, IF(chapters_count is null, 0, chapters_count) as chapters_count FROM (
    SELECT
        m.*, f.id follow, COUNT(mr.id) reviews_count, (SELECT 
                MAX(chapter_number)
            FROM 
                manga_chapters
            WHERE
                manga_id = m.id 
            $languages 
            AND (
                    status = " . MangaChapters::STATUS_SUCCESS_BY_TRUSTED . " 
                OR 
                    status = " . MangaChapters::STATUS_SUCCESS . "  
            )
        ) as chapters_count 
    FROM manga m
    LEFT JOIN `follows` f 
        ON m.id = f.manga_id AND f.user_id = '$user_id'
    LEFT JOIN `manga_reviews` mr 
        ON m.id = mr.manga_id
    WHERE
        m.id = '$id'
) as t
";
                $manga = Yii::$app->db->createCommand($sql)->cache(60*60*24)->queryOne();
                $sql = <<<EOT
                    SELECT 
                        *
                    FROM tags t
                    LEFT JOIN `manga_tags` mt 
                        ON t.id = mt.tag_id
                    WHERE
                        mt.manga_id = '$id';
                EOT;
                $tags = Yii::$app->db->createCommand($sql)->cache(60*60*24)->queryAll();
                $statusSuccess = \common\models\MangaChapters::STATUS_SUCCESS;
                $statusSuccessByTrusted = \common\models\MangaChapters::STATUS_SUCCESS_BY_TRUSTED;
                $languages = '';
                if(!empty(Yii::$app->init->user_languages)) {
                    $languages = ' AND ( ';
                    foreach(Yii::$app->init->user_languages as $language) {
                        $languages .= " mc.language = $language OR";
                    }
                    $languages = substr($languages, 0, -2);
                    $languages .= ' ) ';
                }
                $sql = <<<EOT
                    SELECT 
                        mc.id id, mc.manga_id manga_id, mc.chapter_number chapter_number, mc.chapter_name chapter_name, mc.language language, g.name group_name, u.username username, mc.added_at added_at
                    FROM manga_chapters mc
                    LEFT JOIN users u 
                        ON mc.added_by = u.id
                    LEFT JOIN `groups` g 
                        ON u.group_id = g.id
                    WHERE
                        mc.manga_id = '$id'
                    $languages
                    AND
                        mc.status = '$statusSuccess'
                    OR
                        mc.status = '$statusSuccessByTrusted'
                    ORDER BY
                        chapter_number DESC;
                    UPDATE `manga` SET `views` = `views`+1 WHERE `manga`.`id` = '$manga[id]'; 
                EOT;
                $chapters = Yii::$app->db->createCommand($sql)->queryAll();
                $comments = Comments::find()->joinWith(['user', 'childs childs'])->where(['AND', ['comments.manga_id' => $manga['id']], ['comments.parent_id' => null], ['comments.chapter_id' => null]])->all();
                return Yii::$app->controller->render('index', [
                    'manga' => $manga,
                    'chapters' => $chapters,
                    'tags' => $tags,
                    'comments' => $comments
                ]);
            }
            return Functions::errorPage();
        } catch (\Exception $a) {
            Yii::error($a);
            var_dump($a);
            exit;
            return Functions::errorPage();
        }
    }

    public static function getMangaListPage()
    {
        try {

            $languages = '';
            if(!empty(Yii::$app->init->user_languages)) {
                $languages = ' AND ( ';
                foreach (Yii::$app->init->user_languages as $language) {
                    $languages .= " language = $language OR";
                }
                $languages = substr($languages, 0, -2);
                $languages .= ' ) ';
            }

            $tags = strip_tags(Yii::$app->request->get('tags'));
            $name = strip_tags(Yii::$app->request->get('name'));
            $page = (!empty(Yii::$app->request->get('page')) ? (int) Yii::$app->request->get('page') - 1 : 0);
            $limit = 30;
            $offset = $page > 0 ? $page * $limit : 0;
            if (!empty($tags)) {
                $tags = explode('|', $tags);
            }
            $join = '';
            if (!empty($tags)) {
                $join .= ' INNER JOIN `manga_tags` mt ON m.id = mt.manga_id AND ( ';
                foreach ($tags as $k => $tag) {
                    $join .= " mt.tag_id = $tag ";
                    if ($k !== count($tags)-1) {
                        $join .= ' OR ';
                    }
                }
                $join .= ' ) ';
            }
            $where = '';
            if (!empty($name)) {
                $where .= " WHERE LOWER(m.name) LIKE '%$name%'";
            }
            $sql = "
SELECT *, IF(chapters_count is null, 0, chapters_count) as chapters_count FROM (
    SELECT
        m.*, COUNT(mr.id) reviews_count, (
            SELECT 
                GROUP_CONCAT(`name`) tags
            FROM manga_tags mt
            LEFT JOIN `tags` t 
                ON mt.tag_id = t.id
            WHERE 
                mt.manga_id = m.id
        ) tags, (SELECT 
                MAX(chapter_number)
            FROM 
                manga_chapters
            WHERE
                manga_id = m.id 
            $languages 
            AND (
                    status = " . MangaChapters::STATUS_SUCCESS_BY_TRUSTED . " 
                OR 
                    status = " . MangaChapters::STATUS_SUCCESS . "  
            )
        ) as chapters_count 
    FROM manga m
    $join
    LEFT JOIN `manga_reviews` mr 
        ON m.id = mr.manga_id
    $where
    GROUP BY
        m.id
    " . (Yii::$app->init->config->user_settings->hide_noChapter ? ' HAVING chapters_count IS NOT NULL ' : '') . "
    LIMIT $limit
    OFFSET $offset
) as t";
            $manga_list = Yii::$app->db->createCommand($sql)->queryAll();
            $sql = "
SELECT
    m.id
FROM manga m
$join
$where
GROUP BY
    m.id
" . (Yii::$app->init->config->user_settings->hide_noChapter ? " HAVING (SELECT 
                MAX(chapter_number)
            FROM 
                manga_chapters
            WHERE
                manga_id = m.id 
            $languages 
            AND (
                    status = " . MangaChapters::STATUS_SUCCESS_BY_TRUSTED . " 
                OR 
                    status = " . MangaChapters::STATUS_SUCCESS . "  
            )
        ) IS NOT NULL " : '') . "
";
            $manga_count = count(Yii::$app->db->createCommand($sql)->queryAll());
            $tags = \common\models\Tags::find()->all();
            return Yii::$app->controller->render('list', [
                'manga_list' => $manga_list,
                'manga_count' => $manga_count,
                'tags' => $tags,
                'page' => ++$page,
                'limit' => $limit
            ]);
        } catch (\Exception $a) {
            Yii::error($a);
            var_dump($a);
            exit;
            return Functions::errorPage();
        }
    }

    public static function syncReadList(): array
    {
        Yii::$app->response->format = 'json';
        try {
            if (!Yii::$app->user->isGuest) {
                $status = (int) Yii::$app->request->post('status'); // 0 - download, 1 - upload
                if ($status === 0) {
                    $readList = \common\models\ReadList::find()->where(['user_id' => Yii::$app->user->id])->one();
                    if(!empty($readList) && !empty($readList->list)) {
                        return ['status' => true, 'data' => $readList->list];
                    }
                } elseif($status === 1) {
                    $readList = \common\models\ReadList::find()->where(['user_id' => Yii::$app->user->id])->one();
                    if(empty($readList)) {
                        $readList = new ReadList();
                        $readList->user_id = Yii::$app->user->id;
                    }
                    $readList->list = strip_tags(Yii::$app->request->post('list'));
                    if($readList->save()) {
                        return ['status' => true];
                    }
                }
            }
            return ['status' => false];
        } catch (\Exception $a) {
            Yii::error($a);
            return ['status' => false, 'message' => Yii::t('app', 'Wystąpił problem podczas synchronizacji danych.')];
        }
    }

    public function getChapters()
    {
        return $this->hasMany(MangaChapters::className(), ['manga_id' => 'id']);
    }
}
