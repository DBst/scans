<?php

namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

enum UserStatus: int {
    case DELETED = 0;
    case INACTIVE = 9;
    case ACTIVE = 10;
}

enum UserRole: int {
    case USER = 0;
    case VERIFIED = 1;
    case TRUSTED = 2;
    case DONATOR = 3;

    case SUPPORT = 10;

    case MODERATOR = 20;
    case JRMODERATOR = 21;
    case HDMODERATOR = 22;

    case ADMIN = 30;
    case JRADMIN = 31;
    case HDADMIN = 32;

    case ROOT = 50;
}

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $verification_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 */
class User extends ActiveRecord implements IdentityInterface
{

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['status', 'default', 'value' => UserStatus::INACTIVE->value],
            ['status', 'in', 'range' => [UserStatus::ACTIVE->value, UserStatus::INACTIVE->value, UserStatus::DELETED->value]],
            ['role', 'default', 'value' => UserRole::USER->value],
            ['role', 'in', 'range' => [
                UserRole::USER->value, UserRole::VERIFIED->value, UserRole::DONATOR->value,
                UserRole::SUPPORT->value,
                UserRole::JRMODERATOR->value, UserRole::MODERATOR->value, UserRole::HDMODERATOR->value,
                UserRole::JRADMIN->value, UserRole::ADMIN->value, UserRole::HDADMIN->value
            ]],
        ];
    }

    public static function getRoleName($user = false)
    {
        if(!$user) {
            $user = Yii::$app->user->identity;
        }
        if($user->role == UserRole::USER->value) {
            return Yii::t('app', 'Użytkownik');
        } elseif (self::isVerified()) {
            return Yii::t('app', 'Zweryfikowany');
        } elseif (self::isTrusted()) {
            return Yii::t('app', 'Zaufany');
        } elseif ($user->role == UserRole::DONATOR->value) {
            return Yii::t('app', 'Donator');
        } elseif (self::isSupport()) {
            return Yii::t('app', 'Support');
        } elseif (self::isModerator()) {
            return Yii::t('app', 'Moderator');
        } elseif (self::isAdmin()) {
            return Yii::t('app', 'Admin');
        }
        return null;
    }

    /**
     * Checks if the user is verified
     *
     * @return bool
     */
    public static function isVerified()
    {
        $user = Yii::$app->user;
        if (!$user->isGuest) {
            if ($user->identity->role == UserRole::VERIFIED->value
                || self::isTrusted()
                || self::isSupport()
                || self::isAdmin()
            ) {
                return true;
            }
        }
        return false;
    }

    /**
     * Checks if the user is trusted
     *
     * @return bool
     */
    public static function isTrusted()
    {
        $user = Yii::$app->user;
        if (!$user->isGuest) {
            if ($user->identity->role == UserRole::TRUSTED->value) {
                return true;
            }
        }
        return false;
    }

    /**
     * Checks if the user is an admin
     *
     * @return bool
     */
    public static function isAdmin()
    {
        $user = Yii::$app->user;
        if (!$user->isGuest) {
            if ($user->identity->role == UserRole::HDADMIN->value
                || $user->identity->role == UserRole::ADMIN->value
                || $user->identity->role == UserRole::JRADMIN->value
            ) {
                return true;
            }
        }
        return false;
    }


    /**
     * Checks if the user is an moderator
     *
     * @return bool
     */
    public static function isModerator()
    {
        $user = Yii::$app->user;
        if (!$user->isGuest) {
            if ($user->identity->role == UserRole::HDMODERATOR->value
                || $user->identity->role == UserRole::MODERATOR->value
                || $user->identity->role == UserRole::JRMODERATOR->value
            ) {
                return true;
            }
        }
        return false;
    }


    /**
     * Checks if the user is an support
     *
     * @return bool
     */
    public static function isSupport()
    {
        $user = Yii::$app->user;
        if (!$user->isGuest) {
            if ($user->identity->role == UserRole::SUPPORT->value) {
                return true;
            }
        }
        return false;
    }


    /**
     * Checks if the user belongs to the staff
     *
     * @return bool
     */
    public static function isStaff()
    {
        $user = Yii::$app->user;
        if (!$user->isGuest) {
            if (self::isModerator() || self::isAdmin()) {
                return true;
            }
        }
        return false;
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => UserStatus::ACTIVE->value]);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => UserStatus::ACTIVE->value]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => UserStatus::ACTIVE->value,
        ]);
    }

    /**
     * Finds user by verification email token
     *
     * @param string $token verify email token
     * @return static|null
     */
    public static function findByVerificationToken($token)
    {
        return static::findOne([
            'verification_token' => $token,
            'status' => UserStatus::INACTIVE->value
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    public function getSettings()
    {

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Generates new token for email verification
     */
    public function generateEmailVerificationToken()
    {
        $this->verification_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }
}
