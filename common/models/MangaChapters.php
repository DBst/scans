<?php

namespace common\models;

use app\models\Functions;
use Yii;

/**
 * This is the model class for table "manga_chapters".
 *
 * @property int $id
 * @property int|null $manga_id
 * @property int $chapter_number
 * @property int|null $type
 * @property string|null $imgur_id
 * @property string|null $language
 * @property int|null $added_at
 * @property int|null $added_by
 */
class MangaChapters extends \yii\db\ActiveRecord
{
    const STATUS_IS_WAITING = 0;
    const STATUS_SUCCESS = 1;
    const STATUS_REJECTED = 2;
    const STATUS_SUCCESS_BY_TRUSTED = 3;
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'manga_chapters';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['manga_id', 'chapter_number', 'type', 'added_at', 'added_by'], 'integer'],
            [['chapter_number'], 'required'],
            [['imgur_id'], 'string', 'max' => 255],
            [['language'], 'string', 'max' => 5],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'manga_id' => 'Manga ID',
            'chapter_number' => 'Chapter Number',
            'type' => 'Type',
            'imgur_id' => 'External Url',
            'language' => 'Language',
            'added_at' => 'Added At',
            'added_by' => 'Added By',
        ];
    }

    public static function getStatusName($status)
    {
        if ($status == self::STATUS_IS_WAITING) {
            return Yii::t('app', 'Oczekuje na zaakceptowanie');
        } elseif ($status == self::STATUS_SUCCESS) {
            return Yii::t('app', 'Zaakceptowany');
        } elseif ($status == self::STATUS_REJECTED) {
            return Yii::t('app', 'Odrzucony');
        } elseif ($status == self::STATUS_SUCCESS_BY_TRUSTED) {
            return Yii::t('app', 'Zaakceptowany jako zaufany');
        }
        return '-';
    }

    public static function getReadPage()
    {
        try {
            if (!empty(Yii::$app->request->get('id')) && !empty(Yii::$app->request->get('chapter'))) {
                $chapter = \common\models\MangaChapters::find()
                    ->joinWith(['manga'])
                    ->where(['manga_chapters.manga_id' => (int) Yii::$app->request->get('id')])
                    ->andWhere(['manga_chapters.id' => (int) Yii::$app->request->get('chapter')])
                    ->one();
                $chapter->manga->views = (int) $chapter->manga->views + 1;
                $chapter->views = (int) $chapter->views + 1;
                $chapter->manga->save();
                $chapter->save();
                $images = false;
                if ($chapter->type == 2) {
                    $content = @file_get_contents('https://api.imgur.com/3/gallery/' . $chapter->imgur_id . '?client_id=' . Yii::$app->init->config->imgur_client_id . '&response_type=JSON');
                    if ($content) {
                        $json = json_decode($content);
                        $images = $json->data->images;
                    }
                } elseif ($chapter->type == 3) {
                    $MangaImages = \common\models\MangaImages::find()->where(['chapter_id' => (int) Yii::$app->request->get('chapter')])->all();
                    $images = [];
                    if (!empty($chapter->group)) {
                        $header_img = '/images/upload/groups/' . $chapter->group->id . '_header.webp';
                        $images[]['link'] = $header_img;
                    }
                    foreach ($MangaImages as $k => $image) {
                        if (!empty($image->image)) {
                            $images[]['link'] = '/images/upload/chapters/' . $image->image;
                        } elseif (!empty($image->imgur_url)) {
                            $images[]['link'] = $image->imgur_url;
                        } elseif (!empty($image->imgbb_url)) {
                            $images[]['link'] = $image->imgbb_url;
                        } elseif (!empty($image->freeimage_url)) {
                            $images[]['link'] = $image->freeimage_url;
                        }
                    }
                    if (!empty($chapter->group)) {
                        $footer_img = '/images/upload/groups/' . $chapter->group->id . '_footer.webp';
                        $images[]['link'] = $footer_img;
                    }
                    $images = json_decode(json_encode($images));
                }
                if ($images) {
                    $comments = Comments::find()->joinWith(['user', 'childs childs'])->where(['comments.chapter_id' => $chapter->id])->andWhere(['comments.parent_id' => null])->all();
                    $nextChapter = ($chapter->chapter_number) + 1;
                    $prevChapter = ($chapter->chapter_number) - 1;
                    $chapterInfo = <<<EOT
                        SELECT (
                            SELECT id 
                            FROM manga_chapters 
                            WHERE manga_id = $chapter->manga_id 
                              AND chapter_number = $nextChapter 
                              AND added_by = $chapter->added_by 
                            LIMIT 1
                        ) nextChapterByAuthor, (
                            SELECT id 
                            FROM manga_chapters 
                            WHERE manga_id = $chapter->manga_id 
                              AND chapter_number = $nextChapter
                            LIMIT 1
                        ) nextChapter, (
                            SELECT id 
                            FROM manga_chapters 
                            WHERE manga_id = $chapter->manga_id 
                              AND chapter_number = $prevChapter
                              AND added_by = $chapter->added_by 
                            LIMIT 1
                        ) prevChapterByAuthor, (
                            SELECT id 
                            FROM manga_chapters 
                            WHERE manga_id = $chapter->manga_id 
                              AND chapter_number = $prevChapter
                            LIMIT 1
                        ) prevChapter;
                    EOT;
                    $chapterInfo = Yii::$app->db->createCommand($chapterInfo)->queryOne();
                    return Yii::$app->controller->render('read', [
                        'images' => $images,
                        'chapter' => $chapter,
                        'comments' => $comments,
                        'nextChapterId' => !empty($chapterInfo['nextChapterByAuthor']) ? $chapterInfo['nextChapterByAuthor'] : (!empty($chapterInfo['nextChapter']) ? $chapterInfo['nextChapter'] : null),
                        'prevChapterId' => !empty($chapterInfo['prevChapterByAuthor']) ? $chapterInfo['prevChapterByAuthor'] : (!empty($chapterInfo['prevChapter']) ? $chapterInfo['prevChapter'] : null),
                    ]);
                }
                return Functions::errorPage(503);
            }
            return Functions::errorPage();
        } catch (Exception $a) {
            Yii::error($a);
            return Functions::errorPage();
        }
    }

    public static function saveChapter()
    {
        Yii::$app->response->format = 'json';
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $data = Yii::$app->request->post('data');
            $language = \common\models\Languages::find()->where(['id' => (int) $data['language']])->one();
            if (!empty($data) && !Yii::$app->user->isGuest && !empty($language)) {
                $uploaded_images = [];
                $verified = \common\models\User::isVerified();
                if (!empty($data['images']) || ((int) $data['images_type'] == 2 && $verified)) {
                    if (!empty($data['images']) && count($data['images']) > 25) {
                        return ['status' => false, 'message' => Yii::t('app', 'Limit zdjęć wynosi: 25')];
                    }
                    if (((int) $data['images_type'] == 2 && $verified) || (int) $data['images_type'] == 3) {
                        if ((int) $data['images_type'] == 3) {
                            foreach ($data['images'] as $image) {
                                $size = \app\models\Functions::getBase64Size($image);
                                $maxFileSize = (int) ini_get('upload_max_filesize');
                                $maxFileSize = $maxFileSize > 20 ? 20 : $maxFileSize;
                                if ($size && $size < $maxFileSize) {
                                    $image = \app\models\Functions::uploadImage($image);
                                    if ($image === false) {
                                        return ['status' => false, 'message' => Yii::t('app', 'Wystąpił problem! Spróbuj ponownie za chwilę.')];
                                    }
                                    $uploaded_images[] = $image;
                                } else {
                                    return ['status' => false, 'message' => Yii::t('app', 'Maksymalny rozmiar zdjęcia wynosi {0}MB', $maxFileSize)];
                                }
                            }
                        }
                        $mc = new \common\models\MangaChapters();
                        $mc->manga_id = (int) $data['manga_id'];
                        $mc->chapter_number = (int) $data['chapter_number'];
                        $mc->chapter_name = strip_tags($data['chapter_name']);
                        $mc->type = (int) $data['images_type'];
                        if ($mc->type == 2 && $verified) {
                            $imgur_url = strip_tags($data['imgur_url']);
                            $imgur_domain = explode('https://', $imgur_url);
                            if (!empty($imgur_domain[1]) && str_starts_with($imgur_domain[1], 'imgur.com')) {
                                $id = explode('/', $imgur_url);
                                if (!empty($id) && !empty($id[count($id)-1])) {
                                    $mc->imgur_id = $id[count($id)-1];
                                }
                            }
                        }
                        $mc->language = $language->id;
                        $mc->added_at = time();
                        $mc->added_by = Yii::$app->user->id;
                        if (Yii::$app->user->identity->group_id) {
                            $mc->added_for = Yii::$app->user->identity->group_id;
                        }
                        if (\common\models\User::isTrusted()) {
                            $mc->status = self::STATUS_SUCCESS_BY_TRUSTED;
                        }
                        if ($mc->save() !== false) {
                            $insert = 0;
                            if ($mc->type == 3) {
                                $insert = [];
                                foreach ($uploaded_images as $image) {
                                    if ($image[1] === 0) {
                                        $insert[] = [$mc->id, '', $image[0], $image[2], '', '', '', ''];
                                    } elseif ($image[1] == 1) {
                                        $insert[] = [$mc->id, '', '', '', $image[0], $image[2], '', ''];
                                    } elseif ($image[1] == 2) {
                                        $insert[] = [$mc->id, '', '', '', '', '', $image[0], $image[2]];
                                    } elseif ($image[1] == 10) {
                                        $insert[] = [$mc->id, $image[0], '', '', '', '', '', ''];
                                    } else {
                                        return ['status' => false, 'message' => Yii::t('app', 'Wystąpił problem! Spróbuj ponownie za chwilę.')];
                                    }
                                }
                                $insert = Yii::$app->db->createCommand()->batchInsert('manga_images', ['chapter_id', 'image', 'imgur_url', 'imgur_delete_hash', 'imgbb_url', 'imgbb_delete_url', 'freeimage_url', 'freeimage_delete_url'], $insert)->execute();
                            }
                            if ($insert !== 0 || ($mc->type == 2 && $verified && !empty($mc->imgur_id))) {
                                $transaction->commit();
                                return ['status' => true, 'message' => Yii::t('app', 'Pomyślnie przesłano chapter! Poinformujemy cię kiedy zostanie zaakceptowany.')];
                            }
                        }
                    }
                } else {
                    return ['status' => false, 'message' => Yii::t('app', 'Musisz dodać jakieś zdjęcia.')];
                }
            } else {
                return ['status' => false, 'message' => Yii::t('app', 'Najpierw uzupełnij wszystkie wymagane pola.')];
            }
            $transaction->rollBack();
            return ['status' => false, 'message' => Yii::t('app', 'Wystąpił problem! Spróbuj ponownie za chwilę.')];
        } catch (\Exception $a) {
            $transaction->rollBack();
            Yii::error($a);
            return ['status' => false, 'message' => Yii::t('app', 'Wystąpił problem! Spróbuj ponownie za chwilę.')];
        }
    }

    public static function setChapterStatus()
    {
        $transaction = Yii::$app->db->beginTransaction();
        Yii::$app->response->format = 'json';
        try {
            $id = (int) Yii::$app->request->post('id');
            $status = (int) Yii::$app->request->post('status');
            if (!empty($id) && !empty($status)) {
                $chapter = \common\models\MangaChapters::find()->joinWith(['manga'])->where(['manga_chapters.id' => $id])->one();
                $oldChapterStatus = $chapter->status;
                $chapter->status = $status;
                if ($chapter->save() !== false) {
                    $notification = new \common\models\Notifications();
                    $notification->user_id = $chapter->added_by;
                    $notification->status = $chapter->status;
                    $notification->added_at = time();
                    if ($chapter->status == self::STATUS_SUCCESS) {
                        $manga_name = $chapter->manga->name;
                        if ($oldChapterStatus == self::STATUS_IS_WAITING) {
                            $notification->message = Yii::t('app', 'Twój chapter dla mangi <b>{0}</b> został zaakceptowany.', $manga_name, !empty($chapter->user->language) ? $chapter->user->language : null);
                        } elseif ($oldChapterStatus == self::STATUS_SUCCESS_BY_TRUSTED) {
                            $notification->message = Yii::t('app', 'Twój chapter dla mangi <b>{0}</b> został potwierdzony.', $manga_name, !empty($chapter->user->language) ? $chapter->user->language : null);
                        }
                        if ($notification->save() !== false) {
                            $chapter->manga->updated_at = time();
                            if ($chapter->chapter_number > $chapter->manga->chapters_count) {
                                $chapter->manga->chapters_count = $chapter->chapter_number;
                                $follows = \common\models\Follows::find()->where(['manga_id' => $chapter->manga_id])->all();
                                $insert = [];
                                $time = time();
                                foreach ($follows as $user) {
                                    $insert[] = [$user->user_id, Yii::t('app', 'Manga <b>{0}</b> otrzymała nowy chapter!', $manga_name, !empty($user->user->language) ? $user->user->language : null), 3, $time];
                                }
                                $insert = Yii::$app->db->createCommand()->batchInsert('notifications', ['user_id', 'message', 'status', 'added_at'], $insert)->execute();
                            }
                            if ($chapter->manga->save() === false) {
                                $transaction->rollBack();
                                return ['status' => false, 'message' => 'Wystąpił problem! Spróbuj ponowanie za chwilę!'];
                            }
                            if (\common\models\Users::updateUserRole()) {
                                $transaction->commit();
                                return ['status' => true, 'message' => 'Pomyślnie zaakceptowano chapter!'];
                            }
                        }
                    } elseif ($chapter->status == self::STATUS_REJECTED) {
                        $notification->message = Yii::t('app', 'Twój chapter dla mangi <b>{0}</b> został odrzucony.', $chapter->manga->name, !empty($chapter->user->language) ? $chapter->user->language : null);
                        if ($notification->save() !== false) {
                            $transaction->commit();
                            return ['status' => true, 'message' => 'Pomyślnie odrzucono chapter!'];
                        }
                    } else {
                        $notification->message = Yii::t('app', 'Status twojego chaptera dla mangi <b>{0}</b> został zmieniony.', $chapter->manga->name, !empty($chapter->user->language) ? $chapter->user->language : null);
                        if ($notification->save() !== false) {
                            $transaction->commit();
                            return ['status' => true, 'message' => 'Pomyślnie zmieniono status!'];
                        }
                    }
                }
            }
            $transaction->rollBack();
            return ['status' => false, 'message' => 'Wystąpił problem! Spróbuj ponowanie za chwilę!'];
        } catch (\Exception $a) {
            $transaction->rollBack();
            Yii::error($a);
            return ['status' => false, 'message' => 'Wystąpił problem! Spróbuj ponowanie za chwilę!'];
        }
    }

    public static function getDataForIndexPage()
    {

        $languages = '';
        if(!empty(Yii::$app->init->user_languages)) {
            $languages = ' AND ( ';
            foreach (Yii::$app->init->user_languages as $language) {
                $languages .= " language = $language OR";
            }
            $languages = substr($languages, 0, -2);
            $languages .= ' ) ';
        }

        $main_carousel = MainCarousel::find()->cache(60*60*24)->all();

        $manga = [
            //order by + limit
            ['views', 12, null],
            ['added_at', 24, null],
            ['updated_at', 20, null],
        ];

        foreach($manga as $k => $v) {
            $sql = "
SELECT *, IF(chapters_count is null, 0, chapters_count) as chapters_count FROM (
    SELECT 
        *, (SELECT 
                MAX(chapter_number)
            FROM 
                manga_chapters
            WHERE
                manga_id = m.id 
            $languages 
            AND (
                    status = " . self::STATUS_SUCCESS_BY_TRUSTED . " 
                OR 
                    status = " . self::STATUS_SUCCESS . "  
            )
        ) as chapters_count 
    FROM 
        manga as m 
    " . (Yii::$app->init->config->user_settings->hide_noChapter ? 'HAVING chapters_count IS NOT NULL ' : '') . "
    ORDER BY 
        $v[0] DESC 
    LIMIT $v[1]
) as t
";

            $manga[$k][2] = Yii::$app->db->createCommand($sql)->cache(60*60*24)->queryAll();
        }
        return [
            'carousel' => $main_carousel,
            'manga' => $manga,
        ];
    }

    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'added_by']);
    }

    public function getManga()
    {
        return $this->hasOne(Manga::className(), ['id' => 'manga_id']);
    }

    public function getGroup()
    {
        return $this->hasOne(Groups::className(), ['id' => 'added_for']);
    }

    public function getLanguages()
    {
        return $this->hasMany(Languages::className(), ['id' => 'language']);
    }
}
