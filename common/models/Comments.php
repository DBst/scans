<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "comments".
 *
 * @property int $id
 * @property int $added_by
 * @property int $manga_id
 * @property int|null $chapter_id
 * @property int|null $parent_id
 * @property string $comment
 * @property string $language
 * @property int $added_at
 */
class Comments extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'comments';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['added_by', 'manga_id', 'comment', 'language', 'added_at'], 'required'],
            [['added_by', 'manga_id', 'chapter_id', 'parent_id', 'added_at'], 'integer'],
            [['comment'], 'string', 'max' => 1000],
            [['language'], 'string', 'max' => 5],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'added_by' => 'User ID',
            'manga_id' => 'Manga ID',
            'chapter_id' => 'Chapter ID',
            'parent_id' => 'Parent ID',
            'comment' => Yii::t('app', 'Komentarz'),
            'language' => 'Language',
            'added_at' => 'Added At',
        ];
    }

    public static function rate(): array
    {
        $transaction = Yii::$app->db->beginTransaction();
        Yii::$app->response->format = 'json';
        try {
            if(!Yii::$app->user->isGuest) {
                if(Yii::$app->request->post('id') && Yii::$app->request->post('rate')) {
                    $rate = CommentsRating::find()->where(['comment_id' => (int) Yii::$app->request->post('id')])->andWhere(['rated_by' => Yii::$app->user->id])->one();
                    if(empty($rate)) {
                        $rate = new CommentsRating();
                        $rate->comment_id = (int) Yii::$app->request->post('id');
                        $rate->rated_by = Yii::$app->user->id;
                        $rate->added_at = time();
                        if(Yii::$app->request->post('rate') == 'plus') {
                            $rate->type = '+';
                        } elseif(Yii::$app->request->post('rate') == 'minus') {
                            $rate->type = '-';
                        } else {
                            return ['status' => false, 'message' => Yii::t('app', 'Wystąpił problem! Spróbuj ponownie za chwilę.')];
                        }
                        if($rate->save() !== false) {
                            $comment = Comments::find()->where(['id' => $rate->comment_id])->one();
                            if($rate->type == '+') {
                                $comment->rate = $comment->rate + 1;
                            } else {
                                $comment->rate = $comment->rate - 1;
                            }
                            if($comment->save() !== false) {
                                $transaction->commit();
                                return ['status' => true, 'message' => Yii::t('app', 'Pomyślnie oceniono komentarz!')];
                            }
                        }
                    } else {
                        return ['status' => false, 'message' => Yii::t('app', 'Już raz oceniłeś ten komentarz!')];
                    }
                }
            } else {
                return ['status' => false, 'message' => Yii::t('app', 'Musisz być zalogowany!')];
            }
            $transaction->rollBack();
            return ['status' => false, 'message' => Yii::t('app', 'Wystąpił problem! Spróbuj ponownie za chwilę.')];
        } catch(\Exception $a) {
            $transaction->rollBack();
            Yii::error($a);
            return ['status' => false, 'message' => Yii::t('app', 'Wystąpił problem! Spróbuj ponownie za chwilę.')];
        }
    }

    public static function add()
    {
        Yii::$app->response->format = 'json';
        try {
            if(!Yii::$app->user->isGuest) {
                $parent_id = (int) Yii::$app->request->post('parent_id');
                if(!empty($parent_id)) {
                    $parentComment = Comments::find()->where(['id' => $parent_id])->one();
                    if(!empty($parentComment->parent_id)) {
                        return ['status' => false, 'message' => Yii::t('app', 'Wystąpił problem! Spróbuj ponownie za chwilę.')];
                    }
                }
                $comment = new Comments();
                $comment->manga_id = (int) Yii::$app->request->post('manga_id');
                $chapter_id = (int) Yii::$app->request->post('chapter_id');
                $comment->chapter_id = !empty($chapter_id) ? $chapter_id : null;
                $comment->comment = strip_Tags(Yii::$app->request->post('comment'));
                $comment->added_by = Yii::$app->user->id;
                $comment->added_at = time();
                if(!empty($parent_id)) {
                    $comment->parent_id = $parent_id;
                }
                $comment->language = Yii::$app->user->identity->language;
                if ($comment->save() !== false) {
                    return ['status' => true, 'message' => Yii::t('app', 'Pomyślnie dodano komentarz!')];
                }
                return ['status' => false, 'message' => $comment->getErrorSummary(true)[0]];
            }
            return ['status' => false, 'message' => Yii::t('app', 'Wystąpił problem! Spróbuj ponownie za chwilę.')];
        } catch(\Exception $a) {
            Yii::error($a);
            return ['status' => false, 'message' => Yii::t('app', 'Wystąpił problem! Spróbuj ponownie za chwilę.')];
        }
    }

    /**
     * Gets query for [[AddedBy]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'added_by']);
    }

    public function getChilds()
    {
        return $this->hasMany(Comments::className(), ['parent_id' => 'id']);
    }
}
