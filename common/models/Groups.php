<?php

namespace common\models;

use app\models\Functions;
use Yii;

/**
 * This is the model class for table "groups".
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property int|null $owner_id
 * @property string|null $url
 * @property int $language
 *
 * @property Manga[] $mangas
 */
class Groups extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'groups';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'description'], 'required'],
            [['owner_id'], 'integer'],
            [['name'], 'string', 'max' => 25],
            [['description'], 'string', 'max' => 2500],
            [['url'], 'string', 'max' => 500],
            [['language'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => Yii::t('app', 'Nazwa'),
            'description' => Yii::t('app', 'Opis'),
            'owner_id' => 'Owner ID',
            'url' => 'Url',
            'language' => Yii::t('app', 'Język'),
        ];
    }

    public static function create()
    {
        $transaction = Yii::$app->db->beginTransaction();
        Yii::$app->response->format = 'json';
        try {
            if (!Yii::$app->user->isGuest) {
                if (empty(Yii::$app->user->identity->group_id)) {
                    if (!empty(Yii::$app->request->post())) {
                        $groups = new Groups();
                        $groups->load(Yii::$app->request->post());
                        $groups->owner_id = Yii::$app->user->id;
                        if ($groups->save() !== false) {
                            $user = Users::find()->where(['id' => Yii::$app->user->id])->one();
                            $user->group_id = $groups->id;
                            if ($user->save() !== false) {
                                $transaction->commit();
                                return ['status' => true, 'message' => Yii::t('app', 'Pomyślnie utworzono grupę!')];
                            }
                        }
                    }
                } else {
                    return ['status' => false, 'message' => Yii::t('app', 'Już należych do grupy!')];
                }
            } else {
                return ['status' => false, 'message' => Yii::t('app', 'Musisz być zalogowany!')];
            }
            $transaction->rollBack();
            return ['status' => false, 'message' => Yii::t('app', 'Wystąpił problem! Spróbuj ponownie za chwilę.')];
        } catch (Exception $a) {
            $transaction->rollBack();
            Yii::error($a);
            return ['status' => false, 'message' => Yii::t('app', 'Wystąpił problem! Spróbuj ponownie za chwilę.')];
        }
    }

    public static function managePage($path)
    {
        if(!Yii::$app->user->isGuest && !empty(Yii::$app->user->identity->group_id)) {
            $group = Groups::find()->joinWith(['chapters', 'users', 'invitations'])->where(['groups.id' => Yii::$app->user->identity->group_id])->one();
            if(!empty($group) && $group->owner_id == Yii::$app->user->id) {
                $chapters = null;
                if($path == 'chapters') {
                    $offset = !empty(Yii::$app->request->get('offset')) ? (int) Yii::$app->request->get('offset') : 0;
                    $chapters = MangaChapters::find()->joinWith(['user', 'manga'])->where(['added_for' => $group->id])->orderBy(['manga_chapters.added_at' => SORT_DESC])->limit(11)->offset($offset)->all();
                }
                return Yii::$app->controller->render('group-manage', ['group' => $group, 'path' => $path, 'chapters' => $chapters]);
            }
            return Functions::errorPage(403);
        }
        return Functions::errorPage(401);
    }

    public static function removeUser()
    {
        Yii::$app->response->format = 'json';
        try {
            $id = (int) Yii::$app->request->post('id');
            if(!empty($id)) {
                $user = Users::find()->joinWith(['group'])->where(['users.id' => $id])->one();
                if($user->group->owner_id !== $user->id && $user->group->owner_id == Yii::$app->user->id) {
                    $user->group_id = null;
                    if($user->save() !== false) {
                        return ['status' => true, 'message' => Yii::t('app', 'Pomyślnie wyrzucono użytkownika z grupy!')];
                    }
                }
            }
            return ['status' => false, 'message' => Yii::t('app', 'Wystąpił problem! Spróbuj ponownie za chwilę.')];
        } catch(\Exception $a) {
            Yii::error($a);
            return ['status' => false, 'message' => Yii::t('app', 'Wystąpił problem! Spróbuj ponownie za chwilę.')];
        }
    }

    public static function saveSettings()
    {
        Yii::$app->response->format = 'json';
        try {
            if(!Yii::$app->user->isGuest) {
                $group = Groups::find()->where(['owner_id' => Yii::$app->user->id])->one();
                $group->load(Yii::$app->request->post());
                $group->last_update = time();
                if(strpos(Yii::$app->request->post('header-img'), 'base64')) {
                    $upload = Functions::uploadImage(Yii::$app->request->post('header-img'), 10, 'groups', $group->id . '_header');
                }
                if(strpos(Yii::$app->request->post('footer-img'), 'base64')) {
                    $upload = Functions::uploadImage(Yii::$app->request->post('footer-img'), 10, 'groups', $group->id . '_footer');
                }
                if($group->save() !== false) {
                    return ['status' => true, 'message' => Yii::t('app', 'Pomyślnie zapisano zmiany!')];
                }
            }
            return ['status' => false, 'message' => Yii::t('app', 'Wystąpił problem! Spróbuj ponownie za chwilę.')];
        } catch(\Exception $a) {
            Yii::error($a);
            return ['status' => false, 'message' => Yii::t('app', 'Wystąpił problem! Spróbuj ponownie za chwilę.')];
        }
    }

    public static function invite()
    {
        $transaction = Yii::$app->db->beginTransaction();
        Yii::$app->response->format = 'json';
        try {
            $username = strip_tags(Yii::$app->request->post('username'));
            if(!empty($username)) {
                $user = Users::find()->where(['username' => $username])->one();
                if(!empty($user)) {
                    if($user->id !== Yii::$app->user->id) {
                        $invite = GroupInvite::find()->where(['AND', ['group_id' => Yii::$app->user->identity->group_id], ['user_id' => $user->id]])->one();
                        if(empty($invite)) {
                            $invite = new GroupInvite();
                            $invite->user_id = $user->id;
                            $invite->group_id = Yii::$app->user->identity->group_id;
                            if ($invite->save() !== false) {
                                $group = Groups::find()->where(['id' => Yii::$app->user->identity->group_id])->one();
                                if(!empty($group)) {
                                    $notify = new Notifications();
                                    $notify->user_id = $user->id;
                                    $notify->message = Yii::t('app', 'Otrzymano zaproszenie do grupy {0}', $group->name, $user->language);
                                    $notify->status = 1;
                                    $notify->added_at = time();
                                    if($notify->save() !== false) {
                                        $transaction->commit();
                                        return ['status' => true, 'message' => Yii::t('app', 'Pomyślnie wysłano zaproszenie do użytkownika {0}', $username)];
                                    }
                                }
                            }
                        } else {
                            return ['status' => false, 'message' => Yii::t('app', 'Ten użytkownik już jest zaproszony.')];
                        }
                    } else {
                        return ['status' => false, 'message' => Yii::t('app', 'Nie możesz zaprosić samego siebie!')];
                    }
                } else {
                    return ['status' => false, 'message' => Yii::t('app', 'Nie znaleziono użytkownika z taką nazwą! Pamiętaj o zachowaniu wielkości znaków.')];
                }
            }
            $transaction->rollBack();
            return ['status' => false, 'message' => Yii::t('app', 'Wystąpił problem! Spróbuj ponownie za chwilę.')];
        } catch(\Exception $a) {
            $transaction->rollBack();
            Yii::error($a);
            return ['status' => false, 'message' => Yii::t('app', 'Wystąpił problem! Spróbuj ponownie za chwilę.')];
        }
    }

    /**
     * Gets query for [[Mangas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getChapters()
    {
        return $this->hasMany(MangaChapters::className(), ['added_for' => 'id']);
    }

    public function getUsers()
    {
        return $this->hasMany(Users::className(), ['group_id' => 'id']);
    }

    public function getInvitations()
    {
        return $this->hasMany(GroupInvite::className(), ['group_id' => 'id']);
    }
}
