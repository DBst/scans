<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "comments_rating".
 *
 * @property int $id
 * @property int|null $comment_id
 * @property int|null $rated_by
 * @property int|null $added_at
 * @property string|null $type
 */
class CommentsRating extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'comments_rating';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['comment_id', 'rated_by', 'added_at'], 'integer'],
            [['type'], 'string', 'max' => 1],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'comment_id' => 'Comment ID',
            'rated_by' => 'Rated By',
            'added_at' => 'Added At',
            'type' => 'Type',
        ];
    }
}
