<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "manga_reviews".
 *
 * @property int $id
 * @property int|null $manga_id
 * @property int|null $added_by
 * @property int|null $added_at
 * @property int|null $updated_at
 * @property float|null $rating
 * @property string|null $review
 *
 * @property Users $addedBy
 */
class MangaReviews extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'manga_reviews';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['manga_id', 'added_by', 'added_at', 'updated_at'], 'integer'],
            [['rating'], 'number'],
            [['review'], 'string'],
            [['added_by'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['added_by' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'manga_id' => 'Manga ID',
            'added_by' => 'Added By',
            'added_at' => 'Added At',
            'updated_at' => 'Updated At',
            'rating' => 'Rating',
            'review' => 'Review',
        ];
    }

    public static function setReview()
    {
        $transaction = Yii::$app->db->beginTransaction();
        Yii::$app->response->format = 'json';
        try {
            if (!Yii::$app->user->isGuest) {
                $manga_id = (int) Yii::$app->request->post('manga_id');
                $review = \common\models\MangaReviews::find()->where(['manga_id' => $manga_id])->andWhere(['added_by' => Yii::$app->user->id])->one();
                $new_record = false;
                if (empty($review)) {
                    $review = new \common\models\MangaReviews();
                    $review->added_by = Yii::$app->user->id;
                    $review->added_at = time();
                    $review->manga_id = $manga_id;
                    $new_record = true;
                } else {
                    $review->updated_at = time();
                }
                $rating = (int) Yii::$app->request->post('rate');
                if (empty($rating)) {
                    return ['status' => false, 'message' => Yii::t('app', 'Musisz ustawić ocenę!')];
                }
                if ($rating > 10 || $rating < 1) {
                    return ['status' => false, 'link' => 'limit-reached.png'];
                }
                $review->rating = $rating;
                $review->review = strip_tags(Yii::$app->request->post('review'));
                if ($review->save() !== false) {
                    $sql = <<<EOT
                        UPDATE `manga` SET `rating` = (
                            SELECT AVG(rating) FROM `manga_reviews` WHERE `manga_reviews`.`manga_id` = $manga_id
                        ) WHERE `manga`.`id` = $manga_id;
                    EOT;
                    Yii::$app->db->createCommand($sql)->execute();
                    $transaction->commit();
                    if ($new_record) {
                        return ['status' => true, 'message' => Yii::t('app', 'Pomyślnie dodano ocenę!')];
                    } else {
                        return ['status' => true, 'message' => Yii::t('app', 'Pomyślnie zaktualizowano ocenę!')];
                    }
                }
            } else {
                return ['status' => false, 'message' => Yii::t('app', 'Musisz być zalogowany!')];
            }
            $transaction->rollBack();
            return ['status' => false, 'message' => Yii::t('app', 'Wystąpił problem! Spróbuj ponownie za chwilę.')];
        } catch (\Exception $a) {
            Yii::error($a);
            return ['status' => false, 'message' => Yii::t('app', 'Wystąpił problem! Spróbuj ponownie za chwilę.')];
        }
    }

    public static function getForm()
    {
        Yii::$app->response->format = 'json';
        try {
            if (!Yii::$app->user->isGuest) {
                if (!empty(Yii::$app->request->post('id'))) {
                    $id = (int) Yii::$app->request->post('id');
                    $review = MangaReviews::find()->where(['manga_id' => $id])->andWhere(['added_by' => Yii::$app->user->id])->one();
                    if (empty($review)) {
                        $review = new MangaReviews();
                    }
                    return ['status' => true, 'partial' => Yii::$app->controller->renderPartial('/site/partials/review-form', ['review' => $review, 'manga_id' => $id])];
                }
            } else {
                return ['status' => false, 'message' => Yii::t('app', 'Musisz być zalogowany!')];
            }
            return ['status' => false, 'message' => Yii::t('app', 'Wystąpił problem! Spróbuj ponownie za chwilę.')];
        } catch (Exception $a) {
            Yii::error($a);
            return ['status' => false, 'message' => Yii::t('app', 'Wystąpił problem! Spróbuj ponownie za chwilę.')];
        }
    }

    /**
     * Gets query for [[AddedBy]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAddedBy()
    {
        return $this->hasOne(Users::className(), ['id' => 'added_by']);
    }
}
