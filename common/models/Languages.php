<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "languages".
 *
 * @property int $id
 * @property string|null $shortname
 * @property string|null $language
 * @property int|null $auto_translate
 */
class Languages extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'languages';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['auto_translate'], 'integer'],
            [['shortname'], 'string', 'max' => 2],
            [['language'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'shortname' => 'Shortname',
            'language' => 'Language',
            'auto_translate' => 'Auto Translate',
        ];
    }

    public static function getCountryCode($ip)
    {
        try {
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, 'https://proxycheck.io/v2/' . $ip . '?asn=1');
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            $out = curl_exec($curl);
            if ($out == false) {
                throw new \Exception(curl_error($curl));
            }
            curl_close($curl);
            $json = json_decode($out);
            if ($json->status == 'ok' && !empty($json->$ip) && !empty($json->$ip->isocode)) {
                return strtolower($json->$ip->isocode);
            }
            return false;
        } catch (\Exception $a) {
            return false;
        }
    }

    public static function setLanguage()
    {
        if (!Yii::$app->user->isGuest) {
            $lang_code = 'en';
            foreach(Yii::$app->init->languages as $language) {
                if($language->id == Yii::$app->init->config->user_settings->main_language) {
                    $lang_code = $language->code;
                }
            }
            Yii::$app->language = $lang_code;
            return true;
        } elseif (!empty($_COOKIE['language-code'])) {
            Yii::$app->language = $_COOKIE['language-code'];
            return true;
        } else {
            $ip = \app\models\Functions::getClientIP();
            $code = self::getCountryCode($ip);
            if ($code) {
                $language = \common\models\Languages::find()->where(['shortname' => $code])->one();
                if (!empty($language)) {
                    setcookie('language-code', $language->code, time() + 60 * 60 * 24 * 31 * 12);
                    Yii::$app->session->setFlash('success', Yii::t('app', 'Ustawiono język zgodnie z twoją lokalizacją.'));
                    Yii::$app->language = $language->code;
                    return true;
                }
            }
            setcookie('language-code', 'en-US', time() + 60 * 60 * 24 * 31 * 12);
            Yii::$app->session->setFlash('success', Yii::t('app', 'Nie udało się znaleźć języka dla twojej lokalizacji. Ustawiono język strony na angielski.'));
            Yii::$app->language = 'en-US';
            return false;
        }
    }

    public static function change()
    {
        Yii::$app->response->format = 'json';
        try {
            if (!empty(Yii::$app->request->post('id'))) {
                $language_id = (int) Yii::$app->request->post('id');
                $language = Languages::find()->where(['id' => $language_id])->one();
                if (!empty($language)) {
                    $main_language = (int) Yii::$app->request->post('main_language');
                    if(!$main_language && Yii::$app->user->isGuest) {
                        return ['status' => false, 'message' => Yii::t('app', 'Najpierw musisz się zalogować!')];
                    }
                    Yii::$app->session->setFlash('success', 'Pomyślnie zmieniono język.');
                    setcookie('language-code', $language->code, time() + (60 * 60 * 24 * 31 * 12));
                    if (!Yii::$app->user->isGuest) {
                        if($main_language) {
                            $user = UserSettings::find()->where(['user_id' => Yii::$app->user->id])->one();
                            $user->main_language = $language->id;
                            if ($user->save() === false) {
                                return ['status' => false, 'message' => Yii::t('app', 'Wystąpił problem! Spróbuj ponownie za chwilę.')];
                            }
                        } else {
                            $lang = UserLanguages::findOne(['user_id' => Yii::$app->user->id, 'language_id' => $language_id]);
                            if(empty($lang)) {
                                $lang = new UserLanguages();
                                $lang->user_id = Yii::$app->user->id;
                                $lang->language_id = $language_id;
                                if ($lang->save() === false) {
                                    return ['status' => false, 'message' => Yii::t('app', 'Wystąpił problem! Spróbuj ponownie za chwilę.')];
                                }
                            } else {
                                $lang = UserLanguages::deleteAll(['user_id' => Yii::$app->user->id, 'language_id' => $language_id]);
                                if($lang === false) {
                                    return ['status' => false, 'message' => Yii::t('app', 'Wystąpił problem! Spróbuj ponownie za chwilę.')];
                                }
                            }
                        }
                    }
                    return ['status' => true];
                }
            }
            return ['status' => false, 'message' => Yii::t('app', 'Wystąpił problem! Spróbuj ponownie za chwilę.')];
        } catch (Exception $a) {
            Yii::error($a);
            return ['status' => false, 'message' => Yii::t('app', 'Wystąpił problem! Spróbuj ponownie za chwilę.')];
        }
    }
}
