<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "group_invite".
 *
 * @property int $id
 * @property int|null $group_id
 * @property int|null $user_id
 */
class GroupInvite extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'group_invite';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['group_id', 'user_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'group_id' => 'Group ID',
            'user_id' => 'User ID',
        ];
    }

    public static function cancel()
    {
        Yii::$app->response->format = 'json';
        try {
            $id = (int) Yii::$app->request->post('id');
            if(!empty($id)) {
                $group = Groups::find()->where(['owner_id' => Yii::$app->user->id])->one();
                if(!empty($group)) {
                    $del = GroupInvite::deleteAll(['AND', ['user_id' => $id], ['group_id' => $group->id]]);
                    if($del !== 0) {
                        return ['status' => true, 'message' => Yii::t('app', 'Pomyślnie anulowano zaproszenie')];
                    }
                    return ['status' => false, 'message' => Yii::t('app', 'Nie udało się znaleźć zaproszenia.')];
                }
            }
            return ['status' => false, 'message' => Yii::t('app', 'Wystąpił problem! Spróbuj ponownie za chwilę.')];
        } catch(\Exception $a) {
            Yii::error($a);
            return ['status' => false, 'message' => Yii::t('app', 'Wystąpił problem! Spróbuj ponownie za chwilę.')];
        }
    }

    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }
}
