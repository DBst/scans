<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "main_carousel".
 *
 * @property int $id
 * @property string|null $title
 * @property string|null $description
 * @property string|null $button
 */
class MainCarousel extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'main_carousel';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['description'], 'string'],
            [['title', 'button'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
            'button' => 'Button',
        ];
    }
}
