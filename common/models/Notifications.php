<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "notifications".
 *
 * @property int $id
 * @property int|null $user_id
 * @property string|null $message
 * @property int|null $status
 */
class Notifications extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'notifications';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'status'], 'integer'],
            [['message'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'message' => 'Message',
            'status' => 'Status',
        ];
    }

    public static function get()
    {
        $transaction = Yii::$app->db->beginTransaction();
        Yii::$app->response->format = 'json';
        try {
            if (!Yii::$app->user->isGuest) {
                $notifications = Notifications::find()->where(['user_id' => (int) Yii::$app->user->id])->orderBy(['added_at' => SORT_DESC])->all();
                Notifications::deleteAll(['user_id' => (int) Yii::$app->user->id]);
                if (!empty($notifications)) {
                    $transaction->commit();
                    return ['status' => true, 'partial' => Yii::$app->controller->renderPartial('/site/partials/notifications-list', ['notifications' => $notifications])];
                }
                return ['status' => true, 'message' => Yii::t('app', 'Brak nowych powiadomień.')];
            }
            $transaction->rollBack();
            return ['status' => false, 'message' => Yii::t('app', 'Wystąpił problem! Spróbuj ponownie za chwilę.')];
        } catch (Exception $a) {
            Yii::error($a);
            return ['status' => false, 'message' => Yii::t('app', 'Wystąpił problem! Spróbuj ponownie za chwilę.')];
        }
    }
}
