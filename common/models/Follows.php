<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "follows".
 *
 * @property int $id
 * @property int|null $manga_id
 * @property int|null $user_id
 */
class Follows extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'follows';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['manga_id', 'user_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'manga_id' => 'Manga ID',
            'user_id' => 'User ID',
        ];
    }

    public static function add()
    {
        Yii::$app->response->format = 'json';
        try {
            if (!Yii::$app->user->isGuest && !empty(Yii::$app->request->post('id'))) {
                $follows = Follows::find()->where(['user_id' => Yii::$app->user->id])->andWhere(['manga_id' => (int) Yii::$app->request->post('id')])->one();
                if (!empty($follows)) {
                    if ($follows->delete() !== false) {
                        return ['status' => true, 'message' => Yii::t('app', 'Pomyślnie usunięto z obserwowanych.'), 'type' => 1];
                    }
                } else {
                    $follows = new Follows();
                    $follows->user_id = Yii::$app->user->id;
                    $follows->manga_id = (int) Yii::$app->request->post('id');
                    if ($follows->save() !== false) {
                        return ['status' => true, 'message' => Yii::t('app', 'Pomyślnie zaobserwowano mangę.'), 'type' => 2];
                    }
                }
            }
            return ['status' => false, 'message' => Yii::t('app', 'Wystąpił problem! Spróbuj ponownie za chwilę.')];
        } catch (Exception $a) {
            Yii::error($a);
            return ['status' => false, 'message' => Yii::t('app', 'Wystąpił problem! Spróbuj ponownie za chwilę.')];
        }
    }

    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }
}
