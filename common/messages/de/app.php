<?php
    return [
        'Twój chapter dla mangi <b>{0}</b> został zaakceptowany.' => '',
        'Manga <b>{0}</b> otrzymała nowy chapter!' => '',
        'Twój chapter dla mangi <b>{0}</b> został odrzucony.' => '',
        'Status twojego chaptera dla mangi <b>{0}</b> został zmieniony.' => '',
        'Ustawiono język zgodnie z twoją lokalizacją.' => '',
        'Nie udało się znaleźć języka dla twojej lokalizacji. Ustawiono język strony na angielski.' => '',
        'Musisz być zalogowany!' => '',
        'Wystąpił problem! Spróbuj ponownie za chwilę.' => '',
        'Musisz ustawić ocenę!' => '',
        'Pomyślnie dodano ocenę!' => '',
        'Pomyślnie zaktualizowano ocenę!' => '',
        'Strona niedostępna' => '',
        'Zaloguj się jeszcze raz' => '',
        'Pomyślnie przesłano chapter! Poinformujemy cię kiedy zostanie zaakceptowany.' => '',
        'Pomyślnie wylogowano.' => '',
        'Brak nowych powiadomień.' => '',
        'Pomyślnie usunięto z obserwowanych.' => '',
        'Pomyślnie zaobserwowano mangę.' => '',
        'Pomyślnie zmieniono język.' => '',
        'Rejestracja przebiegła pomyślnie. Sprawdź swoją skrzynkę mailową i potwierdź adres e-mail.' => '',
        'Już jesteś zalogowany!' => '',
        'Pomyślnie zalogowano!' => '',
        'Wprowadzone dane nie są poprawne.' => '',
        'Sprawdź swoją skrzynkę mailową.' => '',
        'Przepraszamy, nie możemy zresetować hasła dla podanego adresu e-mail.' => '',
        'Nie znaleziono takiego adresu e-mail.' => '',
        'Pomyślnie zmieniono hasło.' => '',
        'Pomyślnie potwierdzono adres e-mail!' => '',
        'Przepraszamy, nie możemy zweryfikować twojego konta za pomocą dostarczonego tokenu.' => '',
        'Sprawdź swoją skrzynkę e-mail w celu sprawdzenia dalszych instrukcji.' => '',
        'Przepraszamy, nie byliśmy w stanie ponownie wysłać wiadomości e-mail. Spróbuj ponownie za chwilę.' => '',
        '{0} sekund temu' => '{0} seconds ago',
        '{0} minut temu' => '{0} minutes ago',
        '{0} godzin temu' => '{0} hours ago',
        '{0} dni temu' => '{0} days ago',
        '{0} miesiące temu' => '{0} months ago',
        '{0} lat temu' => '{0} years ago',
        'Musisz podać nazwę użytkownika!' => '',
        'Ta nazwa użytkownika jest już zajęta.' => '',
        'Musisz podać e-mail!' => '',
        'Ten adres e-mail jest już zajęty.' => '',
        'Musisz podać hasło!' => '',
        'Szukaj...' => '',
        'Powiadomienia' => '',
        'Zaloguj się' => '',
        'Obserwowane' => '',
        'Ustawienia' => '',
        'Wyloguj się' => '',
        'Strona Główna' => '',
        'Mangi' => '',
        'Dodaj chapter' => '',
        'Jeżeli uważasz że to błąd, skontaktuj się z administratorem.' => '',
        'Logowanie' => '',
        'Nazwa użytkownika' => '',
        'Hasło' => '',
        'Zapamiętaj mnie' => '',
        'Zapomniałeś hasła? Możesz je' => '',
        'zresetować' => '',
        'Nie masz konta?' => '',
        'Zarejestruj się' => '',
        'Manga' => '',
        'Opis' => '',
        'Oceń mange' => '',
        'Pierwszy chapter' => '',
        'chapter' => '',
        'Resetowanie hasła' => '',
        'Adres e-mail' => '',
        'Przypomnij hasło' => '',
        'Rejestracja' => '',
        'Zarejestruj' => '',
        'Wrzuć tutaj zdjęcia' => '',
        'Maksymalny rozmiar jednego zdjęcia' => '',
        'Wybierz mangę' => '',
        'Numer chaptera' => '',
        'Wybierz język' => '',
        'Nazwa chaptera' => '',
        'Dodaj' => '',
        'Ostatnio dodane' => '',
        'Ostatnio aktualizowane' => '',
        '{0} wyświetleń' => '',
        'Brak chapterów dla tej mangi' => '',
        'Średnia ocena' => '',
        'Wyświetlenia' => '',
        'Typ' => '',
        'Tagi' => '',
        'Liczba chapterów' => '',
        'Data dodania' => '',
        'Data aktualizacji' => '',
        'Najpopularniejsze' => '',
        'Pokaż więcej' => '',
        'Oceń mangę' => '',
        'Opinia (opcjonalnie)' => '',
    ];
