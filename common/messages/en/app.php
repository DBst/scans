<?php
    return [
        'Twój chapter dla mangi <b>{0}</b> został zaakceptowany.' => 'Your chapter for the manga <b>{0}</b> has been approved.',
        'Manga <b>{0}</b> otrzymała nowy chapter!' => 'Manga <b>{0}</b> got a new chapter!',
        'Twój chapter dla mangi <b>{0}</b> został odrzucony.' => 'Your chapter for the manga <b>{0}</b> has been rejected.',
        'Status twojego chaptera dla mangi <b>{0}</b> został zmieniony.' => 'Your chapter status for the <b>{0}</b> manga has been changed.',
        'Ustawiono język zgodnie z twoją lokalizacją.' => 'The language is set according to your location.',
        'Nie udało się znaleźć języka dla twojej lokalizacji. Ustawiono język strony na angielski.' => 'We couldn\'t find the language for your location. The site language is set to English.',
        'Musisz być zalogowany!' => 'You must be logged in!',
        'Wystąpił problem! Spróbuj ponownie za chwilę.' => 'A problem occured! Please try again in a moment.',
        'Musisz ustawić ocenę!' => 'You need to set a rating!',
        'Pomyślnie dodano ocenę!' => 'Rating successfully added!',
        'Pomyślnie zaktualizowano ocenę!' => 'Rating successfully updated!',
        'Strona niedostępna' => 'Website is unavailable',
        'Zaloguj się jeszcze raz' => 'Log in again',
        'Pomyślnie przesłano chapter! Poinformujemy cię kiedy zostanie zaakceptowany.' => 'Chapter successfully uploaded! We will inform you when it is approved.',
        'Pomyślnie wylogowano.' => 'Successfully logged out.',
        'Brak nowych powiadomień.' => 'No new notifications.',
        'Pomyślnie usunięto z obserwowanych.' => 'Successfully removed from watchlist.',
        'Pomyślnie zaobserwowano mangę.' => 'You have successfully added manga to watchlist.',
        'Pomyślnie zmieniono język.' => 'Language changed successfully.',
        'Rejestracja przebiegła pomyślnie. Sprawdź swoją skrzynkę mailową i potwierdź adres e-mail.' => 'Registration was successful. Check your e-mail and confirm your e-mail address.',
        'Już jesteś zalogowany!' => 'You are already logged in!',
        'Pomyślnie zalogowano!' => 'Successfully logged in!',
        'Wprowadzone dane nie są poprawne.' => 'The entered data is not correct.',
        'Sprawdź swoją skrzynkę mailową.' => 'Check your email inbox.',
        'Przepraszamy, nie możemy zresetować hasła dla podanego adresu e-mail.' => 'Sorry, we are unable to reset your password for the email address provided.',
        'Nie znaleziono takiego adresu e-mail.' => 'No such e-mail address was found.',
        'Pomyślnie zmieniono hasło.' => 'Password changed successfully.',
        'Pomyślnie potwierdzono adres e-mail!' => 'Successfully confirmed e-mail address!',
        'Przepraszamy, nie możemy zweryfikować twojego konta za pomocą dostarczonego tokenu.' => 'Sorry, we are unable to verify your account with the provided token.',
        'Sprawdź swoją skrzynkę e-mail w celu sprawdzenia dalszych instrukcji.' => 'Check your email inbox for further instructions.',
        'Przepraszamy, nie byliśmy w stanie ponownie wysłać wiadomości e-mail. Spróbuj ponownie za chwilę.' => 'Sorry, we were unable to resend the email. Please try again in a moment.',
        '{0} sekund temu' => '{0} seconds ago',
        '{0} minut temu' => '{0} minutes ago',
        '{0} godzin temu' => '{0} hours ago',
        '{0} dni temu' => '{0} days ago',
        '{0} miesiące temu' => '{0} months ago',
        '{0} lat temu' => '{0} years ago',
        'Musisz podać nazwę użytkownika!' => 'You must enter a username!',
        'Ta nazwa użytkownika jest już zajęta.' => 'This username is already taken.',
        'Musisz podać e-mail!' => 'You must enter an e-mail address!',
        'Ten adres e-mail jest już zajęty.' => 'This e-mail address is already taken.',
        'Musisz podać hasło!' => 'You must enter a password!',
        'Szukaj...' => 'Search...',
        'Powiadomienia' => 'Notifications',
        'Zaloguj się' => 'Log in',
        'Obserwowane' => 'Watchlist',
        'Ustawienia' => 'Settings',
        'Wyloguj się' => 'Log out',
        'Strona Główna' => 'Home',
        'Mangi' => 'Manga list',
        'Dodaj chapter' => 'Upload chapter',
        'Jeżeli uważasz że to błąd, skontaktuj się z administratorem.' => 'If you think this is a bug, please contact the administrator.',
        'Logowanie' => 'Login',
        'Nazwa użytkownika' => 'User name',
        'Hasło' => 'Password',
        'Zapamiętaj mnie' => 'Remember me',
        'Zapomniałeś hasła? Możesz je' => 'Have you forgotten your password? You can',
        'zresetować' => 'reset them',
        'Nie masz konta?' => 'You do not have an account?',
        'Zarejestruj się' => 'Register',
        'Manga' => 'Manga',
        'Opis' => 'Description',
        'Oceń mange' => 'Review the manga',
        'Pierwszy chapter' => 'First chapter',
        'Najnowszy chapter' => 'Latest chapter',
        'chapter' => 'chapter',
        'Resetowanie hasła' => 'Reset password',
        'Adres e-mail' => 'E-mail',
        'Zresetuj hasło' => 'Reset your password',
        'Rejestracja' => 'Registration',
        'Zarejestruj' => 'Register',
        'Wrzuć tutaj zdjęcia' => 'Drag photos here',
        'Maksymalny rozmiar jednego zdjęcia' => 'Maximum size of one photo',
        'Wybierz mangę' => 'Select manga',
        'Numer chaptera' => 'Manga number',
        'Wybierz język' => 'Select language',
        'Nazwa chaptera' => 'Chapter name',
        'Dodaj' => 'Send',
        'Ostatnio dodane' => 'Last added',
        'Ostatnio aktualizowane' => 'Last updated',
        '{0} wyświetleń' => '{0} views',
        'Brak chapterów dla tej mangi' => 'There are no chapters for this manga',
        'Średnia ocena' => 'Average rating',
        'Wyświetlenia' => 'Views',
        'Typ' => 'Type',
        'Tagi' => 'Tags',
        'Liczba chapterów' => 'Chapter list',
        'Data dodania' => 'Date added',
        'Data aktualizacji' => 'Update date',
        'Najpopularniejsze' => 'Most popular',
        'Pokaż więcej' => 'Show more',
        'Oceń mangę' => 'Review the manga',
        'Opinia (opcjonalnie)' => 'Review (optional)',
        'Moje chaptery' => 'My chapters',
        'Witaj' => 'Hello',
        'Wejdź w poniższy link by zresetować hasło' => 'Follow the link below to reset your password',
        #tags
        'Akcja' => 'Action',
        'Isekai' => 'Isekai',
    ];
