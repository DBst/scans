<?php

namespace admin\assets;

use yii\web\AssetBundle;

/**
 * Main admin application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'plugins/bootstrap-5.1.3-dist/css/bootstrap.min.css', #https://getbootstrap.com/docs/5.1
        'plugins/MDBootstrap-3.10.2/css/mdb.min.css', #https://mdbootstrap.com
        '//cdn.datatables.net/1.11.1/css/jquery.dataTables.min.css',
        'css/site.css',
    ];
    public $js = [
        'plugins/bootstrap-5.1.3-dist/js/bootstrap.min.js', #https://getbootstrap.com/docs/5.1
        'plugins/MDBootstrap-3.10.2/js/mdb.min.js', #https://mdbootstrap.com
        'https://kit.fontawesome.com/2f350925c5.js', #https://fontawesome.com
        '//cdn.datatables.net/1.11.1/js/jquery.dataTables.min.js', #https://datatables.net/
        '/plugins/Notiflix-2.7.0/notiflix-aio.js', #https://www.notiflix.com/
        'https://cdn.jsdelivr.net/npm/sweetalert2@11', #https://sweetalert2.github.io
        'js/site.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];
}
