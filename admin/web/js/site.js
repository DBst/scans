const App = () => {

// page initialization
    function init() {
        window.dispatchEvent(new Event('resize'));
        setTimeout(function () {
            loader(true);
        }, 50);
        $('#table-chapter-verification').DataTable();
        $('#table-chapter-verification_filter').addClass('d-none');
        updateSidebar();
        document.querySelectorAll('.form-outline').forEach((formOutline) => {
            new mdb.Input(formOutline).update();
        })
    }
    const updateSidebar = () => {
        let loc = window.location.pathname;
        $('#sidebarMenu a.active').removeClass('active');
        $('#sidebarMenu ul.collapse.show').removeClass('show');
        $('#sidebarMenu a[href="' + loc + '"]').addClass('active').closest('.collapse').addClass('show');
    }
    $(document).on('pjax:success', function () {
        init();
    });
    $(document).ready(function () {
        init();
    });
    Notiflix.Loading.Init({
        svgColor: '#6032c6',
        fontFamily: 'Quicksand',
        useGoogleFont: true,
        cssAnimationDuration: 200,
    });
    Notiflix.Notify.Init({
        opacity: 0.95,
        borderRadius: '50px',
        clickToClose: true,
        fontSize: '15px',
        cssAnimationStyle: 'from-right',
    });
    loader();
    $(document).on('click', 'a', function (e) {
        try {
            if (typeof $(this).attr('data-pjax') !== 'undefined' && $('#content').length) {
                e.preventDefault();
                if ($(this).attr('href') !== window.location.pathname) {
                    loader();
                    $.pjax({
                        container: '#content',
                        url: $(this).attr('href'),
                        scrollTo: 0,
                    });
                }
            } else {
                window.location.url = $(this).attr('href');
            }
        } catch (error) {
            window.location.url = $(this).attr('href');
        }
    });
    function loader(load = false, message = 'Ładowanie...') {
        if (load) {
            Notiflix.Loading.Remove();
        } else {
            Notiflix.Loading.Dots(message);
        }
    }
    function ajaxRequest(url, data) {
        data['_csrf-frontend'] = $('meta[name="csrf-token"]').attr('content');
        return $.ajax({
            url: url,
            type: 'POST',
            dataType: 'json',
            data: data
        });
    }

    // section for chapter verification
    $(document).on('click', '.set-chapter-status', function () {
        loader();
        ajaxRequest('/set-chapter-status', {
            id: $(this).attr('data-id'),
            status: $(this).attr('data-status'),
        }).then(function (result) {
            loader(true);
            if (result.status == true) {
                $.pjax.reload({container: '#content'});
                Notiflix.Notify.Success(result.message);
            } else {
                Notiflix.Notify.Failure(result.message);
            }
        });
    });
    $(document).on('click', '#chapter-verification-show', function (e) {
        e.preventDefault();
        loader();
        ajaxRequest('/get-chapter-images', {
            id: $(this).attr('data-id'),
        }).then(function (result) {
            loader(true);
            if (result.status == true) {
                Swal.fire({
                    'html': result.partial
                });
            } else {
                Notiflix.Notify.Failure(result.message);
            }
        });
    });

    // toggle sidebar for desktop users
    $(document).on('click', '.sidebar-toggle-desktop', (e) => {
        if($('.sidebar').hasClass('sidebar-hide')) {
            $('.sidebar').removeClass('sidebar-hide');
            $('main').removeClass('without-sidebar');
        } else {
            $('.sidebar').addClass('sidebar-hide');
            $('main').addClass('without-sidebar');
        }
    });

    // save settings
    $(document).on('submit', '#config-form', (e) => {
        e.preventDefault();
        let data = $(e.currentTarget).serialize();
        loader();
        ajaxRequest('/save-config', data).then(function (result) {
            if (result.status == true) {
                $.pjax.reload({
                    'container': '#content',
                }).then(() => {
                    loader(true);
                });
            } else {
                loader(true);
                Notiflix.Notify.Failure(result.message);
            }
        });
    });
}
App();