<?php foreach ($images as $image): ?>
<img src="<?php
    if (!empty($image->imgur_url)) {
        echo $image->imgur_url;
    } elseif (!empty($image->imgbb_url)) {
        echo $image->imgbb_url;
    } elseif (!empty($image->freeimage_url)) {
        echo $image->freeimage_url;
    } elseif (!empty($image->image)) {
        echo Yii::$app->init->config->protocol . Yii::$app->init->config->domain . '/images/upload/chapters/' . $image->image;
    } ?>" class="w-100">
<?php endforeach;
