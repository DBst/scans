<?php
use yii\widgets\Pjax;

$this->title = Yii::t('app', 'Akceptacja chapterów');
?>
<?php Pjax::begin(['id'=>'content']); ?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="main-card mb-3 card">
                <div class="card-header">
                    <h5 class="mb-0">
                        <i class="fas fa-check me-2"></i>
                        <?= Yii::t('app', 'Akceptacja chapterów') ?>
                    </h5>
                </div>
                <div class="table-responsive">
                    <table class="align-middle mb-0 table table-borderless table-striped table-hover"
                        id="table-chapter-verification">
                        <thead>
                            <tr>
                                <th class="text-center">#</th>
                                <th class="text-center">
                                    <?= Yii::t('app', 'Manga') ?>
                                </th>
                                <th class="text-center">
                                    <?= Yii::t('app', 'Numer chaptera') ?>
                                </th>
                                <th class="text-center">
                                    <?= Yii::t('app', 'Dodane przez') ?>
                                </th>
                                <th class="text-center">
                                    <?= Yii::t('app', 'Język') ?>
                                </th>
                                <th class="text-center">
                                    <?= Yii::t('app', 'Podgląd') ?>
                                </th>
                                <th class="text-center">
                                    <?= Yii::t('app', 'Akceptuj') ?>
                                </th>
                                <th class="text-center">
                                    <?= Yii::t('app', 'Odrzuć') ?>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($chapters as $chapter): ?>
                            <tr>
                                <td class="text-center text-muted">
                                    #<?= $chapter->id ?>
                                </td>
                                <td class="text-center">
                                    <?= $chapter->manga->name ?>
                                </td>
                                <td class="text-center">
                                    <div class="badge badge-success">
                                        <?= $chapter->chapter_number ?>
                                    </div>
                                </td>
                                <td class="text-center">
                                    <div class="widget-content p-0">
                                        <div class="widget-content-wrapper">
                                            <div class="widget-content-left flex2">
                                                <div class="widget-heading"><?= $chapter->user->username ?>
                                                </div>
                                                <div class="widget-subheading opacity-7"><?= !empty($chapter->user->group) ? $chapter->user->group->name : '' ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <?php
                                    $langCode = explode('-', $chapter->language);
                                ?>
                                <td class="text-center">
                                    <img src="/images/site/country-flags/<?= !empty($langCode[1]) ? $langCode[1] : '' ?>.jpg"
                                        class="ms-2 mb-1" width="24">
                                </td>
                                <td class="text-center">
                                    <button type="button" id="chapter-verification-show"
                                        data-id="<?= $chapter->id ?>"
                                        class="btn btn-primary btn-sm">
                                        <?= Yii::t('app', 'Podgląd') ?>
                                    </button>
                                </td>
                                <td class="text-center">
                                    <button type="button" id="chapter-verification-accept"
                                        data-id="<?= $chapter->id ?>"
                                        data-status="1" class="btn btn-success btn-sm set-chapter-status">
                                        <?= Yii::t('app', 'Akceptuj') ?>
                                    </button>
                                </td>
                                <td class="text-center">
                                    <button type="button" id="chapter-verification-refuse"
                                        data-id="<?= $chapter->id ?>"
                                        data-status="2" class="btn btn-danger btn-sm set-chapter-status">
                                        <?= Yii::t('app', 'Odrzuć') ?>
                                    </button>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <div class="d-block text-center card-footer">
                    <button class="me-2 btn-icon btn-icon-only btn btn-outline-danger"><i
                            class="pe-7s-trash btn-icon-wrapper"> </i></button>
                    <button class="btn-wide btn btn-success">Save</button>
                </div>
            </div>
        </div>
    </div>
</div>
<?php Pjax::end();
