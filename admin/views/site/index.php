<?php

use yii\widgets\Pjax;

$this->title = Yii::t('app', 'Strona Główna');
?>
<?php Pjax::begin(['id'=>'content']); ?>
	<div class="container-fluid">
		<div class="row">
			<div class="col-xl-3 col-sm-6 col-12 mb-4">
				<div class="card">
					<div class="card-body">
						<div class="d-flex justify-content-between px-md-1">
							<div>
								<h3 class="text-success">
                                    <?= $usersCount ?>
								</h3>
								<p class="mb-0">
									<?= Yii::t('app', 'Użytkownicy') ?>
								</p>
							</div>
							<div class="align-self-center">
								<i class="far fa-user text-success fa-3x"></i>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xl-3 col-sm-6 col-12 mb-4">
				<div class="card">
					<div class="card-body">
						<div class="d-flex justify-content-between px-md-1">
							<div>
								<h3 class="text-info">
                                    <?= $mangaCount ?>
								</h3>
								<p class="mb-0">
									<?= Yii::t('app', 'Mangi') ?>
								</p>
							</div>
							<div class="align-self-center">
								<i class="fas fa-book-open text-info fa-3x"></i>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xl-3 col-sm-6 col-12 mb-4">
				<div class="card">
					<div class="card-body">
						<div class="d-flex justify-content-between px-md-1">
							<div>
								<h3 class="text-danger">
                                    <?= $chaptersCount ?>
								</h3>
								<p class="mb-0">
									<?= Yii::t('app', 'Chaptery') ?>
								</p>
							</div>
							<div class="align-self-center">
								<i class="fas fa-copy text-danger fa-3x"></i>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xl-3 col-sm-6 col-12 mb-4">
				<div class="card">
					<div class="card-body">
						<div class="d-flex justify-content-between px-md-1">
							<div>
								<h3 class="text-warning">
									<?= $commentsCount ?>
								</h3>
								<p class="mb-0">
									<?= Yii::t('app', 'Komentarze') ?>
								</p>
							</div>
							<div class="align-self-center">
								<i class="far fa-comment-alt text-warning fa-3x"></i>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php Pjax::end();
