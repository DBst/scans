<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap4\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;

$this->title = 'Login';
?>
<div class="site-login text-center">
    <div class="offset-lg-3 col-lg-6 shadow p-4">
	    <div class="pt-3 pb-3">
	        <h1>
	            Sign in
	        </h1>

	        <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>


		        <div class="mt-4">
	                <?= $form->field($model, 'username')->textInput(['value' => !empty($_COOKIE['lastLogin']) ? $_COOKIE['lastLogin'] : '']) ?>
		        </div>

			    <div class="mt-4">
		            <?= $form->field($model, 'password')->passwordInput() ?>
			    </div>

			    <div class="mt-4">
		            <?= $form->field($model, 'rememberMe')->checkbox() ?>
			    </div>

	            <div class="form-group mt-3">
	                <?= Html::submitButton('Login', ['class' => 'btn btn-primary btn-block', 'name' => 'login-button']) ?>
	            </div>

	        <?php ActiveForm::end(); ?>
	    </div>
    </div>
</div>