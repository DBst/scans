<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap4\ActiveForm */
/* @var $settings Array */

use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\widgets\Pjax;
?>
<?php Pjax::begin(['id'=>'content']); ?>
    <form id="config-form" class="container-fluid">
		<!-- settings section-->
	    <div class="card">
		    <div class="card-header">
			    <h5 class="mb-0">
				    <i class="fas fa-cog me-2"></i>
                    <?= Yii::t('app', 'Ustawienia') ?>
			    </h5>
		    </div>
		    <div class="card-body">

			    <div class="container-fluid p-0">
				    <div class="row">
					    <div class="col-md-3 mb-3">
						    <div class="form-outline">
							    <input type="text" id="configTitle" name="title" class="form-control" value="<?= $settings['title'] ?>" />
							    <label class="form-label" for="configTitle">
									<?= Yii::t('app', 'Nazwa strony') ?>
							    </label>
						    </div>
					    </div>
					    <div class="col-md-3 mb-3">
						    <div class="form-outline">
							    <input type="text" id="configDomain" name="domain" class="form-control" value="<?= $settings['domain'] ?>" />
							    <label class="form-label" for="configDomain">
									<?= Yii::t('app', 'Domena') ?>
							    </label>
						    </div>
					    </div>
					    <div class="col-md-3 mb-3">
						    <div class="form-group">
							    <select class="form-select" id="configProtocol" name="protocol" aria-label="Default select example">
								    <option>
										<?= Yii::t('app', 'Protokół') ?>
								    </option>
								    <option <?= $settings['protocol'] == 'http://' ? 'selected' : '' ?> value="1">HTTP</option>
								    <option <?= $settings['protocol'] == 'https://' ? 'selected' : '' ?> value="2">HTTPS</option>
							    </select>
						    </div>
					    </div>
				    </div>
			    </div>
		    </div>
	    </div>
	    <!-- end settings section-->

		<!-- API section-->
	    <div class="card mt-4">
		    <div class="card-header">
			    <h5 class="mb-0">
				    <i class="fas fa-network-wired me-2"></i>
                    <?= Yii::t('app', 'Integracja API') ?>
			    </h5>
		    </div>
		    <div class="card-body">

			    <div class="container-fluid p-0">
				    <div class="row">
					    <div class="col-md-3 mb-3">
						    <div class="form-outline">
							    <input type="text" id="configImgur_client_id" name="imgur_client_id" class="form-control" value="<?= $settings['imgur_client_id'] ?>" />
							    <label class="form-label" for="configImgur_client_id">
									<?= Yii::t('app', 'Imgur Client ID') ?>
							    </label>
						    </div>
					    </div>
					    <div class="col-md-3 mb-3">
						    <div class="form-outline">
							    <input type="text" id="configImgbb_api_key" name="imgbb_api_key" class="form-control" value="<?= $settings['imgbb_api_key'] ?>" />
							    <label class="form-label" for="configImgbb_api_key">
									<?= Yii::t('app', 'ImgBB Api Key') ?>
							    </label>
						    </div>
					    </div>
					    <div class="col-md-3 mb-3">
						    <div class="form-outline">
							    <input type="text" id="configFreeimage_api_key" name="freeimage_api_key" class="form-control" value="<?= $settings['freeimage_api_key'] ?>" />
							    <label class="form-label" for="configFreeimage_api_key">
									<?= Yii::t('app', 'FreeImage Api Key') ?>
							    </label>
						    </div>
					    </div>
				    </div>
			    </div>
		    </div>
	    </div>
	    <!-- end API section-->

	    <div class="form-group text-end mt-4">
		    <div class="col-lg-offset-1 col-lg-12">
			    <button type="submit" class="btn btn-success">
					<?= Yii::t('app', 'Zapisz zmiany') ?>
			    </button>
		    </div>
	    </div>
    </form>
<?php Pjax::end();