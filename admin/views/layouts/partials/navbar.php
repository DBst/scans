
<nav id="main-navbar" class="navbar navbar-expand-lg navbar-light bg-white fixed-top">
	<!-- Container wrapper -->
	<div class="container-fluid">
		<!-- Toggle button -->
		<button class="navbar-toggler"
		        type="button"
		        data-mdb-toggle="collapse"
		        data-mdb-target="#sidebarMenu"
		        aria-controls="sidebarMenu"
		        aria-expanded="false"
		        aria-label="Toggle navigation">
			<i class="fas fa-bars"></i>
		</button>

		<!-- Brand -->
		<a class="navbar-brand ms-2 sidebar-toggle-desktop only-desktop" href="javascript:void(0)">
			<i class="fas fa-bars"></i>
		</a>
		<a class="navbar-brand ms-2 only-desktop" href="/" data-pjax="">
			<?= Yii::t('app', 'Panel Admina') ?> - <?= Yii::$app->init->config->title ?>
		</a>

		<!-- Right links -->
		<ul class="navbar-nav ms-auto d-flex flex-row">
			<li class="nav-item">
				<a class="nav-link me-3 me-lg-0" href="<?= Yii::$app->init->config->protocol ?><?= Yii::$app->init->config->domain ?>/">
					<i class="fas fa-binoculars"></i>
				</a>
			</li>

			<!-- Notification dropdown -->
			<li class="nav-item dropdown">
				<a class="nav-link me-3 me-lg-0 dropdown-toggle hidden-arrow"
				   href="#"
				   id="navbarDropdownMenuLink"
				   role="button"
				   data-mdb-toggle="dropdown"
				   aria-expanded="false">
					<i class="fas fa-bell"></i>
					<span class="badge rounded-pill badge-notification bg-danger">1</span>
				</a>
				<ul class="dropdown-menu dropdown-menu-end"
				    aria-labelledby="navbarDropdownMenuLink">
					<li>
						123
					<li>
						321
					</li>
				</ul>
			</li>

			<!-- Icon dropdown -->
			<li class="nav-item dropdown">
				<a class="nav-link me-3 me-lg-0 dropdown-toggle hidden-arrow"
				   href="#"
				   id="navbarDropdown"
				   role="button"
				   data-mdb-toggle="dropdown"
				   aria-expanded="false"
				>
					<i class="united kingdom flag m-0"></i>
				</a>
				<ul class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
					<li>
						<a class="dropdown-item" href="#">
							<i class="united kingdom flag"></i>
							<?= Yii::t('app', 'English') ?>
							<i class="fa fa-check text-success ms-2"></i>
						</a>
					</li>
					<li><hr class="dropdown-divider" /></li>
					<li>
						<a class="dropdown-item" href="#"><i class="poland flag"></i>Polski</a>
					</li>
					<li>
						<a class="dropdown-item" href="#"><i class="germany flag"></i>Deutsch</a>
					</li>
				</ul>
			</li>

			<!-- Avatar -->
			<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle hidden-arrow d-flex align-items-center"
				   href="#"
				   id="navbarDropdownMenuLink"
				   role="button"
				   data-mdb-toggle="dropdown"
				   aria-expanded="false">
					<img src="https://mdbcdn.b-cdn.net/img/Photos/Avatars/img (31).webp"
					     class="rounded-circle"
					     height="22"
					     alt="Avatar"
					     loading="lazy"/>
				</a>
				<ul class="dropdown-menu dropdown-menu-end"
				    aria-labelledby="navbarDropdownMenuLink">
					<li>
						<a class="dropdown-item" href="/logout">Logout</a>
					</li>
				</ul>
			</li>
		</ul>
	</div>
	<!-- Container wrapper -->
</nav>