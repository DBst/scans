
<nav id="sidebarMenu" class="collapse d-lg-block sidebar collapse bg-white">
    <div class="position-sticky">
        <div class="list-group list-group-flush mx-3 mt-4">
            <a href="/" class="list-group-item list-group-item-action py-2 ripple"
               data-pjax="" aria-current="true">
                <i class="fas fa-home fa-fw me-3"></i>
	            <span>
					<?= Yii::t('app', 'Strona Główna') ?>
	            </span>
            </a>
            <a href="/settings" class="list-group-item list-group-item-action py-2 ripple"
               data-pjax="" aria-current="true">
                <i class="fas fa-cog fa-fw me-3"></i>
	            <span>
					<?= Yii::t('app', 'Ustawienia') ?>
	            </span>
            </a>
	        <!-- Collapse 1 -->
	        <a class="list-group-item list-group-item-action py-2 ripple collapsed"
			        aria-current="true"
			        data-mdb-toggle="collapse"
			        href="#collapseExample1"
			        aria-expanded="true"
			        aria-controls="collapseExample1">
		        <i class="fas fa-book fa-fw me-3"></i>
		        <span>
			        <?= Yii::t('app', 'Chaptery') ?>
		        </span>
	        </a>
	        <!-- Collapsed content -->
	        <ul id="collapseExample1"
			        class="collapse list-group list-group-flush">
		        <a href="/chapter/list" data-pjax="" class="list-group-item py-1">
			        <i class="fas fa-list me-2 ms-3"></i>
					<?= Yii::t('app', 'Lista') ?>
		        </a>
		        <a href="/chapter/verification" data-pjax="" class="list-group-item py-1">
			        <i class="fas fa-check me-2 ms-3"></i>
                    <?= Yii::t('app', 'Weryfikacja') ?>
		        </a>
	        </ul>
	        <!-- Collapse 1 -->
            <a href="#" class="list-group-item list-group-item-action py-2 ripple">
	            <i class="fas fa-users fa-fw me-3"></i>
	            <span>
					<?= Yii::t('app', 'Użytkownicy') ?>
	            </span>
            </a>
        </div>
    </div>
</nav>