<?php
if (!Yii::$app->user->isGuest): ?>
	<!--Main Navigation-->
	<header>
		<!-- Sidebar -->
        <?= Yii::$app->controller->renderPartial('/layouts/partials/sidebar') ?>
		<!-- Sidebar -->

		<!-- Navbar -->
        <?= Yii::$app->controller->renderPartial('/layouts/partials/navbar') ?>
		<!-- Navbar -->
	</header>
	<!--Main Navigation-->

	<!--Main layout-->
	<main style="margin-top: 58px;">
		<div class="container pt-4"></div>
	</main>
	<!--Main layout-->
<?php endif;
