<?php

namespace admin\controllers;

use common\models\LoginForm;
use Yii;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index', 'chapter-verification', 'set-chapter-status', 'get-chapter-images', 'settings', 'save-config'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    // 'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays dashboard.
     */
    public function actionIndex(): string
    {
        $usersCount = \common\models\Users::find()->count();
        $mangaCount = \common\models\Manga::find()->count();
        $chaptersCount = \common\models\MangaChapters::find()->count();
        $commentsCount = \common\models\Comments::find()->count();
        return $this->render('index', [
            'usersCount' => $usersCount,
            'mangaCount' => $mangaCount,
            'chaptersCount' => $chaptersCount,
            'commentsCount' => $commentsCount
        ]);
    }

    /**
     * Displays settings page.
     */
    public function actionSettings(): string
    {
        $config = \common\models\Config::find()->all();
        $settings = [];
        foreach ($config as $item) {
            $settings[$item['name']] = $item['value'];
        }
        return $this->render('settings', [
            'settings' => $settings
        ]);
    }

    /**
     * Feature that allows you, to save site settings.
     */
    public function actionSaveConfig(): Array
    {
        $trans = Yii::$app->db->beginTransaction();
        Yii::$app->response->format = 'json';
        try {
            if (\common\models\Config::deleteAll()) {
                $protocol = (int) Yii::$app->request->post('protocol');
                $tableName = \common\models\Config::tableName();
                Yii::$app->db->createCommand("ALTER TABLE $tableName AUTO_INCREMENT = 0;")->execute();
                $config = Yii::$app->db->createCommand()->batchInsert(\common\models\Config::tableName(), ['name', 'value'], [
                    ['title', strip_tags(Yii::$app->request->post('title'))],
                    ['protocol', $protocol == 1 ? 'http://' : ($protocol == 2 ? 'https://' : '')],
                    ['domain', strip_tags(Yii::$app->request->post('domain'))],
                    ['imgur_client_id', strip_tags(Yii::$app->request->post('imgur_client_id'))],
                    ['imgbb_api_key', strip_tags(Yii::$app->request->post('imgbb_api_key'))],
                    ['freeimage_api_key', strip_tags(Yii::$app->request->post('freeimage_api_key'))],
                ])->execute();
                if($config) {
                    $trans->commit();
                    Yii::$app->cache->flush();
                    return ['status' => true, 'message' => Yii::t('app', 'Pomyślnie zapisano ustawienia!')];
                }
            }
            $trans->rollBack();
            return ['status' => false, 'message' => Yii::t('app', 'Wystąpił problem! Spróbuj ponownie za chwilę.')];
        } catch (\Exception $a) {
            Yii::error($a);
            $trans->rollBack();
            return ['status' => false, 'message' => Yii::t('app', 'Wystąpił problem! Spróbuj ponownie za chwilę.')];
        }
    }

    /**
     * Displays Login Site & Login action.
     */
    public function actionLogin(): string|Response
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        //remember me feature
        if(!empty(Yii::$app->request->post())) {
            if((int) Yii::$app->request->post('LoginForm')['rememberMe'] === 1) {
                var_dump(strip_tags(Yii::$app->request->post('LoginForm')['username']));
                setcookie('lastLogin', strip_tags(Yii::$app->request->post('LoginForm')['username']), time() + 60 * 60 * 24 * 31 * 12);
            }
        }

        $this->layout = 'blank';
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';

        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     */
    public function actionLogout(): Response
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
