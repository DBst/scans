<?php

namespace admin\controllers;

class ChapterController extends \yii\web\Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionVerification()
    {
        $chapters = \common\models\MangaChapters::find()
            ->joinWith(['manga', 'user'])
            ->where(['manga_chapters.status' => \common\models\MangaChapters::STATUS_IS_WAITING])
            ->orWhere(['manga_chapters.status' => \common\models\MangaChapters::STATUS_SUCCESS_BY_TRUSTED])
            ->all();
        return $this->render('verification', ['chapters' => $chapters]);
    }

    public function actionSetChapterStatus()
    {
        Yii::$app->response->format = 'json';
        try {
            return \common\models\MangaChapters::setChapterStatus();
            return ['status' => false, 'message' => Yii::t('app', 'Wystąpił problem! Spróbuj ponowanie za chwilę!')];
        } catch (\Exception $a) {
            Yii::error($a);
            return ['status' => false, 'message' => Yii::t('app', 'Wystąpił problem! Spróbuj ponowanie za chwilę!')];
        }
    }


    public function actionGetChapterImages()
    {
        Yii::$app->response->format = 'json';
        try {
            $id = (int) Yii::$app->request->post('id');
            if (!empty($id)) {
                $images = \common\models\MangaImages::find()->where(['chapter_id' => $id])->all();
                if (empty($images)) {
                    $chapter = \common\models\MangaChapters::find()->where(['id' => $id])->one();
                    if (!empty($chapter->imgur_id)) {
                        $content = @file_get_contents('https://api.imgur.com/3/gallery/' . $chapter['imgur_id'] . '?client_id=' . Yii::$app->init->config->imgur_client_id . '&response_type=JSON');
                        if ($content) {
                            $json = json_decode($content);
                            $images = [];
                            if (!empty($json) && !empty($json->data) && !empty($json->data->images)) {
                                foreach ($json->data->images as $img) {
                                    $images[] = [
                                        'imgur_url' => $img->link
                                    ];
                                }
                                $images = json_decode(json_encode($images));
                            }
                        }
                    }
                }
                if (!empty($images)) {
                    return ['status' => true, 'partial' => $this->renderPartial('partials/images', ['images' => $images])];
                }
            }
            return ['status' => false, 'message' => Yii::t('app', 'Wystąpił problem! Spróbuj ponowanie za chwilę!')];
        } catch (\Exception $a) {
            Yii::error($a);
            return ['status' => false, 'message' => Yii::t('app', 'Wystąpił problem! Spróbuj ponowanie za chwilę!')];
        }
    }

}
