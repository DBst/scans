<p align="center">
    <h1 align="center">Hyakki 1.2</h1>
</p>

Hyakki is the simple website, which allows you to read manga and a lot more.<br>
Hyakki was created using the following specification:
- PHP 8.0.13 (Required +8.0)
- MySQL 5.7.36
- Yii 2.0.45

Created using the following libraries:
- Bootstrap 5.1.3
- MDBootstrap 3.10.2
- jQuery 3.6
- PJAX
- Font Awesome 5
- Notiflix 2.7.0
- NiceScroll 3.7.6
- DropZone 5.9.2
- Select2 4.1

The project supports (or will support :D ) the following languages:
- Polish (basic)
- English
- Germany
- Ukrainian
- Russian